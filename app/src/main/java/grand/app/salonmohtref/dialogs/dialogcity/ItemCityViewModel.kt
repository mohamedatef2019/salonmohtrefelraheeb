package grand.app.salonmohtref.dialogs.dialogcity

import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.main.selectaddress.response.CityItem

class ItemCityViewModel(var item: CityItem) : BaseViewModel()
