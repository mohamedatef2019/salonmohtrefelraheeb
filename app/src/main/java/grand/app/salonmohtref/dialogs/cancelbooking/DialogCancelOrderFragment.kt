package grand.app.salonmohtref.dialogs.cancelbooking

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import grand.app.salonmohtref.databinding.DialogCancelOrderBinding
import grand.app.salonmohtref.utils.constants.Codes
import grand.app.salonmohtref.utils.constants.Params
import timber.log.Timber

class DialogCancelOrderFragment  : DialogFragment()
{
    lateinit  var  binding: DialogCancelOrderBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View
    {
        binding = DialogCancelOrderBinding.inflate(inflater, container, false)

        binding.btnIgnore.setOnClickListener {
            val intent = Intent()
            intent.putExtra(Params.DIALOG_CLICK_ACTION, 0)
            requireActivity().setResult(Codes.DIALOG_CANCEL_BOOKING, intent)
            requireActivity().finish()
        }

        binding.btnConfirm.setOnClickListener {
            val intent = Intent()
            intent.putExtra(Params.DIALOG_CLICK_ACTION, 1)
            requireActivity().setResult(Codes.DIALOG_CANCEL_BOOKING, intent)
            requireActivity().finish()
        }

        return binding.root
    }
}