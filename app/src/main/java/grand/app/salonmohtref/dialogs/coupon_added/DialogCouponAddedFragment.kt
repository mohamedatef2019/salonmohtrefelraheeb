package grand.app.salonmohtref.dialogs.coupon_added

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import grand.app.salonmohtref.R
import grand.app.salonmohtref.databinding.DialogCouponAddedBinding
import grand.app.salonmohtref.utils.constants.Codes
import grand.app.salonmohtref.utils.constants.Params

class DialogCouponAddedFragment  : DialogFragment()
{
    lateinit  var  binding: DialogCouponAddedBinding
    var couponPoint : Int = 0
    var dialogDesc : String = ""

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        when {
            arguments != null -> {
                when {
                    requireArguments().containsKey(Params.COUPON_POINTS) -> {
                        couponPoint = requireArguments().getInt(Params.COUPON_POINTS)
                    }
                }
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View
    {
        binding = DialogCouponAddedBinding.inflate(inflater, container, false)

        dialogDesc = " " + couponPoint  + getString(R.string.label_points_used) + getString(R.string.label_right_bracket) + couponPoint +
                getString(R.string.label_left_bracket)  + getString(R.string.label_use_coupon_points)

        binding.tvMessage.text = dialogDesc

        binding.layoutBooked.setOnClickListener {
            val intent = Intent()
            intent.putExtra(Params.DIALOG_CLICK_ACTION, 1)
            requireActivity().setResult(Codes.DIALOG_COUPON_ADDED, intent)
            requireActivity().finish()
        }

        return binding.root
    }
}