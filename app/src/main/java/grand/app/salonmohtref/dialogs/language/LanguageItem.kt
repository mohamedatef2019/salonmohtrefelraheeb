package grand.app.salonmohtref.dialogs.language

data class LanguageItem(
    var id: Int? = null,
    var name: String? = null
)
