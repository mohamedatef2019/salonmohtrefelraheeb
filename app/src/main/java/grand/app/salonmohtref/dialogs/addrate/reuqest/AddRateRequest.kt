package grand.app.salonmohtref.dialogs.addrate.reuqest

data class AddRateRequest (
    var attribute_id: Int? = null,
    var rate: Int? = null,
    var comment: String? = null)