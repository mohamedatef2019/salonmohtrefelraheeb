package grand.app.salonmohtref.dialogs.dialogcity

import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.dialogs.dialogcity.CitiesAdapter
import grand.app.salonmohtref.main.selectaddress.response.CityItem
import grand.app.salonmohtref.utils.constants.Codes

class DialogCityViewModel : BaseViewModel()
{
    var cityItem = CityItem()
    var adapter = CitiesAdapter()

    fun onConfirmClicked() {
        when (cityItem.id) {
            null -> {
                setValue(Codes.EMPTY_CITY)
            }
            else -> {
                setValue(Codes.CITY_SELECTED)
            }
        }
    }

    fun onCloseClicked() {
        setValue(Codes.CLOSE_CLICKED)
    }
}