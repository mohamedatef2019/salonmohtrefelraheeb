package grand.app.salonmohtref.dialogs.confirm

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import grand.app.salonmohtref.databinding.DialogConfirmBinding
import grand.app.salonmohtref.utils.constants.Codes
import grand.app.salonmohtref.utils.constants.Params

class DialogConfirmFragment  : DialogFragment()
{
    lateinit  var  binding: DialogConfirmBinding
    var message : String = ""
    var subMsg : String = ""
    var positiveBtn : String = ""
    var negativeBtn : String = ""

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        when {
            arguments != null -> {
                when {
                    requireArguments().containsKey(Params.DIALOG_CONFIRM_MESSAGE) -> {
                        message = requireArguments().getString(Params.DIALOG_CONFIRM_MESSAGE, null)
                        subMsg = requireArguments().getString(Params.DIALOG_CONFIRM_MESSAGE, null)
                        positiveBtn = requireArguments().getString(Params.DIALOG_CONFIRM_POSITIVE, null)
                        subMsg = requireArguments().getString(Params.DIALOG_CONFIRM_SUB_MESSAGE, null)
                    }
                }
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        binding = DialogConfirmBinding.inflate(inflater, container, false)

        binding.tvMessage.text = message
        binding.tvSubMessage.text = subMsg
        binding.btnConfirm.text = positiveBtn
        binding.btnIgnore.text = negativeBtn

        binding.btnIgnore.setOnClickListener {
            val intent = Intent()
            intent.putExtra(Params.DIALOG_CLICK_ACTION, 0)
            requireActivity().setResult(Codes.DIALOG_CONFIRM_REQUEST, intent)
            requireActivity().finish()
        }

        binding.btnConfirm.setOnClickListener {
            val intent = Intent()
            intent.putExtra(Params.DIALOG_CLICK_ACTION, 1)
            requireActivity().setResult(Codes.DIALOG_CONFIRM_REQUEST, intent)
            requireActivity().finish()
        }

        return binding.root
    }
}