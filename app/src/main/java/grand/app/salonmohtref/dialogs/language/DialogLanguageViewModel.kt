package grand.app.salonmohtref.dialogs.language

import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.utils.constants.Codes

class DialogLanguageViewModel : BaseViewModel()
{
    var adapter = LanguageAdapter()
    var language : Int = -1

    init {
        val paymentList = arrayListOf(
                LanguageItem(0 , "عربي"),
                LanguageItem(1 , "English")
        )

        adapter.updateList(paymentList)
    }

    fun onConfirmClicked() {
        when (language) {
            -1 -> {
                setValue(Codes.EMPTY_SELECTED_LANGUAGE)
            }
            else -> {
                setValue(Codes.SELECT_LANGUAGE)
            }
        }
    }

    fun onCloseClicked() {
        setValue(Codes.CLOSE_CLICKED)
    }
}