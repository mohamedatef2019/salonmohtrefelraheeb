package grand.app.salonmohtref.main.confirmorder.viewmodel

import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.main.confirmorder.request.OrderPaymentItem

class ItemOrderPaymentViewModel(var item: OrderPaymentItem) : BaseViewModel()
