package grand.app.salonmohtref.main.search.request

data class SearchRequest (
        var name: String? = null,
        var type: Int? = null,
        var most_rated: Int = 0,
        var newest: Int = 0,
        var nearest: Int = 0,
        var lat: Double? = 0.0,
        var lng: Double? = 0.0
        )