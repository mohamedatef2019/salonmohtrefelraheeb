package grand.app.salonmohtref.main.barberprofile.viewmodel

import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.main.favorites.response.BarberWorkingItem

class ItemBarberWorkingViewModel(var item: BarberWorkingItem) : BaseViewModel()