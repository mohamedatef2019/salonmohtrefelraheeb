package grand.app.salonmohtref.main.salondetails.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.salonmohtref.R
import grand.app.salonmohtref.databinding.ItemSalonServiceBinding
import grand.app.salonmohtref.main.salondetails.response.SalonServiceItem
import grand.app.salonmohtref.main.salondetails.viewmodel.ItemSalonServiceViewModel
import grand.app.salonmohtref.utils.SingleLiveEvent
import java.util.*

class SalonServicesAdapter : RecyclerView.Adapter<SalonServicesAdapter.SalonServicesHolder>()
{
    var itemsList: ArrayList<SalonServiceItem> = ArrayList()
    var itemServiceLiveData = SingleLiveEvent<SalonServiceItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SalonServicesHolder
    {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemSalonServiceBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_salon_service, parent, false)
        return SalonServicesHolder(binding)
    }

    override fun onBindViewHolder(holder: SalonServicesHolder, position: Int) {
        val itemViewModel = ItemSalonServiceViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.setSelected()

        holder.binding.rawLayout.setOnClickListener {
            when {
                itemViewModel.item.isSelected -> {
                    notifyItemChanged(position)
                    itemViewModel.item.isSelected = false
                }
                else -> {
                    notifyItemChanged(position)
                    itemViewModel.item.isSelected = true
                }
            }
            itemServiceLiveData.value = itemViewModel.item
        }

        holder.binding.ivServiceCheck.setOnClickListener {
            when {
                itemViewModel.item.isSelected -> {
                    notifyItemChanged(position)
                    itemViewModel.item.isSelected = false
                }
                else -> {
                    notifyItemChanged(position)
                    itemViewModel.item.isSelected = true
                }
            }
            itemServiceLiveData.value = itemViewModel.item
        }
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<SalonServiceItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class SalonServicesHolder(val binding: ItemSalonServiceBinding) : RecyclerView.ViewHolder(binding.root)
    {
        fun setSelected()
        {
            when {
                itemsList[adapterPosition].isSelected -> {
                    binding.ivServiceCheck.setImageResource(R.drawable.ic_service_checked)
                }
                else -> {
                    binding.ivServiceCheck.setImageResource(R.drawable.ic_service_unchecked)
                }
            }
        }
    }
}
