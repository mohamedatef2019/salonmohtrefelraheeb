package grand.app.salonmohtref.main.confirmorder.request

import android.os.Parcel
import android.os.Parcelable
import grand.app.salonmohtref.main.salondetails.response.SalonServiceItem

data class OrderRequest (
    var salon_id: Int? = null,
    var date: String? = null,
    var time: String? = null,
    var extra_notes: String = "",
    var sub_category_id: ArrayList<Int> = arrayListOf(),
    var servicesList: ArrayList<SalonServiceItem> = arrayListOf(),
    var total_price: Double? = null,
    var payment_method: Int? = null,
    var free_sub_category_id: ArrayList<Int> = arrayListOf(),

    var barber_id: Int? = null,
    var coupon_id: Int? = null,
    var city_id: Int? = null,
    var area: String? = null,
    var st: String? = null,
    var building_no: String? = null,
    var floor_no: String? = null,
    var phone: String? = null,
    var extra_phone: String? = null,
    var mark: String? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString(),
            parcel.readString().toString(),
            TODO("sub_category_id"),
            TODO("servicesList"),
            parcel.readValue(Double::class.java.classLoader) as? Double,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            TODO("free_sub_category_id"),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(salon_id)
        parcel.writeString(date)
        parcel.writeString(time)
        parcel.writeString(extra_notes)
        parcel.writeValue(total_price)
        parcel.writeValue(payment_method)
        parcel.writeValue(barber_id)
        parcel.writeValue(coupon_id)
        parcel.writeValue(city_id)
        parcel.writeString(area)
        parcel.writeString(st)
        parcel.writeString(building_no)
        parcel.writeString(floor_no)
        parcel.writeString(phone)
        parcel.writeString(extra_phone)
        parcel.writeString(mark)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<OrderRequest> {
        override fun createFromParcel(parcel: Parcel): OrderRequest {
            return OrderRequest(parcel)
        }

        override fun newArray(size: Int): Array<OrderRequest?> {
            return arrayOfNulls(size)
        }
    }
}