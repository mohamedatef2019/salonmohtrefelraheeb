package grand.app.salonmohtref.main.bookings.viewmodel

import androidx.databinding.ObservableInt
import grand.app.salonmohtref.base.BaseViewModel

class MyBookingViewModel : BaseViewModel()
{
    val obsPosition = ObservableInt(0)

    fun onTabClicked(position : Int){
        obsPosition.set(position)
        setValue(position)
    }

    fun onPageSelected(position : Int)
    {
        obsPosition.set(position)
    }
}