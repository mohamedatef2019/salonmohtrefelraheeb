package grand.app.salonmohtref.main.bookings.adapter

import androidx.databinding.ObservableField
import grand.app.salonmohtref.R
import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.main.bookings.response.BookingServiceItem
import grand.app.salonmohtref.main.salondetails.response.SalonServiceItem

class ItemBookingServiceViewModel(var item: BookingServiceItem) : BaseViewModel()
{
    var obsPrice = ObservableField<String>()
    var obsDuration = ObservableField<String>()

    init {
        obsPrice.set("${item.price} ${getString(R.string.label_currency)}")
        obsDuration.set("${item.duration} ${getString(R.string.label_minute)}")
    }
}