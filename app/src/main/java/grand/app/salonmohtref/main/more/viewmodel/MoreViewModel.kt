package grand.app.salonmohtref.main.more.viewmodel

import androidx.core.content.ContextCompat
import grand.app.salonmohtref.R
import grand.app.salonmohtref.base.BaseApp
import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.main.bottombar.more.view.ItemMoreProfile
import grand.app.salonmohtref.main.more.view.MoreProfileAdapter
import grand.app.salonmohtref.utils.PrefMethods

class MoreViewModel : BaseViewModel()
{
    var adapter = MoreProfileAdapter()

    init {
        val moreList = arrayListOf(
                ItemMoreProfile(0, getString(R.string.title_notifications), ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.ic_notifications)),
                ItemMoreProfile(1, getString(R.string.title_terms), ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.ic_terms_and_conditions)),
                ItemMoreProfile(2, getString(R.string.title_about_app), ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.ic_about_app)),
                ItemMoreProfile(3, getString(R.string.title_suggestions), ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.ic_suggestions)),
                ItemMoreProfile(4, getString(R.string.title_cancel_policy), ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.ic_cancel_policy)),
                ItemMoreProfile(5, getString(R.string.title_contact_us), ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.ic_contact_us)),
                ItemMoreProfile(6, getString(R.string.label_share), ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.ic_share)),
                ItemMoreProfile(7, getString(R.string.title_change_language), ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.ic_language)),
                getLastItem()
        )

        adapter.updateList(moreList)
    }

    private fun getLastItem() : ItemMoreProfile
    {
        return when {
            PrefMethods.getUserData() == null -> {
                ItemMoreProfile(8, getString(R.string.label_login), ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.ic_profile_logout))
            }
            else -> {
                ItemMoreProfile(8, getString(R.string.label_log_out), ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.ic_profile_logout))
            }
        }
    }
}