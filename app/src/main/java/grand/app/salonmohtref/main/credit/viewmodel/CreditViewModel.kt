package grand.app.salonmohtref.main.credit.viewmodel

import androidx.databinding.ObservableField
import grand.app.salonmohtref.R
import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.base.LoadingStatus
import grand.app.salonmohtref.main.credit.response.CreditResponse
import grand.app.salonmohtref.main.credit.response.CreditItem
import grand.app.salonmohtref.main.credit.view.CreditAdapter
import grand.app.salonmohtref.network.ApiResponse
import grand.app.salonmohtref.utils.PrefMethods
import grand.app.salonmohtref.utils.constants.Codes
import grand.app.salonmohtref.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.time.format.DateTimeFormatter

class CreditViewModel : BaseViewModel()
{
    var obsCredit = ObservableField<String>()
    var adapter = CreditAdapter()
    var creditList = arrayListOf<CreditItem>()

    fun getMyCredit()
    {
        obsLayout.set(LoadingStatus.SHIMMER)
        requestCall<CreditResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().getMyCredit() }
        })
        { res ->
            apiResponseLiveData.value = ApiResponse.shimmerLoading(false)
            when (res!!.code) {
                200 -> {
                    when (res.myCreditData?.points) {
                        0 -> {
                            obsIsVisible.set(false)
                        }
                        else ->{
                            obsIsVisible.set(true)
                            obsLayout.set(LoadingStatus.FULL)

                            obsCredit.set(" ${res.myCreditData?.points} ${getString(R.string.label_points)}")

                            creditList.clear()

                            res.myCreditData?.ordersList.let { list ->
                                list?.forEach { item ->
                                    creditList.add(CreditItem(item?.id, 0 , item?.date, null, item?.updatedAt))
                                }
                            }

                            res.myCreditData?.couponsList.let { list ->
                                list?.forEach { item ->
                                    creditList.add(CreditItem(item?.couponId, 1 , item?.createdAt, item?.couponPoints, null))
                                }
                            }

                            adapter.orderPoints = res.myCreditData?.reservationPoints!!
                            adapter.couponPoints = res.myCreditData.points!!

                            creditList.sortedWith(compareByDescending<CreditItem> { it.createdAt })

                            adapter.updateList(creditList)
                        }
                    }
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }


    fun onLoginClicked() {
        setValue(Codes.LOGIN_CLICKED)
    }

    fun onAddClicked() {
        setValue(Codes.EXCHANGE_CLICKED)
    }

    fun getUserStatus() {
        when {
            PrefMethods.getUserData() != null -> {
                getMyCredit()
            }
            else -> {
                obsIsVisible.set(false)
                obsLayout.set(LoadingStatus.NOTLOGIN)
            }
        }
    }

    init {
        getUserStatus()
    }
}
