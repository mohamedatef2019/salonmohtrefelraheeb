package grand.app.salonmohtref.main.package_details.viewmodel

import androidx.databinding.ObservableField
import grand.app.salonmohtref.R
import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.base.LoadingStatus
import grand.app.salonmohtref.main.mypackage.view.PackageServiceAdapter
import grand.app.salonmohtref.main.package_details.response.AddPackageResponse
import grand.app.salonmohtref.main.package_details.view.PackagePaymentAdapter
import grand.app.salonmohtref.main.package_details.view.PackagePaymentItem
import grand.app.salonmohtref.main.packages.response.PackageItem
import grand.app.salonmohtref.main.packages.response.PackageServiceItem
import grand.app.salonmohtref.network.ApiResponse
import grand.app.salonmohtref.utils.PrefMethods
import grand.app.salonmohtref.utils.constants.Codes
import grand.app.salonmohtref.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.ArrayList

class PackageDetailsViewModel : BaseViewModel()
{
    var adapter = PackageServiceAdapter()
    var paymentAdapter = PackagePaymentAdapter()
    var obsPeriod = ObservableField<String>()
    var obsPrice = ObservableField<String>()
    var obsPackageName = ObservableField<String>()
    var packageData = PackageItem()
    var obsVisitCount = ObservableField<String>()

    fun fillPackageData(packageItem : PackageItem){
        packageItem.let {
            packageData = packageItem
            adapter.visitCount = packageItem.visits!!
            adapter.updateList(packageItem.servicesList as ArrayList<PackageServiceItem>)
            obsPrice.set("""  ${packageItem.price} ${getString(R.string.label_currency)}""")
            obsPackageName.set(packageItem.name)
            obsVisitCount.set("""  ${packageItem.visits} ${getString(R.string.label_visits)}""")
            obsPeriod.set("""  ${packageItem.period} ${getString(R.string.label_months)} ${getString(R.string.label_added_to)} ${packageItem.freeMonths} ${getString(R.string.label_free_months)}""")
        }
     }

    fun onLoginClicked() {
        setValue(Codes.LOGIN_CLICKED)
    }

    fun onSubscribeClicked() {
        packageData.let { item ->
            obsIsProgress.set(true)
            requestCall<AddPackageResponse?>({
                withContext(Dispatchers.IO) { return@withContext getApiRepo().addPackage(item.id!!) }
            })
            { res ->
                obsIsProgress.set(false)
                when (res?.code) {
                    200 -> {
                        apiResponseLiveData.value = ApiResponse.success(res)
                    }
                    else -> {
                        apiResponseLiveData.value = ApiResponse.errorMessage(res?.msg.toString())
                    }
                }
            }
        }
    }

    fun getUserStatus() {
        when {
            PrefMethods.getUserData() != null -> {
            }
            else -> {
                obsIsVisible.set(false)
                obsLayout.set(LoadingStatus.NOTLOGIN)
            }
        }
    }

    init {
        val paymentList = arrayListOf(
                PackagePaymentItem(0 , "دفع أونلاين"),
                PackagePaymentItem(1 , "دفع عن طريق رصيدي")
        )

        paymentAdapter.updateList(paymentList)
        getUserStatus()
    }
}
