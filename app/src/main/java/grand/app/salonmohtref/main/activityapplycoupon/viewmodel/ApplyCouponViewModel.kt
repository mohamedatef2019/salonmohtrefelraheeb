package grand.app.salonmohtref.main.activityapplycoupon.viewmodel

import androidx.databinding.ObservableField
import grand.app.salonmohtref.R
import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.base.LoadingStatus
import grand.app.salonmohtref.main.activitycoupons.view.ApplyCouponAdapter
import grand.app.salonmohtref.main.activitycoupons.view.SelectedServicesAdapter
import grand.app.salonmohtref.main.activitycoupons.view.UsedServicesAdapter
import grand.app.salonmohtref.main.coupons.response.CouponItem
import grand.app.salonmohtref.main.coupons.response.UserServicesItem
import grand.app.salonmohtref.utils.constants.Codes

class ApplyCouponViewModel : BaseViewModel()
{
    var couponServicesAdapter = ApplyCouponAdapter()
    var usedServicesAdapter = UsedServicesAdapter()
    var selectedServicesAdapter = SelectedServicesAdapter()
    var couponData = CouponItem()
    var obsName = ObservableField<String>()

    fun fillCouponData(couponItem : CouponItem) {
        couponData = couponItem
        obsName.set("""${getString(R.string.label_coupon)} ${getString(R.string.label_right_bracket)}${couponItem.points}${getString(R.string.label_left_bracket)}""")
        couponServicesAdapter.updateList(couponData.services)

        when (couponData.services!!.size) {
            0 -> {
                obsLayout.set(LoadingStatus.EMPTY)
            }
            else -> {
                usedServicesAdapter.updateList(couponData.userServices)
                obsLayout.set(LoadingStatus.FULL)
            }
        }
    }

    fun onApplyCoupon(){
        when (selectedServicesAdapter.itemsList.size) {
            0 -> {
                setValue(Codes.EMPTY_SERVICES)
            }
            else -> {
                setValue(Codes.APPLY_COUPON_CLICKED)
            }
        }
    }

    fun onBackPressed()
    {
        setValue(Codes.BACK_PRESSED)
    }
}
