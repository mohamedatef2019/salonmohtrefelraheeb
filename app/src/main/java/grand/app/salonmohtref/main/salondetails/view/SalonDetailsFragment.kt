package grand.app.salonmohtref.main.salondetails.view

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType
import com.smarteist.autoimageslider.SliderAnimations
import com.smarteist.autoimageslider.SliderView
import grand.app.salonmohtref.R
import grand.app.salonmohtref.activity.auth.AuthActivity
import grand.app.salonmohtref.activity.home.BaseHomeFragment
import grand.app.salonmohtref.base.BaseApp
import grand.app.salonmohtref.databinding.FragmentSalonDetailsBinding
import grand.app.salonmohtref.dialogs.login.DialogLoginFragment
import grand.app.salonmohtref.main.favorites.response.AddToFavResponse
import grand.app.salonmohtref.main.salondetails.response.MediaItem
import grand.app.salonmohtref.main.salondetails.response.SalonDetailsResponse
import grand.app.salonmohtref.main.salondetails.viewmodel.SalonDetailsViewModel
import grand.app.salonmohtref.network.Status
import grand.app.salonmohtref.utils.Utils
import grand.app.salonmohtref.utils.constants.Codes
import grand.app.salonmohtref.utils.constants.Const
import grand.app.salonmohtref.utils.constants.Params
import grand.app.salonmohtref.utils.observe
import timber.log.Timber

class SalonDetailsFragment : BaseHomeFragment()
{
    lateinit var binding: FragmentSalonDetailsBinding
    lateinit var viewModel: SalonDetailsViewModel
    lateinit var sliderView : SliderView
    private val fragmentArgs : SalonDetailsFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_salon_details, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(SalonDetailsViewModel::class.java)
        binding.viewModel = viewModel

        showBottomBar(false)
        viewModel.getDetails(fragmentArgs.salonId)

        when (fragmentArgs.flag) {
            -1 -> {
                binding.layoutSalonssServices.visibility = View.VISIBLE
            }
            else -> {
                binding.layoutSalonssServices.visibility = View.GONE
                viewModel.serviceType = fragmentArgs.flag
            }
        }

        viewModel.categoriesAdapter.selectedPosition = -1
        viewModel.categoriesAdapter.selectedServices.clear()
        viewModel.categoriesAdapter.servicesIds.clear()
        viewModel.categoriesAdapter.totalPrice = 0.0
        viewModel.orderRequest.total_price = 0.0
        viewModel.orderRequest.servicesList.clear()
        viewModel.orderRequest.sub_category_id.clear()
        viewModel.orderRequest.total_price = 0.0
        viewModel.orderRequest.date = null
        viewModel.orderRequest.time = null
        viewModel.orderRequest.extra_notes = ""

        binding.ibBack.setOnClickListener {
            findNavController().navigateUp()
        }

        observe(viewModel.categoriesAdapter.obsShowOrderBtn) {
            when (it) {
                true -> {
                    binding.btnOrderService.visibility = View.VISIBLE
                }
                else -> {
                    binding.btnOrderService.visibility = View.GONE
                }
            }
        }

        binding.rbSalonRate.setOnClickListener {
            findNavController().navigate(SalonDetailsFragmentDirections.salonDetailsToRates(fragmentArgs.salonId))
        }

        observe(viewModel.apiResponseLiveData) {

            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is SalonDetailsResponse -> {
                            binding.tvTitle.text = it.data.salonData?.name
                            setupSlider(it.data.salonData!!.media as List<MediaItem>)
                            when (it.data.salonData.isFavorite) {
                                1 -> {
                                    Glide.with(requireActivity()).load(R.drawable.ic_fav).into(binding.ibFav)
                                }
                                else -> {
                                    Glide.with(requireActivity()).load(R.drawable.ic_un_fav).into(binding.ibFav)
                                }
                            }
                            binding.btnHomeServices.setBackgroundResource(R.drawable.selected_home_bg)
                            binding.btnHomeServices.setTextColor(ContextCompat.getColor(BaseApp.getInstance, R.color.white))
                            binding.btnSalonServices.setBackgroundResource(R.drawable.unselected_home_bg)
                            binding.btnSalonServices.setTextColor(ContextCompat.getColor(BaseApp.getInstance, R.color.color_primary))
                        }
                        is AddToFavResponse -> {
                            showToast(it.data.msg.toString(), 2)
                            when (it.data.addFavData!!.isFavorite) {
                                1 -> {
                                    Glide.with(this).load(R.drawable.ic_fav).into(binding.ibFav)
                                }
                                else -> {
                                    Glide.with(this).load(R.drawable.ic_un_fav).into(binding.ibFav)
                                }
                            }
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }

        observe(viewModel.mutableLiveData) {
            when (it) {
                Codes.NOT_LOGIN -> {
                    Utils.startDialogActivity(requireActivity(), DialogLoginFragment::class.java.name, Codes.DIALOG_LOGIN_REQUEST, null)
                }
                Codes.RATE_ORDER_CLICKED -> {
                    val action = SalonDetailsFragmentDirections.salonDetailsToRates(fragmentArgs.salonId)
                    findNavController().navigate(action)
                }
                Codes.BOOK_ORDER_CLICKED -> {
                    when (viewModel.categoriesAdapter.selectedServices.size) {
                        0 -> {
                            showToast(getString(R.string.msg_empty_services), 1)
                        }
                        else -> {
                            viewModel.orderRequest.sub_category_id = viewModel.categoriesAdapter.servicesIds
                            viewModel.orderRequest.servicesList = viewModel.categoriesAdapter.selectedServices
                            viewModel.orderRequest.total_price = viewModel.categoriesAdapter.totalPrice
                            when (viewModel.serviceType) {
                                0 -> {
                                    // to address
                                    findNavController().navigate(SalonDetailsFragmentDirections.salonDetailsToSelectAddress(viewModel.salonDetails, viewModel.orderRequest))
                                }
                                else -> {
                                    // to home
                                    findNavController().navigate(SalonDetailsFragmentDirections.salonDetailsToSelectTime(viewModel.salonDetails, viewModel.orderRequest))
                                }
                            }
                        }
                    }
                }
                Codes.HOME_CLICKED -> {
                    binding.btnHomeServices.setBackgroundResource(R.drawable.selected_home_bg)
                    binding.btnHomeServices.setTextColor(ContextCompat.getColor(BaseApp.getInstance, R.color.white))
                    binding.btnSalonServices.setBackgroundResource(R.drawable.unselected_home_bg)
                    binding.btnSalonServices.setTextColor(ContextCompat.getColor(BaseApp.getInstance, R.color.color_primary))
                }
                Codes.SALON_CLICKED -> {
                    binding.btnSalonServices.setBackgroundResource(R.drawable.selected_home_bg)
                    binding.btnSalonServices.setTextColor(ContextCompat.getColor(BaseApp.getInstance, R.color.white))
                    binding.btnHomeServices.setBackgroundResource(R.drawable.unselected_home_bg)
                    binding.btnHomeServices.setTextColor(ContextCompat.getColor(BaseApp.getInstance, R.color.color_primary))
                }
            }
        }
    }

    private fun setupSlider(covers: List<MediaItem>) {
        viewModel.sliderAdapter.updateList(covers)
        sliderView = requireActivity().findViewById(R.id.slider_salon_ads)
        sliderView.setSliderAdapter(viewModel.sliderAdapter)
        sliderView.setIndicatorAnimation(IndicatorAnimationType.WORM)
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION)
        sliderView.autoCycleDirection = SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH
        sliderView.indicatorSelectedColor = ContextCompat.getColor(requireActivity(), R.color.color_primary)
        sliderView.indicatorUnselectedColor = Color.WHITE
        sliderView.scrollTimeInSec = 3
        sliderView.isAutoCycle = true
        sliderView.startAutoCycle()
    }

    override fun onResume() {
        super.onResume()
        when (Const.isAskedToLogin) {
            1 -> {
                viewModel.getDetails(fragmentArgs.salonId)
                Const.isAskedToLogin = 0
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when(requestCode) {
            Codes.DIALOG_LOGIN_REQUEST -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        Const.isAskedToLogin = 1
                                        requireActivity().startActivity(Intent(requireActivity(), AuthActivity::class.java).putExtra(Const.ACCESS_LOGIN, true))
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}