package grand.app.salonmohtref.main.coupons.response

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class CouponResponse(

        @field:SerializedName("msg")
	val msg: String? = null,

        @field:SerializedName("code")
	val code: Int? = null,

        @field:SerializedName("data")
	val couponsList: List<CouponItem?>? = null,

        @field:SerializedName("status")
	val status: String? = null
):Serializable

data class CouponServiceItem(

	@field:SerializedName("coupon_id")
	val couponId: Int? = null,

	@field:SerializedName("sub_category_id")
	val subCategoryId: Int? = null,

	@field:SerializedName("count")
	val count: Int? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null
):Serializable

data class UserServicesItem(

	@field:SerializedName("coupon_id")
	var couponId: Int? = null,

	@field:SerializedName("user_id")
	var userId: Int? = null,

	@field:SerializedName("sub_category_id")
	var subCategoryId: Int? = null,

	@field:SerializedName("count")
	var count: Int? = null,

	@field:SerializedName("name")
	var name: String? = null,

	@field:SerializedName("id")
	var id: Int? = null
):Serializable

data class CouponItem(

        @field:SerializedName("user_services")
	val userServices: List<UserServicesItem?>? = null,

        @field:SerializedName("id")
	val id: Int? = null,

        @field:SerializedName("services")
	val services: List<CouponServiceItem?>? = null,

        @field:SerializedName("points")
	val points: Int? = null
):Serializable