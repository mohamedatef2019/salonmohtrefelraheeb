package grand.app.salonmohtref.main.profile.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import grand.app.salonmohtref.activity.home.BaseHomeFragment
import grand.app.salonmohtref.R
import grand.app.salonmohtref.databinding.FragmentProfileBinding
import grand.app.salonmohtref.main.profile.viewmodel.ProfileViewModel
import grand.app.salonmohtref.utils.observe

class ProfileFragment : BaseHomeFragment()
{
    lateinit var binding: FragmentProfileBinding
    lateinit var viewModel: ProfileViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(ProfileViewModel::class.java)
        binding.viewModel = viewModel

        showBottomBar(true)

        observe(viewModel.adapter.itemLiveData){
            it.let {
                when (it?.id) {
                    0 -> {
                        findNavController().navigate(R.id.profile_to_edit)
                    }
                    1 -> {
                        findNavController().navigate(R.id.profile_to_change_password)
                    }
                    2 -> {
                        findNavController().navigate(R.id.profile_to_bookings)
                    }
                    3 -> {
                        findNavController().navigate(R.id.profile_to_credit)
                    }
                    4 -> {
                         findNavController().navigate(R.id.profile_to_wallet)
                    }
                    5 -> {
                        findNavController().navigate(R.id.profile_to_packages)
                    }
                    6 -> {
                        findNavController().navigate(R.id.profile_to_favorites)
                    }
                    7 -> {
                         findNavController().navigate(R.id.profile_to_coupons)
                    }
                }
            }
        }
    }
}