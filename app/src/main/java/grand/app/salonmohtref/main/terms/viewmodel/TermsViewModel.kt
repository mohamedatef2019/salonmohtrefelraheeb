package grand.app.salonmohtref.main.terms.viewmodel

import androidx.databinding.ObservableField
import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.base.LoadingStatus
import grand.app.salonmohtref.main.terms.response.TermsItem
import grand.app.salonmohtref.main.terms.response.TermsResponse
import grand.app.salonmohtref.main.terms.view.TermsAdapter
import grand.app.salonmohtref.network.ApiResponse
import grand.app.salonmohtref.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.*

class TermsViewModel : BaseViewModel()
{
    var adapter = TermsAdapter()
    var termsImg = ObservableField<String>()

    init {
        getTerms()
    }

    fun getTerms()
    {
        obsLayout.set(LoadingStatus.SHIMMER)
        requestCall<TermsResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().getTerms() }
        })
        { res ->
            apiResponseLiveData.value = ApiResponse.shimmerLoading(false)
            when (res!!.code) {
                200 -> {
                    obsLayout.set(LoadingStatus.FULL)
                    termsImg.set(res.termsData!!.img)
                    adapter.updateList(res.termsData.termsList as ArrayList<TermsItem>)
                    notifyChange()
                }
                401 -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }
}