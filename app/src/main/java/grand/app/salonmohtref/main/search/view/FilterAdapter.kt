package grand.app.salonmohtref.main.search.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.salonmohtref.R
import grand.app.salonmohtref.databinding.ItemFilterBinding
import grand.app.salonmohtref.utils.SingleLiveEvent
import kotlin.collections.ArrayList

class FilterAdapter : RecyclerView.Adapter<FilterAdapter.FilterHolder>()
{
    var itemsList: ArrayList<FilterItem> = ArrayList()
    var itemLiveData = SingleLiveEvent<FilterItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FilterHolder {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemFilterBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_filter, parent, false)
        return FilterHolder(binding)
    }

    override fun onBindViewHolder(holder: FilterHolder, position: Int) {
        val itemViewModel = ItemFilterViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        when (position) {
            itemsList.size-1 -> {
                holder.binding.tvDivider.visibility = View.GONE
            }
        }

        holder.setSelected()
        holder.binding.rawLayout.setOnClickListener {
            when {
                itemViewModel.item.isSelected == 1-> {
                    notifyItemChanged(position)
                    itemViewModel.item.isSelected = 0
                }
                else -> {
                    notifyItemChanged(position)
                    itemViewModel.item.isSelected = 1
                }
            }
            itemLiveData.value = itemViewModel.item
        }

        holder.binding.ibFilterChecked.setOnClickListener {
            when {
                itemViewModel.item.isSelected == 1 -> {
                    notifyItemChanged(position)
                    itemViewModel.item.isSelected = 0
                }
                else -> {
                    notifyItemChanged(position)
                    itemViewModel.item.isSelected = 1
                }
            }
            itemLiveData.value = itemViewModel.item
        }
    }

    fun getItem(pos:Int): FilterItem {
        return itemsList[pos]
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<FilterItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class FilterHolder(val binding: ItemFilterBinding) : RecyclerView.ViewHolder(binding.root) {
        fun setSelected()
        {
            when {
                itemsList[adapterPosition].isSelected == 0 -> {
                    binding.ibFilterChecked.setImageResource(R.drawable.ic_unchecked)
                }
                else -> {
                    binding.ibFilterChecked.setImageResource(R.drawable.ic_checked)
                }
            }
        }
    }
}
