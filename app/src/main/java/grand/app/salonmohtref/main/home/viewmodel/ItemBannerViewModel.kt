package grand.app.salonmohtref.main.home.viewmodel

import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.main.bottombar.home.response.HomeSliderItem

class ItemBannerViewModel(var item: HomeSliderItem) : BaseViewModel()