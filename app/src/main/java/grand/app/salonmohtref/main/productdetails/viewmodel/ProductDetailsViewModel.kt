package grand.app.salonmohtref.main.productdetails.viewmodel

import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.base.LoadingStatus
import grand.app.salonmohtref.main.productdetails.response.ProductDetailsData
import grand.app.salonmohtref.main.productdetails.response.ProductDetailsResponse
import grand.app.salonmohtref.main.bottombar.products.response.ProductItem
import grand.app.salonmohtref.main.products.view.ProductsAdapter
import grand.app.salonmohtref.network.ApiResponse
import grand.app.salonmohtref.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.ArrayList

class ProductDetailsViewModel : BaseViewModel()
{
    var adapter = ProductsAdapter()
    var productDetails = ProductDetailsData()

    fun getProductDetails(productId : Int) {
        obsLayout.set(LoadingStatus.SHIMMER)

        requestCall<ProductDetailsResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().getProductDetails(productId) } })
        { res ->
            obsLayout.set(LoadingStatus.FULL)
            when (res!!.code) {
                200 -> {
                    productDetails = res.productDetailsData!!
                    apiResponseLiveData.value = ApiResponse.success(res)
                    when {
                        res.productDetailsData.relatedProductsList!!.isNotEmpty() -> {
                            obsIsVisible.set(true)
                            adapter.updateList(res.productDetailsData.relatedProductsList as ArrayList<ProductItem>)
                        }
                        else -> {
                            obsIsVisible.set(false)
                        }
                    }
                    notifyChange()
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }
}