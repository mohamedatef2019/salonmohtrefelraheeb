package grand.app.salonmohtref.main.activitycoupons.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.salonmohtref.R
import grand.app.salonmohtref.databinding.ItemCouponSelectedServicesBinding
import grand.app.salonmohtref.main.activitycoupons.viewmodel.ItemSelectedServiceViewModel
import grand.app.salonmohtref.main.coupons.response.UserServicesItem
import grand.app.salonmohtref.utils.SingleLiveEvent
import timber.log.Timber
import java.util.*

class SelectedServicesAdapter : RecyclerView.Adapter<SelectedServicesAdapter.SelectedServicesHolder>()
{
    var itemsList: ArrayList<UserServicesItem> = ArrayList()
    var itemLiveData = SingleLiveEvent<UserServicesItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SelectedServicesHolder
    {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemCouponSelectedServicesBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_coupon_selected_services, parent, false)
        return SelectedServicesHolder(binding)
    }

    override fun onBindViewHolder(holder: SelectedServicesHolder, position: Int) {
        val itemViewModel = ItemSelectedServiceViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.binding.rawLayout.setOnClickListener {
            itemLiveData.value = itemViewModel.item
        }
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun getItem(pos:Int): UserServicesItem {
        return itemsList[pos]
    }

    fun notifyItemAdded(item : UserServicesItem)
    {
        itemsList.forEach {
            if (item.id == it.id)
            {
                it.count = it.count?.plus(1)
                notifyDataSetChanged()
                return
            }
        }
        itemsList.add(item)
        notifyDataSetChanged()
    }

    fun notifyItemRemoved(item : UserServicesItem)
    {

        var pos = itemsList.indexOf(item)
        itemsList.forEach {
            if (item.id == it.id && it.count!! > 1)
            {
                it.count = it.count?.minus(1)
                notifyDataSetChanged()
                return
            }
        }
        itemsList.removeAt(pos)

        notifyDataSetChanged()
    }

    fun updateList(models: List<UserServicesItem?>?) {
        itemsList = models as ArrayList<UserServicesItem>
        notifyDataSetChanged()
    }

    inner class SelectedServicesHolder(val binding: ItemCouponSelectedServicesBinding) : RecyclerView.ViewHolder(binding.root)
}
