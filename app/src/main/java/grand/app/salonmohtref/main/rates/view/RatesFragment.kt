package grand.app.salonmohtref.main.rates.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import grand.app.salonmohtref.activity.home.BaseHomeFragment
import grand.app.salonmohtref.R
import grand.app.salonmohtref.activity.auth.AuthActivity
import grand.app.salonmohtref.databinding.FragmentRatesBinding
import grand.app.salonmohtref.dialogs.addrate.view.DialogAddRateFragment
import grand.app.salonmohtref.dialogs.login.DialogLoginFragment
import grand.app.salonmohtref.main.rates.viewmodel.RatesViewModel
import grand.app.salonmohtref.utils.PrefMethods
import grand.app.salonmohtref.utils.Utils
import grand.app.salonmohtref.utils.constants.Codes
import grand.app.salonmohtref.utils.constants.Const
import grand.app.salonmohtref.utils.constants.Params
import grand.app.salonmohtref.utils.observe


class RatesFragment : BaseHomeFragment()
{
    lateinit var binding: FragmentRatesBinding
    lateinit var viewModel: RatesViewModel
    private val fragmentArgs : RatesFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_rates, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(RatesViewModel::class.java)
        binding.viewModel = viewModel

        showBottomBar(false)

        if(fragmentArgs !== null) {
            viewModel.getRates(fragmentArgs.salonId)
        }else {
            viewModel.getAllRates()

        }
        observe(viewModel.mutableLiveData){
            when (it) {
                Codes.RATE_APP_CLICKED -> {
                    val bundle = Bundle()
                    bundle.putInt(Params.SALON_ID, fragmentArgs.salonId)
                    Utils.startDialogActivity(requireActivity(), DialogAddRateFragment::class.java.name, Codes.DIALOG_ADD_RATE, bundle)
                }
                Codes.NOT_LOGIN -> {
                    Utils.startDialogActivity(requireActivity(), DialogLoginFragment::class.java.name, Codes.DIALOG_LOGIN_REQUEST, null)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when(requestCode){
            Codes.DIALOG_LOGIN_REQUEST -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        Const.isAskedToLogin = 1
                                        requireActivity().startActivity(Intent(requireActivity(), AuthActivity::class.java).putExtra(Const.ACCESS_LOGIN , true))
                                    }
                                }
                            }
                        }
                    }
                }
            }
            Codes.DIALOG_ADD_RATE -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        viewModel.getRates(fragmentArgs.salonId)
                                     }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        when (Const.isAskedToLogin) {
            1 -> {
                when {
                    PrefMethods.getUserData() != null -> {
                        val bundle = Bundle()
                        bundle.putString(Params.SALON_ID, fragmentArgs.salonId.toString())
                        Utils.startDialogActivity(requireActivity(), DialogAddRateFragment::class.java.name, Codes.DIALOG_ADD_RATE, null)
                        Const.isAskedToLogin = 0
                    }
                }
            }
        }
    }
}