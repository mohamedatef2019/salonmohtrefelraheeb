package grand.app.salonmohtref.main.confirmorder.viewmodel

import androidx.databinding.ObservableField
import grand.app.salonmohtref.R
import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.main.salondetails.response.SalonServiceItem

class ItemOrderServiceViewModel(var item: SalonServiceItem) : BaseViewModel()
{
    var obsPrice = ObservableField<String>()
    var obsDuration = ObservableField<String>()

    init {
        obsPrice.set("${item.price} ${getString(R.string.label_details_currency)}")
        obsDuration.set("${item.duration} ${getString(R.string.label_minute)}")
    }
}
