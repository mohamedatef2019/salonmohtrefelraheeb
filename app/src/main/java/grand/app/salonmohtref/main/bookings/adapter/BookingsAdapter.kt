package grand.app.salonmohtref.main.bookings.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.salonmohtref.R
import grand.app.salonmohtref.databinding.ItemBookingBinding
import grand.app.salonmohtref.main.bookings.response.MyBookingItem
import grand.app.salonmohtref.utils.SingleLiveEvent
import java.util.*

class BookingsAdapter : RecyclerView.Adapter<BookingsAdapter.CurrentBookingsHolder>()
{
    var itemsList: ArrayList<MyBookingItem> = ArrayList()
    var barberLiveData = SingleLiveEvent<MyBookingItem>()
    var cancelLiveData = SingleLiveEvent<MyBookingItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrentBookingsHolder {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemBookingBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_booking, parent, false)
        return CurrentBookingsHolder(binding)
    }

    override fun onBindViewHolder(holder: CurrentBookingsHolder, position: Int) {
        val itemViewModel = ItemBookingViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.binding.barberLayout.setOnClickListener {
            barberLiveData.value = itemViewModel.item
        }

        holder.binding.btnCancel.setOnClickListener {
            cancelLiveData.value = itemViewModel.item
        }
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<MyBookingItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class CurrentBookingsHolder(val binding: ItemBookingBinding) : RecyclerView.ViewHolder(binding.root)
}
