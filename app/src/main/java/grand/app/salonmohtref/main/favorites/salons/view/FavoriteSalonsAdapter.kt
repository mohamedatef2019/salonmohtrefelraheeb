package grand.app.salonmohtref.main.favorites.salons.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import grand.app.salonmohtref.R
import grand.app.salonmohtref.base.BaseApp
import grand.app.salonmohtref.databinding.ItemBranchBinding
import grand.app.salonmohtref.databinding.ItemFavoriteSalonBinding
import grand.app.salonmohtref.main.favorites.response.FavoriteSalonItem
import grand.app.salonmohtref.main.favorites.salons.viewmodel.ItemFavoriteSalonViewModel
import grand.app.salonmohtref.utils.SingleLiveEvent
import timber.log.Timber
import java.util.*

class FavoriteSalonsAdapter : RecyclerView.Adapter<FavoriteSalonsAdapter.BranchesHolder>()
{
    var itemsList: ArrayList<FavoriteSalonItem> = ArrayList()
    var itemLiveData = SingleLiveEvent<FavoriteSalonItem>()
    var favLiveData = SingleLiveEvent<FavoriteSalonItem>()
    var rateLiveData = SingleLiveEvent<FavoriteSalonItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BranchesHolder {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemFavoriteSalonBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_favorite_salon, parent, false)
        return BranchesHolder(binding)
    }

    override fun onBindViewHolder(holder: BranchesHolder, position: Int) {
        val itemViewModel = ItemFavoriteSalonViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.binding.rawLayout.setOnClickListener {
            itemLiveData.value = itemViewModel.item
        }
        holder.binding.ibFav.setOnClickListener {
            favLiveData.value = itemViewModel.item
        }

        holder.binding.ratingLayout.setOnClickListener {
            rateLiveData.value = itemViewModel.item
        }
        holder.setFav()
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<FavoriteSalonItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    fun removeItem(item: FavoriteSalonItem) {
        itemsList.remove(item)
        notifyDataSetChanged()
    }

    fun getItem(pos:Int): FavoriteSalonItem {
        return itemsList[pos]
    }

    fun notifyItemSelected(item : FavoriteSalonItem)
    {
        getItem(itemsList.indexOf(item)).isFavorite = item.isFavorite
        notifyItemChanged(itemsList.indexOf(item))
    }

    inner class BranchesHolder(val binding: ItemFavoriteSalonBinding) : RecyclerView.ViewHolder(binding.root)
    {
        fun setFav(){
            when (getItem(adapterPosition).isFavorite) {
                1 -> {
                    Timber.e("is favorite : true")
                    Glide.with(BaseApp.getInstance).load(R.drawable.ic_fav).into(binding.ibFav)
                }
                else -> {
                    Timber.e("is favorite : false")
                    Glide.with(BaseApp.getInstance).load(R.drawable.ic_un_fav).into(binding.ibFav)
                }
            }
        }
    }
}
