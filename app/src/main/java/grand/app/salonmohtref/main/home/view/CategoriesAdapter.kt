package grand.app.salonmohtref.main.home.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import grand.app.salonmohtref.R
import grand.app.salonmohtref.databinding.ItemHomeCategoryBinding
import grand.app.salonmohtref.main.home.viewmodel.ItemCategoryViewModel
import grand.app.salonmohtref.base.BaseApp
import grand.app.salonmohtref.main.bottombar.home.response.CategoriesItem
import grand.app.salonmohtref.utils.SingleLiveEvent
import java.util.*

class CategoriesAdapter : RecyclerView.Adapter<CategoriesAdapter.CategoriesHolder>()
{
    var itemsList: ArrayList<CategoriesItem> = ArrayList()
    var itemLiveData = SingleLiveEvent<CategoriesItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoriesHolder
    {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemHomeCategoryBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_home_category, parent, false)
        return CategoriesHolder(binding)
    }

    override fun onBindViewHolder(holder: CategoriesHolder, position: Int) {
        val itemViewModel = ItemCategoryViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        Glide.with(BaseApp.getInstance).load(itemViewModel.item.img).into(holder.binding.imgCategory)

        holder.binding.rawLayout.setOnClickListener {
            itemLiveData.value = itemViewModel.item
        }
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<CategoriesItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class CategoriesHolder(val binding: ItemHomeCategoryBinding) : RecyclerView.ViewHolder(binding.root)
}
