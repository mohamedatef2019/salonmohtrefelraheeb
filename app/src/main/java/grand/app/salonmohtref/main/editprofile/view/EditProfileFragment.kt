package grand.app.salonmohtref.main.editprofile.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.net.toUri
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.fxn.pix.Pix
import grand.app.salonmohtref.R
import grand.app.salonmohtref.activity.auth.AuthActivity
import grand.app.salonmohtref.activity.home.BaseHomeFragment
import grand.app.salonmohtref.databinding.FragmentEditProfileBinding
import grand.app.salonmohtref.location.map.MapsActivity
import grand.app.salonmohtref.location.util.AddressItem
import grand.app.salonmohtref.main.editprofile.response.UpdateProfileResponse
import grand.app.salonmohtref.main.editprofile.viewmodel.EditProfileViewModel
import grand.app.salonmohtref.network.Status
import grand.app.salonmohtref.utils.PrefMethods
import grand.app.salonmohtref.utils.constants.Codes
import grand.app.salonmohtref.utils.constants.Const
import grand.app.salonmohtref.utils.constants.Params
import grand.app.salonmohtref.utils.observe
import timber.log.Timber

class EditProfileFragment : BaseHomeFragment()
{
    lateinit var binding : FragmentEditProfileBinding
    lateinit var viewModel : EditProfileViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_edit_profile, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(EditProfileViewModel::class.java)
        binding.viewModel = viewModel

        showBottomBar(false)

        observe(viewModel.mutableLiveData){
            when (it) {
                Codes.LOCATION_CLICKED -> {
                    startActivityForResult(Intent(requireActivity(), MapsActivity::class.java), Codes.GET_LOCATION_FROM_MAP)
                }
                Codes.CHOOSE_IMAGE_CLICKED -> {
                    pickImage(Codes.USER_PROFILE_IMAGE_REQUEST)
                }
                Codes.LOGIN_CLICKED -> {
                    Const.isAskedToLogin = 1
                    requireActivity().startActivity(Intent(requireActivity(), AuthActivity::class.java).putExtra(Const.ACCESS_LOGIN , true))
                }
            }
        }

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is UpdateProfileResponse -> {
                            showToast(it.data.msg.toString(), 2)
                            findNavController().navigateUp()
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Timber.e("mou3az_address : ")

        when(requestCode) {
            Codes.GET_LOCATION_FROM_MAP -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        val userLocation = data.getParcelableExtra<AddressItem>(Params.ADDRESS_ITEM) as AddressItem
                                        binding.etAddress.setText(userLocation.address)
                                        viewModel.request.address = userLocation.address
                                        viewModel.request.lat = userLocation.lat
                                        viewModel.request.lng = userLocation.lng
                                        viewModel.notifyChange()
                                    }
                                }
                            }
                        }
                    }
                }
            }
            Codes.USER_PROFILE_IMAGE_REQUEST -> {
                data?.let {
                    val returnValue = it.getStringArrayListExtra(Pix.IMAGE_RESULTS)
                    returnValue?.let { array ->
                        if (requestCode == Codes.USER_PROFILE_IMAGE_REQUEST) {
                            val returnValue = data.getStringArrayListExtra(Pix.IMAGE_RESULTS)!!
                            // binding.ivUserImg.setImageURI(returnValue[0].toUri())
                            viewModel.obsUserImg.set(returnValue[0].toUri())
                            viewModel.gotImage(requestCode, array[0])
                        }
                    }
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        when (Const.isAskedToLogin) {
            1 -> {
                viewModel.getUserStatus()
                Const.isAskedToLogin = 0
            }
        }
    }
}