package grand.app.salonmohtref.main.bookings.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import grand.app.salonmohtref.activity.home.BaseHomeFragment
import grand.app.salonmohtref.R
import grand.app.salonmohtref.activity.home.MainActivity
import grand.app.salonmohtref.databinding.FragmentMyBookingBinding
import grand.app.salonmohtref.main.bookings.current.view.CurrentBookingsFragment
import grand.app.salonmohtref.main.bookings.old.view.OldBookingsFragment
import grand.app.salonmohtref.main.bookings.viewmodel.MyBookingViewModel
import grand.app.salonmohtref.utils.constants.Codes
import grand.app.salonmohtref.utils.constants.Params
import grand.app.salonmohtref.utils.observe
import grand.app.salonmohtref.utils.viewpager.DepthPageTransformer
import timber.log.Timber

class BookingsFragment : BaseHomeFragment() {
    private lateinit var binding: FragmentMyBookingBinding
    private lateinit var viewModel: MyBookingViewModel
    lateinit var viewPager: ViewPager2

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_my_booking, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(this).get(MyBookingViewModel::class.java)
        binding.viewModel = viewModel

        showBottomBar(false)

        observe(viewModel.mutableLiveData) {
            when (it) {
                Codes.BARBER_ITEM -> {
                    viewPager.currentItem = 0
                }
                Codes.SALONS_ITEM -> {
                    viewPager.currentItem = 1
                }
            }
        }

        initViewPager()
    }

    private fun initViewPager() {

        // Instantiate a ViewPager2 and a PagerAdapter.
        viewPager = binding.vpFavorites

        // The pager adapter, which provides the pages to the view pager widget.
        val pagerAdapter = FavoritePagerAdapter(requireActivity())
        viewPager.adapter = pagerAdapter

        viewPager.setPageTransformer(DepthPageTransformer())

        viewPager.isUserInputEnabled = false

        viewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                viewModel.onPageSelected(position)
            }
        })
    }

    /**
     * A simple pager adapter that represents 5 ScreenSlidePageFragment objects, in
     * sequence.
     */
    private inner class FavoritePagerAdapter(fa: FragmentActivity) : FragmentStateAdapter(fa) {
        override fun getItemCount(): Int = 2

        override fun createFragment(position: Int): Fragment {
            return when (position) {
                0 -> CurrentBookingsFragment()
                else -> OldBookingsFragment()
            }
        }
    }
}
