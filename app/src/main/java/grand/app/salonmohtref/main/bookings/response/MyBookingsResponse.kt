package grand.app.salonmohtref.main.bookings.response

import com.google.gson.annotations.SerializedName
import grand.app.salonmohtref.main.favorites.response.FavoriteBarberItem

data class MyBookingsResponse(

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val myBookingsData: MyBookingsData? = null,

	@field:SerializedName("status")
	val status: String? = null
) 

data class User(

	@field:SerializedName("img")
	val img: String? = null,

	@field:SerializedName("wallet")
	val wallet: Int? = null,

	@field:SerializedName("unique_id")
	val uniqueId: String? = null,

	@field:SerializedName("lng")
	val lng: String? = null,

	@field:SerializedName("jwt")
	val jwt: String? = null,

	@field:SerializedName("package_id")
	val packageId: Any? = null,

	@field:SerializedName("type")
	val type: Int? = null,

	@field:SerializedName("firebase_token")
	val firebaseToken: Any? = null,

	@field:SerializedName("points")
	val points: Int? = null,

	@field:SerializedName("visits")
	val visits: Int? = null,

	@field:SerializedName("social_id")
	val socialId: Any? = null,

	@field:SerializedName("expire_date")
	val expireDate: Any? = null,

	@field:SerializedName("phone")
	val phone: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("email")
	val email: String? = null,

	@field:SerializedName("lat")
	val lat: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
) 

data class Salon(

	@field:SerializedName("license")
	val license: String? = null,

	@field:SerializedName("lng")
	val lng: String? = null,

	@field:SerializedName("min_price")
	val minPrice: Int? = null,

	@field:SerializedName("rate")
	val rate: String? = null,

	@field:SerializedName("rates_count")
	val ratesCount: Int? = null,

	@field:SerializedName("phone")
	val phone: String? = null,

	@field:SerializedName("working_hours")
	val workingHours: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("salon_type")
	val salonType: Int? = null,

	@field:SerializedName("certificate")
	val certificate: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("lat")
	val lat: String? = null,

	@field:SerializedName("desc")
	val desc: String? = null
) 

data class BookingServiceItem(

	@field:SerializedName("duration")
	val duration: Int? = null,

	@field:SerializedName("price")
	val price: Int? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null
) 

data class MyBookingItem(

		@field:SerializedName("date")
	val date: String? = null,

		@field:SerializedName("img")
	val img: String? = null,

		@field:SerializedName("salon_id")
	val salonId: Int? = null,

		@field:SerializedName("user_name")
	val userName: String? = null,

		@field:SerializedName("payment_status")
	val paymentStatus: Int? = null,

		@field:SerializedName("services")
	val servicesList: List<BookingServiceItem?>? = null,

		@field:SerializedName("type")
	val type: Int? = null,

		@field:SerializedName("user_img")
	val userImg: String? = null,

		@field:SerializedName("total")
	val total: Int? = null,

		@field:SerializedName("barber_id")
	val barberId: Int? = null,

		@field:SerializedName("user_id")
	val userId: Int? = null,

		@field:SerializedName("extra_notes")
	val extraNotes: String? = null,

		@field:SerializedName("name")
	val name: String? = null,

		@field:SerializedName("logo")
	val logo: String? = null,

		@field:SerializedName("id")
	val id: Int? = null,

		@field:SerializedName("time")
	val time: String? = null,

		@field:SerializedName("barber")
	val barber: FavoriteBarberItem? = null,

		@field:SerializedName("user")
	val user: User? = null,

		@field:SerializedName("payment_method")
	val paymentMethod: Int? = null,

		@field:SerializedName("status")
	val status: Int? = null,

		@field:SerializedName("desc")
	val desc: String? = null,

		@field:SerializedName("salon")
	val salon: Salon? = null
)

data class MyBookingsData(

	@field:SerializedName("first_page_url")
	val firstPageUrl: String? = null,

	@field:SerializedName("path")
	val path: String? = null,

	@field:SerializedName("per_page")
	val perPage: Int? = null,

	@field:SerializedName("total")
	val total: Int? = null,

	@field:SerializedName("data")
	val myBookingsList: List<MyBookingItem?>? = null,

	@field:SerializedName("last_page")
	val lastPage: Int? = null,

	@field:SerializedName("last_page_url")
	val lastPageUrl: String? = null,

	@field:SerializedName("next_page_url")
	val nextPageUrl: Any? = null,

	@field:SerializedName("from")
	val from: Int? = null,

	@field:SerializedName("to")
	val to: Int? = null,

	@field:SerializedName("prev_page_url")
	val prevPageUrl: Any? = null,

	@field:SerializedName("current_page")
	val currentPage: Int? = null
) 
