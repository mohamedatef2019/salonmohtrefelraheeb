package grand.app.salonmohtref.main.salonbarbers.request

data class AllBarbersRequest (
        var salon_id: Int? = null,
        var date: String? = null,
        var time: String? = null
)