package grand.app.salonmohtref.main.wallet.viewmodel

import androidx.databinding.ObservableField
import grand.app.salonmohtref.R
import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.base.LoadingStatus
import grand.app.salonmohtref.main.wallet.response.WalletHistoryItem
import grand.app.salonmohtref.main.wallet.response.WalletResponse
import grand.app.salonmohtref.main.wallet.view.WalletOrdersAdapter
import grand.app.salonmohtref.network.ApiResponse
import grand.app.salonmohtref.utils.PrefMethods
import grand.app.salonmohtref.utils.constants.Codes
import grand.app.salonmohtref.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.ArrayList

class WalletViewModel : BaseViewModel()
{
    var adapter = WalletOrdersAdapter()
    var obsCredit = ObservableField<String>()

    fun getWallet()
    {
        obsLayout.set(LoadingStatus.SHIMMER)
        requestCall<WalletResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().getWallet() }
        })
        { res ->
            apiResponseLiveData.value = ApiResponse.shimmerLoading(false)
            when (res!!.code) {
                200 -> {
                    obsLayout.set(LoadingStatus.FULL)
                    obsCredit.set("${res.walletData!!.wallet.toString()} ${getString(R.string.label_currency)}")
                    adapter.updateList(res.walletData.historyList as ArrayList<WalletHistoryItem>)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }

    fun onLoginClicked() {
        setValue(Codes.LOGIN_CLICKED)
    }

    fun onAddClicked(){
        setValue(Codes.ADD_CLICKED)
    }

    fun getUserStatus() {
        when {
            PrefMethods.getUserData() != null -> {
                obsIsVisible.set(true)
                getWallet()
            }
            else -> {
                obsLayout.set(LoadingStatus.NOTLOGIN)
            }
        }
    }

    init {
        getUserStatus()
    }
}
