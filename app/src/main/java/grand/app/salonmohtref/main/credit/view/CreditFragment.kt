package grand.app.salonmohtref.main.credit.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import grand.app.salonmohtref.activity.home.BaseHomeFragment
import grand.app.salonmohtref.R
import grand.app.salonmohtref.activity.auth.AuthActivity
import grand.app.salonmohtref.databinding.FragmentMyCreditBinding
import grand.app.salonmohtref.main.credit.response.CreditResponse
import grand.app.salonmohtref.main.credit.viewmodel.CreditViewModel
import grand.app.salonmohtref.main.profile.view.ProfileFragmentDirections
import grand.app.salonmohtref.network.Status
import grand.app.salonmohtref.utils.PrefMethods
import grand.app.salonmohtref.utils.constants.Codes
import grand.app.salonmohtref.utils.constants.Const
import grand.app.salonmohtref.utils.observe
import timber.log.Timber

class CreditFragment : BaseHomeFragment()
{
    lateinit var binding: FragmentMyCreditBinding
    lateinit var viewModel: CreditViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_my_credit, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(CreditViewModel::class.java)
        binding.viewModel = viewModel

        showBottomBar(false)

        observe(viewModel.mutableLiveData){
            when (it) {
                Codes.LOGIN_CLICKED -> {
                    Const.isAskedToLogin = 1
                    requireActivity().startActivity(
                        Intent(requireActivity(), AuthActivity::class.java).putExtra(
                            Const.ACCESS_LOGIN , true))
                }
                Codes.EXCHANGE_CLICKED -> {
                    findNavController().navigate(R.id.credit_to_coupons)
                }
            }
        }

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is CreditResponse -> {
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        when (Const.isAskedToLogin) {
            1 -> {
                viewModel.getUserStatus()
                Const.isAskedToLogin = 0
            }
        }
    }
}