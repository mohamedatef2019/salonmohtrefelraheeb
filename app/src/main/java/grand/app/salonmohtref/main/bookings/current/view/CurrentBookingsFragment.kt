package grand.app.salonmohtref.main.bookings.current.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import grand.app.salonmohtref.activity.home.BaseHomeFragment
import grand.app.salonmohtref.R
import grand.app.salonmohtref.activity.auth.AuthActivity
import grand.app.salonmohtref.activity.home.MainActivity
import grand.app.salonmohtref.auth.login.response.LoginResponse
import grand.app.salonmohtref.auth.login.view.LoginFragmentDirections
import grand.app.salonmohtref.databinding.FragmentCurrentBookingsBinding
import grand.app.salonmohtref.dialogs.cancelbooking.DialogCancelOrderFragment
import grand.app.salonmohtref.main.bookings.current.viewmodel.CurrentBookingsViewModel
import grand.app.salonmohtref.main.bookings.response.MyBookingItem
import grand.app.salonmohtref.main.bookings.response.MyBookingsResponse
import grand.app.salonmohtref.main.bookings.view.BookingsFragmentDirections
import grand.app.salonmohtref.network.Status
import grand.app.salonmohtref.utils.PrefMethods
import grand.app.salonmohtref.utils.Utils
import grand.app.salonmohtref.utils.constants.Codes
import grand.app.salonmohtref.utils.constants.Const
import grand.app.salonmohtref.utils.constants.Params
import grand.app.salonmohtref.utils.observe
import timber.log.Timber

class CurrentBookingsFragment : BaseHomeFragment()
{
    private lateinit var binding : FragmentCurrentBookingsBinding
    private lateinit var viewModel: CurrentBookingsViewModel
    var bookingItem = MyBookingItem()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_current_bookings, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(this).get(CurrentBookingsViewModel::class.java)
        binding.viewModel = viewModel

//        binding.swipeLayout.setOnRefreshListener {
//            binding.swipeLayout.isRefreshing = false
//            viewModel.getCurrentBookings()
//        }

        Params.mutableActionLiveData.observe(requireActivity(), Observer {
            when (it) {
                1 -> {
                    Params.mutableActionLiveData.value = 0
                    viewModel.cancelOrder(bookingItem)
                }
            }
        })

        observe(viewModel.adapter.barberLiveData){
            val action = BookingsFragmentDirections.bookingsToBarberProfile(it?.barber!!)
            findNavController().navigate(action)
        }

        viewModel.adapter.cancelLiveData.observe(viewLifecycleOwner, Observer {
            bookingItem = it!!
            Utils.startDialogActivity(requireActivity(), DialogCancelOrderFragment::class.java.name, Codes.DIALOG_CANCEL_BOOKING, null)
        })

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is MyBookingsResponse -> {
                            binding.rvSalons.smoothScrollToPosition(0)
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }

        observe(viewModel.mutableLiveData) {
            when (it) {
                Codes.LOGIN_CLICKED -> {
                    Const.isAskedToLogin = 1
                    requireActivity().startActivity(Intent(requireActivity(), AuthActivity::class.java).putExtra(Const.ACCESS_LOGIN, true))
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when(requestCode) {
            Codes.DIALOG_CANCEL_BOOKING-> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                           viewModel.cancelOrder(bookingItem)
                                    }
                                }
                            }
                        }
                    }
                }
            }
            Codes.DIALOG_LOGIN_REQUEST -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        Const.isAskedToLogin = 1
                                        requireActivity().startActivity(Intent(requireActivity(), AuthActivity::class.java).putExtra(Const.ACCESS_LOGIN, true))
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        when (Const.isAskedToLogin) {
            1 -> {
                viewModel.getUserStatus()
                Const.isAskedToLogin = 0
            }
        }
    }
}