package grand.app.salonmohtref.main.favorites.salons.viewmodel

import androidx.databinding.ObservableField
import grand.app.salonmohtref.R
import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.main.favorites.response.FavoriteSalonItem

class ItemFavoriteSalonViewModel(var item: FavoriteSalonItem) : BaseViewModel()
{
    var obsPrices = ObservableField<String>()
    var obsTimes = ObservableField<String>()
    var obsRate : Float = 0f

    init {
        obsRate = item.rate!!.toFloat()
        obsPrices.set(getSalonPrices(item.minPrice.toString()))
        obsTimes.set(getSalonTimes(item.startTime.toString(), item.endTime.toString()))
    }

    private fun getSalonPrices(price : String) : String {

        return getString(R.string.label_prices_start_from) + " " + price + " " + getString(R.string.label_currency)
    }

    private fun getSalonTimes(startTime : String, endTime : String) : String {

        return getString(R.string.label_working_houser_start_from) + " " + startTime + " " + getString(R.string.label_to) + " " + endTime
    }
}