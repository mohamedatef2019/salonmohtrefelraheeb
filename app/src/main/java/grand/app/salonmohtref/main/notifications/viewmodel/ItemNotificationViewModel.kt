package grand.app.salonmohtref.main.notifications.viewmodel

import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.main.notifications.response.NotificationItem

class ItemNotificationViewModel(var item: NotificationItem) : BaseViewModel()