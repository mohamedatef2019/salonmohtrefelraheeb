package grand.app.salonmohtref.main.barberprofile.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.salonmohtref.R
import grand.app.salonmohtref.databinding.ItemBarberWorkingBinding
import grand.app.salonmohtref.main.barberprofile.viewmodel.ItemBarberWorkingViewModel
import grand.app.salonmohtref.main.favorites.response.BarberWorkingItem
import java.util.*

class BarberWorkingAdapter : RecyclerView.Adapter<BarberWorkingAdapter.CurrentBookingsHolder>()
{
    var itemsList: ArrayList<BarberWorkingItem> = ArrayList()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrentBookingsHolder {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemBarberWorkingBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_barber_working, parent, false)
        return CurrentBookingsHolder(binding)
    }

    override fun onBindViewHolder(holder: CurrentBookingsHolder, position: Int) {
        val itemViewModel = ItemBarberWorkingViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<BarberWorkingItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class CurrentBookingsHolder(val binding: ItemBarberWorkingBinding) : RecyclerView.ViewHolder(binding.root)
}
