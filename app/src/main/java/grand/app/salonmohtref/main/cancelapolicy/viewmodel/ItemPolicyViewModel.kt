package grand.app.salonmohtref.main.cancelapolicy.viewmodel

import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.main.cancelapolicy.response.CancellationPolicyItem

class ItemPolicyViewModel(var item: CancellationPolicyItem) : BaseViewModel()