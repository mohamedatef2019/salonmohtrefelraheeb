package grand.app.salonmohtref.main.notifications.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.salonmohtref.R
import grand.app.salonmohtref.databinding.ItemNotificationBinding
import grand.app.salonmohtref.main.notifications.response.NotificationItem
import grand.app.salonmohtref.main.notifications.viewmodel.ItemNotificationViewModel
import grand.app.salonmohtref.utils.SingleLiveEvent
import java.util.*

class NotificationsAdapter : RecyclerView.Adapter<NotificationsAdapter.TermsHolder>()
{
    var itemsList: ArrayList<NotificationItem> = ArrayList()
    var itemLiveData  = SingleLiveEvent<NotificationItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TermsHolder
    {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemNotificationBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_notification, parent, false)
        return TermsHolder(binding)
    }

    override fun onBindViewHolder(holder: TermsHolder, position: Int) {
        val itemViewModel = ItemNotificationViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.binding.rawLayout.setOnClickListener {
            itemLiveData.value = itemViewModel.item
        }
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<NotificationItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class TermsHolder(val binding: ItemNotificationBinding) : RecyclerView.ViewHolder(binding.root)
}
