package grand.app.salonmohtref.main.branches.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import grand.app.salonmohtref.R
import grand.app.salonmohtref.base.BaseApp
import grand.app.salonmohtref.databinding.ItemBranchBinding
import grand.app.salonmohtref.main.allsalonss.response.SalonItem
import grand.app.salonmohtref.main.branches.viewmodel.ItemSalonViewModel
import grand.app.salonmohtref.utils.SingleLiveEvent
import timber.log.Timber
import java.util.*

class SalonsAdapter : RecyclerView.Adapter<SalonsAdapter.BranchesHolder>()
{
    var itemsList: ArrayList<SalonItem> = ArrayList()
    var itemLiveData = SingleLiveEvent<SalonItem>()
    var favLiveData = SingleLiveEvent<SalonItem>()
    var rateLiveData = SingleLiveEvent<SalonItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BranchesHolder {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemBranchBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_branch, parent, false)
        return BranchesHolder(binding)
    }

    override fun onBindViewHolder(holder: BranchesHolder, position: Int) {
        val itemViewModel = ItemSalonViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.binding.rawLayout.setOnClickListener {
            itemLiveData.value = itemViewModel.item
        }
        holder.binding.ibFav.setOnClickListener {
            favLiveData.value = itemViewModel.item
        }

        holder.binding.ratingLayout.setOnClickListener {
            rateLiveData.value = itemViewModel.item
        }
        holder.setFav()
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<SalonItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    fun getItem(pos:Int): SalonItem {
        return itemsList[pos]
    }

    fun notifyItemSelected(item : SalonItem)
    {
        getItem(itemsList.indexOf(item)).isFavorite = item.isFavorite
        notifyItemChanged(itemsList.indexOf(item))
    }

    inner class BranchesHolder(val binding: ItemBranchBinding) : RecyclerView.ViewHolder(binding.root)
    {
        fun setFav(){
            Timber.e("is favorite : " + getItem(adapterPosition).isFavorite)
            when (getItem(adapterPosition).isFavorite) {
                1 -> {
                    Timber.e("is favorite : true")
                    Glide.with(BaseApp.getInstance).load(R.drawable.ic_fav).into(binding.ibFav)
                }
                else -> {
                    Timber.e("is favorite : false")
                    Glide.with(BaseApp.getInstance).load(R.drawable.ic_un_fav).into(binding.ibFav)
                }
            }
        }
    }
}
