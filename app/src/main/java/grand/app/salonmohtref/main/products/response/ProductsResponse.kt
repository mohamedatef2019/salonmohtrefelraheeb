package grand.app.salonmohtref.main.bottombar.products.response

import com.google.gson.annotations.SerializedName

data class ProductsResponse(

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val productsList: List<ProductItem?>? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class ProductItem(

	@field:SerializedName("img")
	val img: String? = null,

	@field:SerializedName("price")
	val price: Int? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("desc")
	val desc: String? = null
)
