package grand.app.salonmohtref.main.suggestion.viewmodel

import grand.app.salonmohtref.auth.splash.response.SettingResponse
import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.main.suggestion.request.SuggestRequest
import grand.app.salonmohtref.main.suggestion.response.SuggestResponse
import grand.app.salonmohtref.network.ApiResponse
import grand.app.salonmohtref.utils.PrefMethods
import grand.app.salonmohtref.utils.constants.Codes
import grand.app.salonmohtref.utils.isEmailValid
import grand.app.salonmohtref.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class SuggestViewModel : BaseViewModel()
{
    var request = SuggestRequest()

    init {
        when {
            PrefMethods.getSettingData() == null -> {
                requestCall<SettingResponse?>({
                    withContext(Dispatchers.IO) { return@withContext getApiRepo().getSocialLinks() }
                })
                { res ->
                    when (res!!.code) {
                        200 -> {
                            PrefMethods.saveSettingData(res.settingData!!)
                            notifyChange()
                        }
                    }
                }
            }
        }
    }

    fun onSendClicked() {
        setClickable()
        when {
            request.name.isNullOrEmpty() || request.name.isNullOrBlank() -> {
                setValue(Codes.EMPTY_NAME)
            }
            request.email.isNullOrEmpty() || request.email.isNullOrBlank() -> {
                setValue(Codes.EMAIL_EMPTY)
            }
            !isEmailValid(request.email!!) -> {
                setValue(Codes.INVALID_EMAIL)
            }
            request.phone.isNullOrEmpty() || request.phone.isNullOrBlank() -> {
                setValue(Codes.EMPTY_PHONE)
            }
            request.message.isNullOrEmpty() || request.message.isNullOrBlank() -> {
                setValue(Codes.EMPTY_MESSAGE)
            }
            else -> {
                sendHelpMsg()
            }
        }
    }

    private fun sendHelpMsg()
    {
        obsIsProgress.set(true)
        requestCall<SuggestResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().sendSuggest(request) }
        })
        { res ->
            obsIsProgress.set(false)
            when (res!!.code) {
                200 -> {
                    apiResponseLiveData.value = ApiResponse.success(res)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }

    fun onFacebookClicked(){
        setValue(Codes.FACE_CLICKED)
    }

    fun onTwitterClicked(){
        setValue(Codes.TWITTER_CLICKED)
    }

    fun onWhatsClicked(){
        setValue(Codes.WHATSAPP_CLICKED)
    }
}