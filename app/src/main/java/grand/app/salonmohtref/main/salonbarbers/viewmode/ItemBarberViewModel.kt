package grand.app.salonmohtref.main.salonbarbers.viewmode

import androidx.databinding.ObservableField
import grand.app.salonmohtref.R
import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.main.salonbarbers.response.SalonBarberItem

class ItemBarberViewModel(var item: SalonBarberItem?) : BaseViewModel()
{
    var obsWorkingHours = ObservableField<String>()

    init {
        try {
            obsWorkingHours.set(getSalonTimes(item?.startTime!!, item?.endTime!!))
        } catch (e: Exception) {
            // handler
        }
    }

    private fun getSalonTimes(startTime : String, endTime : String) : String {
        return getString(R.string.label_working_houser_start_from) + " " + startTime + " " + getString(R.string.label_to) + " " + endTime
    }
}
