package grand.app.salonmohtref.main.ordertime.viewmodel

import androidx.databinding.ObservableField
import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.main.confirmorder.request.OrderRequest
import grand.app.salonmohtref.main.ordertime.time.OrderTimesAdapter
import grand.app.salonmohtref.main.salondetails.response.SalonDetailsData
import grand.app.salonmohtref.utils.constants.Codes

class OrderTimeViewModel : BaseViewModel()
{
    var salonDetails = SalonDetailsData()
    var orderRequest = OrderRequest()
    var obsNote = ObservableField<String>()
    var timesAdapter = OrderTimesAdapter()
    var obsDay = ObservableField<String>()
    var obstime = ObservableField<String>()

    fun onConfirmClicked() {
        when {
            obsDay.get() == null -> {
                setValue(Codes.EMPTY_DATE)
            }
            obstime.get() == null -> {
                setValue(Codes.EMPTY_TIME)
            }
            else -> {
                setValue(Codes.TIME_SELECTED)
            }
        }
    }
}