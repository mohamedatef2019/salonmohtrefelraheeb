package grand.app.salonmohtref.main.mypackage.viewmodel

import androidx.databinding.ObservableField
import grand.app.salonmohtref.R
import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.base.LoadingStatus
import grand.app.salonmohtref.main.mypackage.response.MyPackageResponse
import grand.app.salonmohtref.main.mypackage.response.PackageDetailsData
import grand.app.salonmohtref.main.mypackage.view.PackageServiceAdapter
import grand.app.salonmohtref.main.package_details.response.UpgradePackageResponse
import grand.app.salonmohtref.main.packages.response.PackageItem
import grand.app.salonmohtref.main.packages.response.PackageServiceItem
import grand.app.salonmohtref.network.ApiResponse
import grand.app.salonmohtref.utils.PrefMethods
import grand.app.salonmohtref.utils.constants.Codes
import grand.app.salonmohtref.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.util.ArrayList

class MyPackageViewModel : BaseViewModel()
{
    var adapter = PackageServiceAdapter()
    var packageDetails = PackageDetailsData()
    var obsNextPackageBtn = ObservableField<String>()
    var obsShowNextBtn = ObservableField<Boolean>()
    var obsPeriod = ObservableField<String>()
    var obsPrice = ObservableField<String>()
    var obsVisitCount = ObservableField<String>()
    var obsUpgradeText = ObservableField<String>()

    fun getMyPackage()
    {
        Timber.e("mou3aaaz_packages" + " get packages")
        obsLayout.set(LoadingStatus.SHIMMER)
        requestCall<MyPackageResponse?>({ withContext(Dispatchers.IO) { return@withContext getApiRepo().getMyPackage() } })
        { res ->
            apiResponseLiveData.value = ApiResponse.shimmerLoading(false)
            when (res!!.code) {
                200 -> {
                    when (res.packageDetails?.servicesList?.size) {
                        0 -> {
                            Timber.e("mou3aaaz_packages" + " empty")
                            obsIsVisible.set(true)
                            obsLayout.set(LoadingStatus.EMPTY)
                        }
                        else -> {
                            obsLayout.set(LoadingStatus.FULL)
                            Timber.e("mou3aaaz_packages response full")

                            packageDetails = res.packageDetails!!
                            obsNextPackageBtn.set("""ترقية الي الباقة$ {res.packageDetails.nextPackageName} ${res.packageDetails.nextPackagePrice} ${getString(R.string.label_currency)}""")

                            when (res.packageDetails.nextPackageId) {
                                0 -> {
                                    obsShowNextBtn.set(false)
                                }
                                else -> {
                                    obsShowNextBtn.set(true)
                                    obsUpgradeText.set(getString(R.string.label_upgrade_to) + " " + res.packageDetails.nextPackageName + " "
                                            + res.packageDetails.nextPackagePrice + " " + getString(R.string.label_currency))
                                }
                            }

                            obsPrice.set(res.packageDetails.price.toString())
                            obsPeriod.set("""  ${res.packageDetails.period} ${getString(R.string.label_months)} ${getString(R.string.label_added_to)} ${res.packageDetails.freeMonths} ${getString(R.string.label_free_months)}""")
                            obsVisitCount.set("""  ${res.packageDetails.visits!!} ${getString(R.string.label_visits)}""")
                            adapter.updateList(res.packageDetails.servicesList as ArrayList<PackageServiceItem>)
                            notifyChange()
                        }
                    }
                }
                404 -> {
                    obsLayout.set(LoadingStatus.EMPTY)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }

    private fun upgradePackage()
    {
        obsLayout.set(LoadingStatus.SHIMMER)
        requestCall<UpgradePackageResponse?>({ withContext(Dispatchers.IO) { return@withContext getApiRepo().upgradePackage(packageDetails.nextPackageId!!) } })
        { res ->
            apiResponseLiveData.value = ApiResponse.shimmerLoading(false)
            when (res!!.code) {
                200 -> {
                    apiResponseLiveData.value = ApiResponse.successMessage(res.msg)
                    getMyPackage()
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }

    fun onLoginClicked() {
        setValue(Codes.LOGIN_CLICKED)
    }

    fun onUpgradeClicked() {
        upgradePackage()
    }

    fun onShowAll() {
        setValue(Codes.SHOW_ALL_PACKAGES)
    }

    fun getUserStatus() {
        when {
            PrefMethods.getUserData() != null -> {
                getMyPackage()
            }
            else -> {
                obsIsVisible.set(false)
                obsLayout.set(LoadingStatus.NOTLOGIN)
            }
        }
    }

    init {
        getUserStatus()
    }
}
