package grand.app.salonmohtref.main.activitycoupons.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.salonmohtref.R
import grand.app.salonmohtref.databinding.ItemCouponUsedServicesBinding
import grand.app.salonmohtref.main.activitycoupons.viewmodel.ItemCouponUsedServiceViewModel
import grand.app.salonmohtref.main.coupons.response.UserServicesItem
import grand.app.salonmohtref.utils.SingleLiveEvent
import timber.log.Timber
import java.util.*

class UsedServicesAdapter : RecyclerView.Adapter<UsedServicesAdapter.UsedServicesHolder>()
{
    var itemsList: ArrayList<UserServicesItem> = ArrayList()
    var itemLiveData = SingleLiveEvent<UserServicesItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UsedServicesHolder
    {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemCouponUsedServicesBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_coupon_used_services, parent, false)
        return UsedServicesHolder(binding)
    }

    override fun onBindViewHolder(holder: UsedServicesHolder, position: Int) {
        val itemViewModel = ItemCouponUsedServiceViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.binding.rawLayout.setOnClickListener {
            itemLiveData.value = itemViewModel.item
        }
    }

    fun getItem(pos:Int): UserServicesItem {
        return itemsList[pos]
    }

    fun notifyItemAdded(item : UserServicesItem)
    {
        itemsList.forEach {
            if (item.id == it.id)
            {
                it.count = it.count?.plus(1)
                notifyDataSetChanged()
                return
            }
        }
        itemsList.add(item)
        notifyDataSetChanged()
    }

    fun notifyItemRemoved(item : UserServicesItem)
    {
        val pos = itemsList.indexOf(item)
        itemsList.forEach {
            if (item.id == it.id && it.count!! > 1)
            {
                it.count = it.count?.minus(1)
                notifyDataSetChanged()
                return
            }
        }
        itemsList.removeAt(pos)

        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: List<UserServicesItem?>?) {
        itemsList = models as ArrayList<UserServicesItem>
        notifyDataSetChanged()
    }

    inner class UsedServicesHolder(val binding: ItemCouponUsedServicesBinding) : RecyclerView.ViewHolder(binding.root)
}