package grand.app.salonmohtref.main.aboutapp.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.salonmohtref.R
import grand.app.salonmohtref.databinding.ItemAboutAppBinding
import grand.app.salonmohtref.main.aboutapp.response.AboutItem
import grand.app.salonmohtref.main.aboutapp.viewmodel.ItemAboutViewModel
import grand.app.salonmohtref.main.cancelapolicy.viewmodel.ItemPolicyViewModel
import grand.app.salonmohtref.main.cancelapolicy.view.PolicyAdapter
import java.util.*

class AboutAdapter : RecyclerView.Adapter<AboutAdapter.AboutHolder>()
{
    var itemsList: ArrayList<AboutItem> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AboutHolder
    {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemAboutAppBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_about_app, parent, false)
        return AboutHolder(binding)
    }

    override fun onBindViewHolder(holder: AboutHolder, position: Int) {
        val itemViewModel = ItemAboutViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<AboutItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class AboutHolder(val binding: ItemAboutAppBinding) : RecyclerView.ViewHolder(binding.root)
}
