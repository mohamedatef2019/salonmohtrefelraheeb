package grand.app.salonmohtref.main.ordertime.time

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.salonmohtref.R
import grand.app.salonmohtref.base.BaseApp
import grand.app.salonmohtref.databinding.ItemOrderTimeBinding
import grand.app.salonmohtref.utils.SingleLiveEvent
import kotlin.collections.ArrayList

class OrderTimesAdapter : RecyclerView.Adapter<OrderTimesAdapter.OrderTimesHolder>()
{
    var itemsList: ArrayList<ItemSalonTime> = ArrayList()
    var itemLiveData = SingleLiveEvent<ItemSalonTime>()
    var selectedPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderTimesHolder
    {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemOrderTimeBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_order_time, parent, false)
        return OrderTimesHolder(binding)
    }

    override fun onBindViewHolder(holder: OrderTimesHolder, position: Int)
    {
        val itemViewModel = ItemTimeViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.setOpen()
        holder.setSelected()
        holder.binding.timeLayout.setOnClickListener {
            when {
                position != selectedPosition -> {
                    notifyItemChanged(position)
                    notifyItemChanged(selectedPosition)
                    selectedPosition = position
                    itemLiveData.value = itemViewModel.item
                }
            }
        }

        when (itemsList[position].isSelected) {
            false -> {
                holder.binding.rawLayout.background = ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.unselected_time)
                holder.binding.tvTime.setTextColor(ContextCompat.getColor(BaseApp.getInstance, R.color.color_primary))
            }
        }

        holder.binding.timeLayout.setOnClickListener {
            when (itemViewModel.item.isSelected) {
                true -> {
                    when {
                        position != selectedPosition -> {
                            notifyItemChanged(position)
                            notifyItemChanged(selectedPosition)
                            selectedPosition = position
                            itemLiveData.value = itemViewModel.item
                        }
                    }
                }
            }
        }
    }

    fun getItem(pos:Int): ItemSalonTime {
        return itemsList[pos]
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<ItemSalonTime>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class OrderTimesHolder(val binding: ItemOrderTimeBinding) : RecyclerView.ViewHolder(binding.root)
    {
        fun setSelected()
        {
            when (selectedPosition) {
                adapterPosition -> {
                    binding.rawLayout.background = ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.selected_time)
                    binding.tvTime.setTextColor(ContextCompat.getColor(BaseApp.getInstance, R.color.white))
                }
                else -> {
                    binding.rawLayout.background = ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.unselected_time)
                    binding.tvTime.setTextColor(ContextCompat.getColor(BaseApp.getInstance, R.color.color_primary))
                }
            }
        }

        fun setOpen() {
            when (getItem(adapterPosition).isSelected) {
                false -> {
                    binding.rawLayout.background = ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.unselected_time)
                    binding.tvTime.setTextColor(ContextCompat.getColor(BaseApp.getInstance, R.color.grey_subcategory))
                }
            }
        }
    }
}
