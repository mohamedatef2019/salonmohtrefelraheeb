package grand.app.salonmohtref.main.search.viewmodel

import androidx.databinding.ObservableField
import grand.app.salonmohtref.R
import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.base.LoadingStatus
import grand.app.salonmohtref.main.search.request.SearchRequest
import grand.app.salonmohtref.main.search.response.SearchItem
import grand.app.salonmohtref.main.search.response.SearchResponse
import grand.app.salonmohtref.main.search.view.FilterAdapter
import grand.app.salonmohtref.main.search.view.FilterItem
import grand.app.salonmohtref.main.search.view.SearchAdapter
import grand.app.salonmohtref.network.ApiResponse
import grand.app.salonmohtref.utils.PrefMethods
import grand.app.salonmohtref.utils.constants.Codes
import grand.app.salonmohtref.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.ArrayList

class SearchViewModel : BaseViewModel()
{
    var adapter = SearchAdapter()
    var obsName = ObservableField<String>()
    var request = SearchRequest()
    var filterAdapter = FilterAdapter()
    var isFilter : Boolean = false

    init {
        val filterList = arrayListOf(
                FilterItem(0 , getString(R.string.label_top_rated), 0),
                FilterItem(1 , getString(R.string.label_newest), 0),
                FilterItem(2 , getString(R.string.label_nearest), 0)
        )

        filterAdapter.updateList(filterList)
    }

    fun onSearchClicked() {
        obsIsVisible.set(false)
        when {
            obsName.get() == null -> {
                setValue(Codes.NAME_EMPTY)
            }
            else -> {
                when {
                    PrefMethods.getUserData() != null -> {
                        searchLoginSalons()
                    }
                    else -> {
                        searchSkipSalons()
                    }
                }
            }
        }
    }

    fun onFilterClicked() {
        when (isFilter) {
            false -> {
                obsIsVisible.set(true)
                isFilter = true
            }
            else -> {
                obsIsVisible.set(false)
               isFilter = false
            }
        }
    }

    fun searchSkipSalons()
    {
        obsIsProgress.set(true)
        request.name = obsName.get()

        requestCall<SearchResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().searchSkipSalons(request) }
        })
        { res ->
            obsIsProgress.set(false)
            when (res!!.code) {
                200 -> {
                    when {
                        res.searchData!!.searchList!!.isEmpty() -> {
                            obsLayout.set(LoadingStatus.EMPTY)
                            adapter.clearList()
                        }
                        else -> {
                            obsLayout.set(LoadingStatus.FULL)
                            adapter.updateList(res.searchData.searchList as ArrayList<SearchItem>)
                        }
                    }
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }

    fun searchLoginSalons()
    {
        obsIsProgress.set(true)
        request.name = obsName.get()

        requestCall<SearchResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().searchLoginSalons(request) }
        })
        { res ->
            obsIsProgress.set(false)
            when (res!!.code) {
                200 -> {
                    when {
                        res.searchData!!.searchList!!.isEmpty() -> {
                            obsLayout.set(LoadingStatus.EMPTY)
                            adapter.clearList()
                        }
                        else -> {
                            obsLayout.set(LoadingStatus.FULL)
                            adapter.updateList(res.searchData.searchList as ArrayList<SearchItem>)
                        }
                    }
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }
}