package grand.app.salonmohtref.main.products.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import grand.app.salonmohtref.R
import grand.app.salonmohtref.activity.home.BaseHomeFragment
import grand.app.salonmohtref.databinding.FragmentProductsBinding
import grand.app.salonmohtref.main.bottombar.products.response.ProductsResponse
import grand.app.salonmohtref.main.products.viewmodel.ProductsViewModel
import grand.app.salonmohtref.network.Status
import grand.app.salonmohtref.utils.initViewModel
import grand.app.salonmohtref.utils.observe
import timber.log.Timber

class ProductsFragment : BaseHomeFragment()
{
    lateinit var binding: FragmentProductsBinding
    lateinit var viewModel: ProductsViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_products, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = initViewModel {
            binding.viewModel = this
        }

        showBottomBar(true)

        viewModel.adapter.itemLiveData.observe(viewLifecycleOwner, Observer {
            findNavController().navigate(ProductsFragmentDirections.productsToDetails(it?.id!!))
        })

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is ProductsResponse -> {
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }
    }
}