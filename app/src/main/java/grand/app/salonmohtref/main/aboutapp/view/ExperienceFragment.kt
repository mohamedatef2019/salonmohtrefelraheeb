package grand.app.salonmohtref.main.aboutapp.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import grand.app.salonmohtref.R
import grand.app.salonmohtref.activity.home.BaseHomeFragment
import grand.app.salonmohtref.databinding.FragmentAboutAppBinding
import grand.app.salonmohtref.databinding.FragmentOurExperianceBinding
import grand.app.salonmohtref.main.aboutapp.response.AboutAppResponse
import grand.app.salonmohtref.main.aboutapp.viewmodel.AboutViewModel
import grand.app.salonmohtref.main.aboutapp.viewmodel.ExperienceViewModel
import grand.app.salonmohtref.main.barberprofile.view.BarberProfileFragmentArgs
import grand.app.salonmohtref.main.barberprofile.view.BarberProfileFragmentDirections
import grand.app.salonmohtref.main.favorites.response.FavoriteBarberItem
import grand.app.salonmohtref.network.Status
import grand.app.salonmohtref.utils.observe
import timber.log.Timber

class ExperienceFragment : BaseHomeFragment()
{
    lateinit var binding: FragmentOurExperianceBinding
    lateinit var viewModel: ExperienceViewModel
    val fragmentArgs : BarberProfileFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_our_experiance, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(ExperienceViewModel::class.java)
        binding.viewModel = viewModel

        showBottomBar(false)

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {

                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is AboutAppResponse -> {
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }

        viewModel.barberAdapter.itemLiveData.observe(viewLifecycleOwner, Observer {

//            findNavController().navigate(BarberProfileFragmentDirections.barberProfileToSalonDetails(
//                it!!.salonId!!, -1))

            val item = FavoriteBarberItem()
            item.id=it?.id
            item.salonName = it?.salonName
            item.img = it?.img
            item.name = it?.name
            item.startTime = it?.startTime
            item.endTime = it?.endTime
            item.salonId = it?.salonId
            item.salonName = it?.salonName
            findNavController().navigate(ExperienceFragmentDirections.actionNavigationExperienceToNavigationBarberProfile(item))
        })
    }
}