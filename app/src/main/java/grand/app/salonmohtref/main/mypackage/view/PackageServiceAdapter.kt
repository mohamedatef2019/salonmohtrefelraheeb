package grand.app.salonmohtref.main.mypackage.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.salonmohtref.R
import grand.app.salonmohtref.databinding.ItemPackageServiceBinding
import grand.app.salonmohtref.main.mypackage.viewmodel.ItemPackageServiceViewModel
import grand.app.salonmohtref.main.packages.response.PackageServiceItem
import grand.app.salonmohtref.utils.SingleLiveEvent
import java.util.*

class PackageServiceAdapter : RecyclerView.Adapter<PackageServiceAdapter.PackageServiceHolder>()
{
    var itemsList: ArrayList<PackageServiceItem> = ArrayList()
    var itemLiveData = SingleLiveEvent<PackageServiceItem>()
    var visitCount : Int = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PackageServiceHolder
    {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemPackageServiceBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_package_service, parent, false)
        return PackageServiceHolder(binding)
    }

    override fun onBindViewHolder(holder: PackageServiceHolder, position: Int) {
        val itemViewModel = ItemPackageServiceViewModel(visitCount, itemsList[position])
        holder.binding.viewModel = itemViewModel

    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<PackageServiceItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class PackageServiceHolder(val binding: ItemPackageServiceBinding) : RecyclerView.ViewHolder(binding.root)
}
