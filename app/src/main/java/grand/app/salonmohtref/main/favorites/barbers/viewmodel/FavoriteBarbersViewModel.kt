package grand.app.salonmohtref.main.favorites.barbers.viewmodel

import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.base.LoadingStatus
import grand.app.salonmohtref.main.favorites.barbers.view.FavoriteBarbersAdapter
import grand.app.salonmohtref.main.favorites.response.AddToFavResponse
import grand.app.salonmohtref.main.favorites.response.FavoriteBarberItem
import grand.app.salonmohtref.main.favorites.response.FavoriteBarberResponse
import grand.app.salonmohtref.network.ApiResponse
import grand.app.salonmohtref.utils.PrefMethods
import grand.app.salonmohtref.utils.constants.Codes
import grand.app.salonmohtref.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.ArrayList

class FavoriteBarbersViewModel : BaseViewModel() {

    var adapter = FavoriteBarbersAdapter()
    var barbersList = ArrayList<FavoriteBarberItem>()

    private fun getFavBarbers() {

        obsLayout.set(LoadingStatus.SHIMMER)
        requestCall<FavoriteBarberResponse?>({ withContext(Dispatchers.IO) { return@withContext getApiRepo().getFavoriteBarbers() } })
        { res ->
            when (res!!.code) {
                200 -> {
                    apiResponseLiveData.value = ApiResponse.success(res)
                    when {
                        res.favoriteData?.barbersList.isNullOrEmpty() || res.favoriteData?.barbersList!!.isEmpty() -> {
                            obsLayout.set(LoadingStatus.EMPTY)
                        }
                        else -> {
                            obsLayout.set(LoadingStatus.FULL)
                            barbersList = res.favoriteData.barbersList as ArrayList<FavoriteBarberItem>
                            adapter.updateList(barbersList)
                        }
                    }
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }

    fun addToFav(barberItem : FavoriteBarberItem) {
        obsIsProgress.set(true)
        requestCall<AddToFavResponse?>({ withContext(Dispatchers.IO) { return@withContext getApiRepo().addToFav(barberItem.id!!, 1) } })
        { res ->
            obsIsProgress.set(false)
            when (res!!.code) {
                200 -> {
                    apiResponseLiveData.value = ApiResponse.success(res)
                    apiResponseLiveData.value = ApiResponse.success(res)
                    adapter.removeItem(barberItem)
                    barbersList.remove(barberItem)
                    apiResponseLiveData.value = ApiResponse.success(res)

                    when (barbersList.size) {
                        0 -> {
                            obsLayout.set(LoadingStatus.EMPTY)
                        }
                        else -> {
                            obsLayout.set(LoadingStatus.FULL)
                        }
                    }
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }

    fun onLoginClicked() {
        setValue(Codes.LOGIN_CLICKED)
    }

    fun getUserStatus() {
        when {
            PrefMethods.getUserData() != null -> {
                getFavBarbers()
            }
            else -> {
                obsLayout.set(LoadingStatus.NOTLOGIN)
            }
        }
    }

    init {
        getUserStatus()
    }
}