package grand.app.salonmohtref.main.coupondetails.viewmodel

import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.main.coupons.response.CouponServiceItem

class ItemCouponDetailsServiceViewModel(var item: CouponServiceItem) : BaseViewModel()