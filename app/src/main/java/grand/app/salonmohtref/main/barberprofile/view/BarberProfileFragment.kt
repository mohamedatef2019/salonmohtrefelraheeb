package grand.app.salonmohtref.main.barberprofile.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import grand.app.salonmohtref.activity.home.BaseHomeFragment
import grand.app.salonmohtref.R
import grand.app.salonmohtref.databinding.FragmentBarberProfileBinding
import grand.app.salonmohtref.main.barberprofile.viewmodel.BarberProfileViewModel
import grand.app.salonmohtref.main.bookings.response.CancelOrderResponse
import grand.app.salonmohtref.network.Status
import grand.app.salonmohtref.utils.constants.Codes
import grand.app.salonmohtref.utils.observe
import timber.log.Timber

class BarberProfileFragment : BaseHomeFragment()
{
    lateinit var binding: FragmentBarberProfileBinding
    lateinit var viewModel: BarberProfileViewModel
    val fragmentArgs : BarberProfileFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_barber_profile, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(BarberProfileViewModel::class.java)
        binding.viewModel = viewModel

        showBottomBar(false)

        viewModel.setBarberData(fragmentArgs.barberData)

        observe(viewModel.mutableLiveData){
            when (it) {
                Codes.SALON_CLICKED -> {
                    findNavController().navigate(BarberProfileFragmentDirections.barberProfileToSalonDetails(fragmentArgs.barberData.salonId!!, -1))
                }
            }
        }

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is CancelOrderResponse -> {
                            showToast(it.data.msg.toString(), 2)
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }
    }
}