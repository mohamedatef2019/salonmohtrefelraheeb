package grand.app.salonmohtref.main.package_details.view

data class PackagePaymentItem(
        var id: Int? = null,
        var name: String? = null
)