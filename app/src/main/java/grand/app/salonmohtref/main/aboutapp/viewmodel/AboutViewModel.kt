package grand.app.salonmohtref.main.aboutapp.viewmodel

import androidx.databinding.ObservableField
import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.base.LoadingStatus
import grand.app.salonmohtref.main.aboutapp.response.AboutAppResponse
import grand.app.salonmohtref.main.aboutapp.response.AboutItem
import grand.app.salonmohtref.main.aboutapp.view.AboutAdapter
import grand.app.salonmohtref.network.ApiResponse
import grand.app.salonmohtref.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.ArrayList

class AboutViewModel : BaseViewModel()
{
    var adapter = AboutAdapter()
    var aboutImg = ObservableField<String>()

    init {
        getAboutData()
    }

    fun getAboutData() {

        obsLayout.set(LoadingStatus.SHIMMER)

        requestCall<AboutAppResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().getAboutApp() }
        })
        { res ->
            when (res!!.code) {
                200 -> {
                    obsLayout.set(LoadingStatus.FULL)
                    adapter.updateList(res.aboutData!!.aboutsList as ArrayList<AboutItem>)
                    aboutImg.set(res.aboutData.img)
                    notifyChange()
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg.toString())
                }
            }
        }
    }
}