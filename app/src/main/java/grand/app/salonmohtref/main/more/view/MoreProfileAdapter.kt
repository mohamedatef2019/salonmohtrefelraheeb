package grand.app.salonmohtref.main.more.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.salonmohtref.R
import grand.app.salonmohtref.databinding.ItemMoreProfileBinding
import grand.app.salonmohtref.main.bottombar.more.view.ItemMoreProfile
import grand.app.salonmohtref.main.more.viewmodel.ItemMoreProfileViewModel
import grand.app.salonmohtref.utils.SingleLiveEvent
import java.util.*

class MoreProfileAdapter : RecyclerView.Adapter<MoreProfileAdapter.MoreProfileHolder>()
{
    var itemsList: ArrayList<ItemMoreProfile> = ArrayList()
    var itemLiveData = SingleLiveEvent<ItemMoreProfile>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoreProfileHolder
    {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemMoreProfileBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_more_profile, parent, false)
        return MoreProfileHolder(binding)
    }

    override fun onBindViewHolder(holder: MoreProfileHolder, position: Int) {
        val itemViewModel = ItemMoreProfileViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.binding.rawLayout.setOnClickListener {
            itemLiveData.value = itemViewModel.item
        }
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<ItemMoreProfile>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class MoreProfileHolder(val binding: ItemMoreProfileBinding) : RecyclerView.ViewHolder(binding.root)
}
