package grand.app.salonmohtref.main.editprofile.request

import java.io.File

data class EditProfileRequest (
        var name: String? = null,
        var email: String? = null,
        var address: String? = null,
        var lat: Double? = null,
        var lng: Double? = null,
        var phone: String? = null,
        var userImg: File? = null
)