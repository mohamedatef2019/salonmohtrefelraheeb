package grand.app.salonmohtref.main.confirmorder.viewmodel

import androidx.databinding.ObservableField
import grand.app.salonmohtref.R
import grand.app.salonmohtref.auth.splash.response.SettingResponse
import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.base.LoadingStatus
import grand.app.salonmohtref.main.confirmorder.request.OrderPaymentItem
import grand.app.salonmohtref.main.confirmorder.request.OrderRequest
import grand.app.salonmohtref.main.confirmorder.response.CreateOrderResponse
import grand.app.salonmohtref.main.confirmorder.view.OrderServicesAdapter
import grand.app.salonmohtref.main.confirmorder.view.OrderPaymentAdapter
import grand.app.salonmohtref.main.mypackage.response.MyPackageResponse
import grand.app.salonmohtref.main.packages.response.PackageServiceItem
import grand.app.salonmohtref.main.salondetails.response.SalonDetailsData
import grand.app.salonmohtref.network.ApiResponse
import grand.app.salonmohtref.utils.PrefMethods
import grand.app.salonmohtref.utils.constants.Codes
import grand.app.salonmohtref.utils.constants.Params
import grand.app.salonmohtref.utils.formatDate
import grand.app.salonmohtref.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.util.ArrayList

class ConfirmOrderViewModel : BaseViewModel() {
    var orderRequest = OrderRequest()
    var paymentAdapter = OrderPaymentAdapter()
    var salonData = SalonDetailsData()
    var adapter = OrderServicesAdapter()
    var obsDate = ObservableField<String>()
    var obsTime = ObservableField<String>()
    var obsTaxesPrice = ObservableField<String>()
    var obsTotalPrice = ObservableField<String>()
    var total: Double = 0.0
    var packageId : Int = -1
    var packageName : String? = null

    fun setData(orderData: OrderRequest) {

        adapter.updateList(orderData.servicesList)

        orderData.date?.let { date ->
            Timber.e("mou3az_date " + date)
            obsDate.set(formatDate(date, "dd-MM-yyyy", "dd MMM yyyy"))
        }

        orderData.time?.let { date ->
            obsTime.set(formatDate(date, "HH:mm", "h:mm a"))
        }

        obsTaxesPrice.set(PrefMethods.getSettingData()!!.tax.toString() + " " + getString(R.string.label_currency))

        total = orderData.total_price!! + PrefMethods.getSettingData()!!.tax!!
        obsTotalPrice.set("$total ${getString(R.string.label_currency)}")
    }

    fun onConfirmClicked() {
        confirmOrder()
    }

    fun onCouponsClicked() {
        setValue(Codes.APPLY_COUPON_CLICKED)
    }

    private fun confirmOrder() {
        orderRequest.salon_id = salonData.id

        orderRequest.servicesList.clear()

//        when {
//            Params.couponIds.size != 0 -> {
//                Params.couponIds.forEach {
//                    orderRequest.free_sub_category_id.add(it.id!!)
//                }
//            }
//        }

        when {
            Params.couponId != -1 -> {
                orderRequest.coupon_id = Params.couponId
            }
        }

        obsIsProgress.set(true)

        Params.couponIds.clear()
        Params.couponId = -1

        requestCall<CreateOrderResponse?>({ withContext(Dispatchers.IO) { return@withContext getApiRepo().createOrder(orderRequest) } })
        { res ->
            obsIsProgress.set(false)
            when (res?.code) {
                200 -> {
                    apiResponseLiveData.value = ApiResponse.success(res)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res?.msg)
                }
            }
        }
    }

    init {
        getUserPackage()
        when {
            PrefMethods.getSettingData() == null -> {
                requestCall<SettingResponse?>({ withContext(Dispatchers.IO) { return@withContext getApiRepo().getSocialLinks() } })
                { res ->
                    when (res!!.code) {
                        200 -> {
                            PrefMethods.saveSettingData(res.settingData!!)
                            notifyChange()
                        }
                    }
                }
            }
        }
    }

    private fun getUserPackage() {

        Timber.e("mou3aaaz_packages" + " get packages")
        obsLayout.set(LoadingStatus.SHIMMER)
        requestCall<MyPackageResponse?>({ withContext(Dispatchers.IO) { return@withContext getApiRepo().getMyPackage() } })
        { res ->
            apiResponseLiveData.value = ApiResponse.shimmerLoading(false)
            when (res!!.code) {
                200 -> {
                    packageId = res.packageDetails?.id!!
                    packageName = res.packageDetails.name!!
                    setPaymentMethods(packageId, packageName!!)
                }
                else -> {
                    setPaymentMethods(-1)
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }

    private fun setPaymentMethods(id : Int, packageName : String?= null)
    {
        var packageDesc = ""
        val packageDesc1 = getString(R.string.label_subscibed_on)
        val packageDesc2 = getString(R.string.label_these_services_availble)

        packageDesc = when (id) {
            -1 -> {
                getString(R.string.label_not_subscribed)
            }
            else -> {
                packageDesc1 + packageName + packageDesc2
            }
        }

        val paymentList = arrayListOf(
                OrderPaymentItem(0, getString(R.string.label_payment_cash), getString(R.string.label_package_dec)),
                OrderPaymentItem(1, getString(R.string.label_payment_online)),
                OrderPaymentItem(2, getString(R.string.label_package), packageDesc)
        )

        paymentAdapter.updateList(paymentList)
    }
}