package grand.app.salonmohtref.main.confirmorder.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.salonmohtref.R
import grand.app.salonmohtref.databinding.ItemOrderServiceBinding
import grand.app.salonmohtref.main.confirmorder.viewmodel.ItemOrderServiceViewModel
import grand.app.salonmohtref.main.salondetails.response.SalonServiceItem
import grand.app.salonmohtref.utils.SingleLiveEvent
import kotlin.collections.ArrayList

class OrderServicesAdapter : RecyclerView.Adapter<OrderServicesAdapter.PaymentHolder>()
{
    var itemsList: ArrayList<SalonServiceItem> = ArrayList()
    var removeLiveData = SingleLiveEvent<SalonServiceItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PaymentHolder {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemOrderServiceBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_order_service, parent, false)
        return PaymentHolder(binding)
    }

    override fun onBindViewHolder(holder: PaymentHolder, position: Int) {
        val itemViewModel = ItemOrderServiceViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.binding.ibRemove.setOnClickListener {
            removeLiveData.value = itemViewModel.item
        }
    }

    fun getItem(pos:Int): SalonServiceItem {
        return itemsList[pos]
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<SalonServiceItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class PaymentHolder(val binding: ItemOrderServiceBinding) : RecyclerView.ViewHolder(binding.root)
}
