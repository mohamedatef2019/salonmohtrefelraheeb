package grand.app.salonmohtref.main.search.view

import java.io.Serializable

data class FilterItem(
    var id: Int? = null,
    var name: String? = null,
    var isSelected : Int = 0
) : Serializable
