package grand.app.salonmohtref.main.home.viewmodel

import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.main.bottombar.home.response.CategoriesItem

class ItemCategoryViewModel(var item: CategoriesItem) : BaseViewModel()