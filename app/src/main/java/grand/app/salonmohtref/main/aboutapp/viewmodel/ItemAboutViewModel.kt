package grand.app.salonmohtref.main.aboutapp.viewmodel

import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.main.aboutapp.response.AboutItem

class ItemAboutViewModel(var item: AboutItem) : BaseViewModel()