package grand.app.salonmohtref.main.activitycoupons.viewmodel

import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.base.LoadingStatus
import grand.app.salonmohtref.main.coupons.response.CouponResponse
import grand.app.salonmohtref.main.coupons.view.CouponAdapter
import grand.app.salonmohtref.network.ApiResponse
import grand.app.salonmohtref.utils.constants.Codes
import grand.app.salonmohtref.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class MyCouponsViewModel : BaseViewModel()
{
    var adapter = CouponAdapter()

    fun getCoupons()
    {
        obsLayout.set(LoadingStatus.SHIMMER)
        requestCall<CouponResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().getMyCoupons() }
        })
        { res ->
            apiResponseLiveData.value = ApiResponse.shimmerLoading(false)
            when (res!!.code) {
                200 -> {
                    when (res.couponsList?.size) {
                        0 -> {
                            obsLayout.set(LoadingStatus.EMPTY)
                        }
                        else ->{
                            obsLayout.set(LoadingStatus.FULL)
                            adapter.updateList(res.couponsList)
                        }
                    }
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }

    fun onBackPressed()
    {
        setValue(Codes.BACK_PRESSED)
    }
}
