package grand.app.salonmohtref.main.wallet.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.salonmohtref.R
import grand.app.salonmohtref.databinding.ItemWalletOrderBinding
import grand.app.salonmohtref.main.wallet.response.WalletHistoryItem
import grand.app.salonmohtref.main.wallet.viewmodel.ItemWalletOrderViewModel
import java.util.*

class WalletOrdersAdapter : RecyclerView.Adapter<WalletOrdersAdapter.WalletOrdersHolder>()
{
    var itemsList: ArrayList<WalletHistoryItem> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WalletOrdersHolder
    {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemWalletOrderBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_wallet_order, parent, false)
        return WalletOrdersHolder(binding)
    }

    override fun onBindViewHolder(holder: WalletOrdersHolder, position: Int) {
        val itemViewModel = ItemWalletOrderViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<WalletHistoryItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class WalletOrdersHolder(val binding: ItemWalletOrderBinding) : RecyclerView.ViewHolder(binding.root)
}
