package grand.app.salonmohtref.main.allsalonss.home.viewmodel

import androidx.databinding.ObservableField
import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.base.LoadingStatus
import grand.app.salonmohtref.main.allsalonss.request.HomeShopsRequest
import grand.app.salonmohtref.main.allsalonss.response.AllSalonsResponse
import grand.app.salonmohtref.main.allsalonss.response.SalonItem
import grand.app.salonmohtref.main.branches.view.SalonsAdapter
import grand.app.salonmohtref.main.favorites.response.AddToFavResponse
import grand.app.salonmohtref.network.ApiResponse
import grand.app.salonmohtref.utils.PrefMethods
import grand.app.salonmohtref.utils.constants.Codes
import grand.app.salonmohtref.utils.constants.Params
import grand.app.salonmohtref.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.ArrayList

class HomeSalonsViewModel : BaseViewModel() {

    var obsSearchNam = ObservableField<String>()
    var adapter = SalonsAdapter()
    var obsAddress = ObservableField<String>()
    var request = HomeShopsRequest()

    fun onSearchClicked() {
        when {
            obsSearchNam.get() == null -> {
                setValue(Codes.EMPTY_SALON_NAME)
            }
            else -> {
                setValue(Codes.SEARCH_CLICKED)
            }
        }
    }
    fun onAddressClicked(){
        setValue(Codes.ADDRESS_CLICKED)
    }

    fun getAllSalons()
    {
        request.type = 1
        request.category_id = Params.categoryid
        when {
            PrefMethods.getUserData() == null -> {
                getVisitorHomeShops()
            }
            else -> {
                getUserHomeShops()
            }
        }
    }

    private fun getVisitorHomeShops() {

        obsLayout.set(LoadingStatus.SHIMMER)
        requestCall<AllSalonsResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().getVisitorHomeShops(request) } })
        { res ->
            when (res!!.code) {
                200 -> {
                    apiResponseLiveData.value = ApiResponse.success(res)
                    when {
                        res.allSalonsData?.salonsList.isNullOrEmpty() || res.allSalonsData?.salonsList!!.isEmpty() -> {
                            obsLayout.set(LoadingStatus.EMPTY)
                        }
                        else -> {
                            obsLayout.set(LoadingStatus.FULL)
                            adapter.updateList(res.allSalonsData.salonsList as ArrayList<SalonItem>)
                        }
                    }
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }

    private fun getUserHomeShops() {

        obsLayout.set(LoadingStatus.SHIMMER)
        requestCall<AllSalonsResponse?>({ withContext(Dispatchers.IO) { return@withContext getApiRepo().getUserHomeShops(request) } })
        { res ->
            when (res!!.code) {
                200 -> {
                    apiResponseLiveData.value = ApiResponse.success(res)
                    when {
                        res.allSalonsData?.salonsList.isNullOrEmpty() || res.allSalonsData?.salonsList!!.isEmpty() -> {
                            obsLayout.set(LoadingStatus.EMPTY)
                        }
                        else -> {
                            obsLayout.set(LoadingStatus.FULL)
                            adapter.updateList(res.allSalonsData.salonsList as ArrayList<SalonItem>)
                        }
                    }
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }


    fun addToFav(salonId : Int) {
        obsIsProgress.set(true)
        requestCall<AddToFavResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().addToFav(salonId, 0) }
        })
        { res ->
            obsIsProgress.set(false)
            when (res!!.code) {
                200 -> {
                    apiResponseLiveData.value = ApiResponse.success(res)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }

    init {
        getAllSalons()
    }
}