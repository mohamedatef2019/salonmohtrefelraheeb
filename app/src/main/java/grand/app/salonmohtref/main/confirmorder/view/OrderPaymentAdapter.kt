package grand.app.salonmohtref.main.confirmorder.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.salonmohtref.R
import grand.app.salonmohtref.base.BaseApp
import grand.app.salonmohtref.databinding.ItemOrderPaymentBinding
import grand.app.salonmohtref.main.confirmorder.request.OrderPaymentItem
import grand.app.salonmohtref.main.confirmorder.viewmodel.ItemOrderPaymentViewModel
import grand.app.salonmohtref.utils.SingleLiveEvent
import kotlin.collections.ArrayList

class OrderPaymentAdapter : RecyclerView.Adapter<OrderPaymentAdapter.PaymentHolder>()
{
    var itemsList: ArrayList<OrderPaymentItem> = ArrayList()
    var itemLiveData = SingleLiveEvent<OrderPaymentItem>()
    var selectedPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PaymentHolder {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemOrderPaymentBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_order_payment, parent, false)
        return PaymentHolder(binding)
    }

    override fun onBindViewHolder(holder: PaymentHolder, position: Int) {
        val itemViewModel = ItemOrderPaymentViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.setSelected()
        holder.binding.rawLayout.setOnClickListener {
            when {
                position != selectedPosition -> {
                    notifyItemChanged(position)
                    notifyItemChanged(selectedPosition)
                    selectedPosition = position
                    itemLiveData.value = itemViewModel.item
                }
                else -> {
                    selectedPosition = -1
                    notifyItemChanged(position)
                    itemLiveData.value = itemViewModel.item
                }
            }
        }
    }

    fun getItem(pos:Int): OrderPaymentItem {
        return itemsList[pos]
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<OrderPaymentItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class PaymentHolder(val binding: ItemOrderPaymentBinding) : RecyclerView.ViewHolder(binding.root) {
        fun setSelected()
        {
            when (selectedPosition) {
                adapterPosition -> {
                    binding.rawLayout.setBackgroundResource(R.drawable.selected_home_bg)
                    binding.tvName.setTextColor(ContextCompat.getColor(BaseApp.getInstance, R.color.white))
                    binding.icChecked.setImageResource(R.drawable.ic_payment_checked)
                    binding.tvDesc.visibility = ViewGroup.VISIBLE
                }
                else -> {
                    binding.rawLayout.background = null
                    binding.tvName.setTextColor(ContextCompat.getColor(BaseApp.getInstance, R.color.color_primary))
                    binding.icChecked.setImageResource(R.drawable.ic_payment_unchecked)
                    binding.tvDesc.visibility = ViewGroup.GONE
                }
            }
        }
    }
}
