package grand.app.salonmohtref.main.packages.response

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class PackagesResponse(

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val packagesList: List<PackageItem?>? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class PackageItem(

	@field:SerializedName("cover")
	val cover: String? = null,

	@field:SerializedName("badge")
	val badge: String? = null,

	@field:SerializedName("visits")
	val visits: Int? = null,

	@field:SerializedName("period")
	val period: Int? = null,

	@field:SerializedName("price")
	val price: Int? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("services")
	val servicesList: List<PackageServiceItem?>? = null,

	@field:SerializedName("free_months")
	val freeMonths: Int? = null
) : Parcelable {
	constructor(parcel: Parcel) : this(
			parcel.readString(),
			parcel.readString(),
			parcel.readValue(Int::class.java.classLoader) as? Int,
			parcel.readValue(Int::class.java.classLoader) as? Int,
			parcel.readValue(Int::class.java.classLoader) as? Int,
			parcel.readString(),
			parcel.readValue(Int::class.java.classLoader) as? Int,
			TODO("servicesList"),
			parcel.readValue(Int::class.java.classLoader) as? Int) {
	}

	override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeString(cover)
		parcel.writeString(badge)
		parcel.writeValue(visits)
		parcel.writeValue(period)
		parcel.writeValue(price)
		parcel.writeString(name)
		parcel.writeValue(id)
		parcel.writeValue(freeMonths)
	}

	override fun describeContents(): Int {
		return 0
	}

	companion object CREATOR : Parcelable.Creator<PackageItem> {
		override fun createFromParcel(parcel: Parcel): PackageItem {
			return PackageItem(parcel)
		}

		override fun newArray(size: Int): Array<PackageItem?> {
			return arrayOfNulls(size)
		}
	}
}

data class PackageServiceItem(

	@field:SerializedName("sub_category_id")
	val subCategoryId: Int? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("package_id")
	val packageId: Int? = null,

	@field:SerializedName("user_count")
	val userCount: Int? = null
)
