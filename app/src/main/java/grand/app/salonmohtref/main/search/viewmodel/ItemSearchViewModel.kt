package grand.app.salonmohtref.main.search.viewmodel

import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.main.search.response.SearchItem

class ItemSearchViewModel(var item: SearchItem) : BaseViewModel()