package grand.app.salonmohtref.main.cancelapolicy.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.salonmohtref.R
import grand.app.salonmohtref.databinding.ItemPolicyBinding
import grand.app.salonmohtref.main.cancelapolicy.response.CancellationPolicyItem
import grand.app.salonmohtref.main.cancelapolicy.viewmodel.ItemPolicyViewModel
import java.util.*

class PolicyAdapter : RecyclerView.Adapter<PolicyAdapter.PolicyHolder>()
{
    var itemsList: ArrayList<CancellationPolicyItem> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PolicyHolder
    {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemPolicyBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_policy, parent, false)
        return PolicyHolder(binding)
    }

    override fun onBindViewHolder(holder: PolicyHolder, position: Int) {
        val itemViewModel = ItemPolicyViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<CancellationPolicyItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class PolicyHolder(val binding: ItemPolicyBinding) : RecyclerView.ViewHolder(binding.root)
}
