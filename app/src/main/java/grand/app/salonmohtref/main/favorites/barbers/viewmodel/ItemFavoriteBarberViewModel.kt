package grand.app.salonmohtref.main.favorites.barbers.viewmodel

import androidx.databinding.ObservableField
import grand.app.salonmohtref.R
import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.main.favorites.response.FavoriteBarberItem

class ItemFavoriteBarberViewModel(var item: FavoriteBarberItem) : BaseViewModel()
{
    var obsRate : Float = 0f
    var obsWorkingHours = ObservableField<String>()

    init {
        obsRate = item.rate!!.toFloat()
        obsWorkingHours.set(getSalonTimes(item.startTime, item.endTime))
    }

    private fun getSalonTimes(startTime : String?, endTime : String?) : String {
        return getString(R.string.label_working_houser_start_from) + " " + startTime + " " + getString(R.string.label_to) + " " + endTime
    }
}