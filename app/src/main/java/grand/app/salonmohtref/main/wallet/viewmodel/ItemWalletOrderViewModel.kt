package grand.app.salonmohtref.main.wallet.viewmodel

import androidx.databinding.ObservableField
import grand.app.salonmohtref.R
import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.main.wallet.response.WalletHistoryItem

class ItemWalletOrderViewModel(var item: WalletHistoryItem) : BaseViewModel()
{
    var obsDesc= ObservableField<String>()

    init {
        when (item.type) {
            0 -> {
                obsDesc.set(" ${getString(R.string.label_added)} ${item.credit} " +
                        "${getString(R.string.label_point)} ${getString(R.string.to_your_credit)} ${item.createdAt}")
            }
            1 -> {
                obsDesc.set(" ${getString(R.string.label_subtracted)} ${item.credit} " +
                        "${getString(R.string.label_point)} ${getString(R.string.label_coupon)} ${item.credit} ${getString(R.string.label_point)}")
            }
        }
    }
}