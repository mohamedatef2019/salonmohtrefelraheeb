package grand.app.salonmohtref.main.cancelapolicy.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import grand.app.salonmohtref.main.cancelapolicy.viewmodel.PolicyViewModel
import grand.app.salonmohtref.R
import grand.app.salonmohtref.activity.home.BaseHomeFragment
import grand.app.salonmohtref.databinding.FragmentCancelPolicyBinding
import grand.app.salonmohtref.network.Status
import grand.app.salonmohtref.utils.observe
import timber.log.Timber

class PolicyFragment : BaseHomeFragment()
{
    lateinit var binding: FragmentCancelPolicyBinding
    lateinit var viewModel: PolicyViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_cancel_policy, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(PolicyViewModel::class.java)
        binding.viewModel = viewModel

        showBottomBar(false)


        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }
    }
}