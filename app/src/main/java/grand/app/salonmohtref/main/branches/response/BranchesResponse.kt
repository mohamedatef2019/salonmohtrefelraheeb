package grand.app.salonmohtref.main.branches.response

import com.google.gson.annotations.SerializedName
import grand.app.salonmohtref.main.allsalonss.response.SalonItem

data class BranchesResponse(

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val branchesData: BranchesData? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class BranchesData(

		@field:SerializedName("first_page_url")
		val firstPageUrl: String? = null,

		@field:SerializedName("path")
		val path: String? = null,

		@field:SerializedName("per_page")
		val perPage: Int? = null,

		@field:SerializedName("total")
		val total: Int? = null,

		@field:SerializedName("data")
		val branchesList: List<SalonItem?>? = null,

		@field:SerializedName("last_page")
		val lastPage: Int? = null,

		@field:SerializedName("last_page_url")
		val lastPageUrl: String? = null,

		@field:SerializedName("next_page_url")
		val nextPageUrl: Any? = null,

		@field:SerializedName("from")
		val from: Int? = null,

		@field:SerializedName("to")
		val to: Int? = null,

		@field:SerializedName("prev_page_url")
		val prevPageUrl: Any? = null,

		@field:SerializedName("current_page")
		val currentPage: Int? = null
)