package grand.app.salonmohtref.main.aboutapp.response

import com.google.gson.annotations.SerializedName
import grand.app.salonmohtref.main.favorites.response.BarberWorkingItem
import grand.app.salonmohtref.main.salonbarbers.response.SalonBarberItem

data class ExperianceResponse(

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val data: Data? = null,

	@field:SerializedName("status")
	val status: String? = null
)



data class Data(

	@field:SerializedName("data")
	val data: Data? = null,

	@field:SerializedName("barbers")
	val barbers: List<SalonBarberItem?>? = null,

	@field:SerializedName("workings")
	val workings: List<BarberWorkingItem?>? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("desc")
	val desc: String? = null
)
