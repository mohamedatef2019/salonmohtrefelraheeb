package grand.app.salonmohtref.main.packages.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.salonmohtref.R
import grand.app.salonmohtref.databinding.ItemPackageBinding
import grand.app.salonmohtref.main.packages.response.PackageItem
import grand.app.salonmohtref.main.packages.viewmodel.ItemPackageViewModel
import grand.app.salonmohtref.utils.SingleLiveEvent
import java.util.*

class PackagesAdapter : RecyclerView.Adapter<PackagesAdapter.PackageHolder>()
{
    var itemsList: ArrayList<PackageItem> = ArrayList()
    var itemLiveData = SingleLiveEvent<PackageItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PackageHolder
    {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemPackageBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_package, parent, false)
        return PackageHolder(binding)
    }

    override fun onBindViewHolder(holder: PackageHolder, position: Int) {
        val itemViewModel = ItemPackageViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.binding.rawLayout.setOnClickListener {
            itemLiveData.value = itemViewModel.item
        }
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<PackageItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class PackageHolder(val binding: ItemPackageBinding) : RecyclerView.ViewHolder(binding.root)
}
