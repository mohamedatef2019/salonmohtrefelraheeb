package grand.app.salonmohtref.main.ordertime.time

import androidx.databinding.ObservableField
import grand.app.salonmohtref.base.BaseViewModel
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class ItemTimeViewModel(var item: ItemSalonTime) : BaseViewModel()
{
    var obstime = ObservableField<String>()
    var date = Date()

    init {
        when {
            item.time != null -> {
                val format = SimpleDateFormat("HH:mm")
                try {
                    date = format.parse(item.time)
                } catch (e: ParseException) {
                    e.printStackTrace()
                }

                val timeFormat = SimpleDateFormat("HH:mm", Locale("en"))
                obstime.set(timeFormat.format(date).toString())
            }
        }
    }
}
