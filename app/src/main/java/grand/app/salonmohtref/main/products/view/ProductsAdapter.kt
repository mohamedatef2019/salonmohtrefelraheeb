package grand.app.salonmohtref.main.products.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.salonmohtref.R
import grand.app.salonmohtref.databinding.ItemProductBinding
import grand.app.salonmohtref.main.bottombar.products.response.ProductItem
import grand.app.salonmohtref.main.products.viewmodel.ItemProductViewModel
import grand.app.salonmohtref.utils.SingleLiveEvent
import java.util.*

class ProductsAdapter : RecyclerView.Adapter<ProductsAdapter.ProductsHolder>()
{
    var itemsList: ArrayList<ProductItem> = ArrayList()
    var itemLiveData = SingleLiveEvent<ProductItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductsHolder {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemProductBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_product, parent, false)
        return ProductsHolder(binding)
    }

    override fun onBindViewHolder(holder: ProductsHolder, position: Int) {
        val itemViewModel = ItemProductViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.binding.rawLayout.setOnClickListener {
            itemLiveData.value = itemViewModel.item
        }
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<ProductItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class ProductsHolder(val binding: ItemProductBinding) : RecyclerView.ViewHolder(binding.root)
}
