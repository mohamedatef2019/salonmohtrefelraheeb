package grand.app.salonmohtref.main.allsalonss.request

import grand.app.salonmohtref.utils.PrefMethods

data class HomeShopsRequest (
        var lat: Double? = PrefMethods.getUserLocation()?.lat,
        var lng: Double? = PrefMethods.getUserLocation()?.lng,
        var type : Int =  1,
        var category_id: Int ? = null
)