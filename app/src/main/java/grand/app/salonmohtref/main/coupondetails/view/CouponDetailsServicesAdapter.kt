package grand.app.salonmohtref.main.coupondetails.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.salonmohtref.R
import grand.app.salonmohtref.databinding.ItemCouponDetailsServiceBinding
import grand.app.salonmohtref.main.coupondetails.viewmodel.ItemCouponDetailsServiceViewModel
import grand.app.salonmohtref.main.coupons.response.CouponServiceItem
import java.util.*

class CouponDetailsServicesAdapter : RecyclerView.Adapter<CouponDetailsServicesAdapter.CouponHolder>()
{
    var itemsList: ArrayList<CouponServiceItem> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CouponHolder
    {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemCouponDetailsServiceBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_coupon_details_service, parent, false)
        return CouponHolder(binding)
    }

    override fun onBindViewHolder(holder: CouponHolder, position: Int) {
        val itemViewModel = ItemCouponDetailsServiceViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: List<CouponServiceItem?>?) {
        itemsList = models as ArrayList<CouponServiceItem>
        notifyDataSetChanged()
    }

    inner class CouponHolder(val binding: ItemCouponDetailsServiceBinding) : RecyclerView.ViewHolder(binding.root)
}
