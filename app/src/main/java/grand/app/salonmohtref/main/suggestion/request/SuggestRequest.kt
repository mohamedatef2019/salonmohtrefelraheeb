package grand.app.salonmohtref.main.suggestion.request

class SuggestRequest {
    var name: String? = null
    var email: String? = null
    var phone: String? = null
    var message: String? = null
}