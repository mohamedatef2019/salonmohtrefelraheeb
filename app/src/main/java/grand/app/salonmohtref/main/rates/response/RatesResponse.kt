package grand.app.salonmohtref.main.rates.response

import com.google.gson.annotations.SerializedName

data class RatesResponse(

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val ratesList: List<RateItem?>? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class RateItem(

	@field:SerializedName("rate")
	val rate: Int? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("img")
	val img: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("comment")
	val comment: String? = null,

	@field:SerializedName("id")
	val id: Int? = null
)
