package grand.app.salonmohtref.main.productdetails.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import grand.app.salonmohtref.activity.home.BaseHomeFragment
import grand.app.salonmohtref.R
import grand.app.salonmohtref.databinding.FragmentProductDetailsBinding
import grand.app.salonmohtref.main.productdetails.response.ProductDetailsResponse
import grand.app.salonmohtref.main.productdetails.viewmodel.ProductDetailsViewModel
import grand.app.salonmohtref.network.Status
import grand.app.salonmohtref.utils.observe
import timber.log.Timber

class ProductDetailsFragment : BaseHomeFragment()
{
    lateinit var binding: FragmentProductDetailsBinding
    lateinit var viewModel: ProductDetailsViewModel
    private val fragmentArgs : ProductDetailsFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_product_details, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(ProductDetailsViewModel::class.java)
        binding.viewModel = viewModel

        showBottomBar(false)

        viewModel.getProductDetails(fragmentArgs.productId)

        observe(viewModel.adapter.itemLiveData){
            it?.id.let { id ->
                viewModel.getProductDetails(id!!)
            }
        }

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is ProductDetailsResponse -> {
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }
    }
}