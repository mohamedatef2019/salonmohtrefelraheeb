package grand.app.salonmohtref.main.activitycoupons.view

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import grand.app.salonmohtref.R
import grand.app.salonmohtref.base.BaseActivity
import grand.app.salonmohtref.base.BaseApp
import grand.app.salonmohtref.databinding.ActivityCouponsBinding
import grand.app.salonmohtref.dialogs.toast.DialogToastFragment
import grand.app.salonmohtref.main.activityapplycoupon.view.ApplyCouponActivity
import grand.app.salonmohtref.main.activitycoupons.viewmodel.MyCouponsViewModel
import grand.app.salonmohtref.main.coupons.response.UserServicesItem
import grand.app.salonmohtref.main.credit.response.CreditResponse
import grand.app.salonmohtref.network.Status
import grand.app.salonmohtref.utils.Utils
import grand.app.salonmohtref.utils.constants.Codes
import grand.app.salonmohtref.utils.constants.Params
import grand.app.salonmohtref.utils.observe
import timber.log.Timber

class CouponsActivity : BaseActivity()
{
    lateinit var binding: ActivityCouponsBinding
    lateinit var viewModel: MyCouponsViewModel
    private var mReturningWithResult = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_coupons)

        viewModel = ViewModelProvider(this).get(MyCouponsViewModel::class.java)
        binding.viewModel = viewModel

        viewModel.getCoupons()

        when {
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP -> {
                val window: Window = window
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                window.statusBarColor = ContextCompat.getColor(this, R.color.color_primary)
            }
        }

        observe(viewModel.mutableLiveData){
            when (it) {
                Codes.BACK_PRESSED -> {
                    val intent = Intent()
                    intent.putExtra(Params.DIALOG_CLICK_ACTION, 0)
                    setResult(Codes.ALL_COUPONS_REQUEST, intent)
                    finish()
                }
            }
        }

        observe(viewModel.adapter.itemLiveData){
            val intent = Intent(BaseApp.getInstance, ApplyCouponActivity::class.java)
            intent.putExtra(Params.COUPON_ITEM, it)
            startActivityForResult(intent, Codes.APPLY_COUPON_REQUEST)
        }

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is CreditResponse -> {
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }
    }

    fun showToast(msg: String, type: Int) {
        val bundle = Bundle()
        bundle.putString(Params.DIALOG_TOAST_MESSAGE, msg)
        bundle.putInt(Params.DIALOG_TOAST_TYPE, type)
        Utils.startDialogActivity(
            this,
            DialogToastFragment::class.java.name,
            Codes.DIALOG_TOAST_REQUEST,
            bundle
        )
    }

    @SuppressLint("MissingSuperCall")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        Timber.e("mou3az_coupons : " + requestCode)
        Timber.e("mou3az_coupons : " + resultCode)
        when(requestCode) {
            Codes.APPLY_COUPON_REQUEST -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        Timber.e("mou3az_coupons ")
                                        val intent = Intent()
                                        intent.putExtra(Params.DIALOG_CLICK_ACTION, 1)
                                        setResult(Codes.ALL_COUPONS_REQUEST, intent)
                                        finish()
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        mReturningWithResult = true;
    }

    override fun onPostResume() {
        super.onPostResume()
        if (mReturningWithResult)
        {
            mReturningWithResult = false
        }
    }

    override fun onBackPressed()
    {
        val intent = Intent()
        intent.putExtra(Params.DIALOG_CLICK_ACTION, 0)
        setResult(Codes.ALL_COUPONS_REQUEST, intent)
        finish()
    }
}