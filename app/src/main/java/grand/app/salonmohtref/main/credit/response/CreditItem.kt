package grand.app.salonmohtref.main.credit.response

data class CreditItem(
    var id : Int ?= null,
    var type : Int ?= null, // 1 خصم // 0 شحن
    var createdAt : String ?= null,

    // This parameter for Coupon Only
    var couponPoints : Int ?= null,

    // This parameter for Order Only
    var updated_at : String ?= null
)