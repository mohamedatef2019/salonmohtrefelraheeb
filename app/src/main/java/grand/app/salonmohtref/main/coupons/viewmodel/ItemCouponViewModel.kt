package grand.app.salonmohtref.main.coupons.viewmodel

import androidx.databinding.ObservableField
import grand.app.salonmohtref.R
import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.main.coupons.response.CouponItem

class ItemCouponViewModel(var item: CouponItem) : BaseViewModel()
{
    var obsTitle = ObservableField<String>()

    init {
//        obsTitle.set("""كوبون ال ( ${item.points}) ${getString(R.string.label_point)}""")
        obsTitle.set("""${getString(R.string.label_coupon)} ${getString(R.string.label_right_bracket)}${item.points}${getString(R.string.label_left_bracket)}""")
    }
}