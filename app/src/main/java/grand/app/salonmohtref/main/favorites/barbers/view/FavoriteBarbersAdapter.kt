package grand.app.salonmohtref.main.favorites.barbers.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import grand.app.salonmohtref.R
import grand.app.salonmohtref.base.BaseApp
import grand.app.salonmohtref.databinding.ItemFavoriteBarberBinding
import grand.app.salonmohtref.main.favorites.barbers.viewmodel.ItemFavoriteBarberViewModel
import grand.app.salonmohtref.main.favorites.response.FavoriteBarberItem
import grand.app.salonmohtref.main.favorites.response.FavoriteSalonItem
import grand.app.salonmohtref.utils.SingleLiveEvent
import java.util.*

class FavoriteBarbersAdapter : RecyclerView.Adapter<FavoriteBarbersAdapter.BarberHolder>()
{
    var itemsList: ArrayList<FavoriteBarberItem> = ArrayList()
    var itemLiveData  = SingleLiveEvent<FavoriteBarberItem>()
    var favLiveData  = SingleLiveEvent<FavoriteBarberItem>()
    var selectedPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BarberHolder
    {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemFavoriteBarberBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_favorite_barber, parent, false)
        return BarberHolder(binding)
    }

    override fun onBindViewHolder(holder: BarberHolder, position: Int) {
        val itemViewModel = ItemFavoriteBarberViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.setSelected()
        holder.setFav()

        holder.binding.ibFav.setOnClickListener {
            favLiveData.value = itemViewModel.item
        }

        holder.binding.rawLayout.setOnClickListener {
            when {
                position != selectedPosition -> {
                    notifyItemChanged(position)
                    notifyItemChanged(selectedPosition)
                    selectedPosition = position
                }
                selectedPosition == position -> {
                    selectedPosition = -1
                    notifyItemChanged(position)
                }
            }
            itemLiveData.value = itemViewModel.item
        }
    }

    fun removeItem(item: FavoriteBarberItem) {
        itemsList.remove(item)
        notifyDataSetChanged()
    }

    fun getItem(pos:Int): FavoriteBarberItem {
        return itemsList[pos]
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<FavoriteBarberItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class BarberHolder(val binding: ItemFavoriteBarberBinding) : RecyclerView.ViewHolder(binding.root)
    {
        fun setSelected()
        {
            when (selectedPosition) {
                adapterPosition -> {
                    binding.rawLayout.background = ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.barber_selected_bg)
                    binding.tvBarberName.setTextColor(ContextCompat.getColor(BaseApp.getInstance, R.color.white))
                    binding.tvBarberAddress.setTextColor(ContextCompat.getColor(BaseApp.getInstance, R.color.white))
                    binding.tvBarberWorkingHours.setTextColor(ContextCompat.getColor(BaseApp.getInstance, R.color.white))
                    binding.tvStatus.background = ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.barber_not_selected_bg)
                    binding.tvStatus.setTextColor(ContextCompat.getColor(BaseApp.getInstance, R.color.color_primary))
                }
                else -> {
                    binding.rawLayout.background = ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.barber_not_selected_bg)
                    binding.tvBarberName.setTextColor(ContextCompat.getColor(BaseApp.getInstance, R.color.color_primary))
                    binding.tvBarberAddress.setTextColor(ContextCompat.getColor(BaseApp.getInstance, R.color.color_primary))
                    binding.tvBarberWorkingHours.setTextColor(ContextCompat.getColor(BaseApp.getInstance, R.color.color_primary))

//                    when (getItem(adapterPosition).status) {
//                        0 -> {
//                            binding.tvStatus.background = ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.btn_not_available)
//                            binding.tvStatus.text =  BaseApp.getInstance.getString(R.string.label_not_available)
//                        }
//                        1 -> {
//                            binding.tvStatus.background = ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.barber_selected_bg)
//                        }
//                    }
                    binding.tvStatus.setTextColor(ContextCompat.getColor(BaseApp.getInstance, R.color.white))
                }
            }
        }

        fun setFav(){
            Glide.with(BaseApp.getInstance).load(R.drawable.ic_fav).into(binding.ibFav)
        }
    }
}
