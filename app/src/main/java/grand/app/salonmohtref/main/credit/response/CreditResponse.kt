package grand.app.salonmohtref.main.credit.response

import com.google.gson.annotations.SerializedName

data class CreditResponse(

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val myCreditData: MyCreditData? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class MyCreditData(

	@field:SerializedName("reservation_points")
	val reservationPoints: Int? = null,

	@field:SerializedName("coupons")
	val couponsList: List<CreditCouponItem?>? = null,

	@field:SerializedName("orders")
	val ordersList: List<CreditOrderItem?>? = null,

	@field:SerializedName("points")
	val points: Int? = null
)

data class CreditCouponItem(

	@field:SerializedName("coupon_id")
	val couponId: Int? = null,

	@field:SerializedName("user_id")
	val userId: Int? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("coupon_points")
	val couponPoints: Int? = null
)

data class CreditOrderItem(

	@field:SerializedName("date")
	val date: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("img")
	val img: String? = null,

	@field:SerializedName("user_name")
	val userName: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("logo")
	val logo: String? = null,

	@field:SerializedName("time")
	val time: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("type")
	val type: Int? = null,

	@field:SerializedName("user_img")
	val userImg: String? = null,

	@field:SerializedName("desc")
	val desc: String? = null
)
