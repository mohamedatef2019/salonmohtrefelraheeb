package grand.app.salonmohtref.main.activityapplycoupon.view

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import es.dmoral.toasty.Toasty
import grand.app.salonmohtref.R
import grand.app.salonmohtref.base.BaseActivity
import grand.app.salonmohtref.databinding.ActivityApplyCouponBinding
import grand.app.salonmohtref.main.activityapplycoupon.viewmodel.ApplyCouponViewModel
import grand.app.salonmohtref.main.coupons.response.CouponItem
import grand.app.salonmohtref.main.coupons.response.UserServicesItem
import grand.app.salonmohtref.utils.constants.Codes
import grand.app.salonmohtref.utils.constants.Params
import grand.app.salonmohtref.utils.observe

class ApplyCouponActivity : BaseActivity()
{
    lateinit var binding: ActivityApplyCouponBinding
    lateinit var viewModel: ApplyCouponViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_apply_coupon)
        viewModel = ViewModelProvider(this).get(ApplyCouponViewModel::class.java)
        binding.viewModel = viewModel

        when {
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP -> {
                val window: Window = window
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                window.statusBarColor = ContextCompat.getColor(this, R.color.color_primary)
            }
        }

        when {
            intent.hasExtra(Params.COUPON_ITEM) -> {
                when {
                    intent.getSerializableExtra(Params.COUPON_ITEM) != null -> {
                        val couponItem: CouponItem = (intent.extras!!.getSerializable(Params.COUPON_ITEM) as CouponItem?)!!
                        viewModel.fillCouponData(couponItem)
                    }
                }
            }
        }

        observe(viewModel.mutableLiveData){
            when (it) {
                Codes.APPLY_COUPON_CLICKED -> {
                    Params.couponIds.addAll(viewModel.selectedServicesAdapter.itemsList)
                    Params.couponId = viewModel.couponData.id!!
                    val intent = Intent()
                    intent.putExtra(Params.DIALOG_CLICK_ACTION, 1)
                    setResult(Codes.APPLY_COUPON_REQUEST, intent)
                    finish()
                }
                Codes.BACK_PRESSED -> {
                    val intent = Intent()
                    intent.putExtra(Params.DIALOG_CLICK_ACTION, 0)
                    setResult(Codes.APPLY_COUPON_REQUEST, intent)
                    finish()
                }
                Codes.EMPTY_SERVICES ->{
                    Toasty.warning(this, getString(R.string.msg_empty_coupon_services)).show()
//                    val bundle = Bundle()
//                    bundle.putString(Params.DIALOG_TOAST_MESSAGE, getString(R.string.msg_empty_coupon_services))
//                    bundle.putInt(Params.DIALOG_TOAST_TYPE, 1)
//                    Utils.startDialogActivity(this, DialogToastFragment::class.java.name, Codes.DIALOG_TOAST_REQUEST, bundle)
                }
            }
        }

        observe(viewModel.usedServicesAdapter.itemLiveData) {

            viewModel.usedServicesAdapter.notifyItemRemoved(it!!)

            var item = UserServicesItem()
            item.id = it.id
            item.count = 1
            item.couponId = it.couponId
            item.subCategoryId = it.subCategoryId
            item.name = it.name
            item.userId = it.userId

            viewModel.selectedServicesAdapter.notifyItemAdded(item)
        }

        observe(viewModel.selectedServicesAdapter.itemLiveData){

            viewModel.selectedServicesAdapter.notifyItemRemoved(it!!)

            var item = UserServicesItem()
            item.id = it.id
            item.count = 1
            item.couponId = it.couponId
            item.subCategoryId = it.subCategoryId
            item.name = it.name
            item.userId = it.userId

            viewModel.usedServicesAdapter.notifyItemAdded(item)
        }
    }

    override fun onBackPressed()
    {
        val intent = Intent()
        intent.putExtra(Params.DIALOG_CLICK_ACTION, 0)
        setResult(Codes.APPLY_COUPON_REQUEST, intent)
        finish()
    }
}