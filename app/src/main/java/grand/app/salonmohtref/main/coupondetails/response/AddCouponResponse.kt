package grand.app.salonmohtref.main.coupondetails.response

import com.google.gson.annotations.SerializedName

data class AddCouponResponse(

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("status")
	val status: String? = null
)
