package grand.app.salonmohtref.main.bottombar.home.request

data class UpdateTokenRequest(
        var firebase_token : String ?= null
)
