package grand.app.salonmohtref.main.aboutapp.viewmodel


import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.base.LoadingStatus
import grand.app.salonmohtref.main.aboutapp.response.ExperianceResponse
import grand.app.salonmohtref.main.aboutapp.view.BarberOldWorkingAdapter
import grand.app.salonmohtref.main.aboutapp.view.BranchBarberAdapter
import grand.app.salonmohtref.network.ApiResponse
import grand.app.salonmohtref.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.ArrayList

class ExperienceViewModel : BaseViewModel()
{
    var barberAdapter = BranchBarberAdapter()
    var workingAdapter = BarberOldWorkingAdapter()
    var  experianceResponse = ExperianceResponse()
    init {
        getExperienceData()
    }

    fun getExperienceData() {

        obsLayout.set(LoadingStatus.SHIMMER)

        requestCall<ExperianceResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().getExperience() }
        })
        { res ->
            when (res!!.code) {
                200 -> {
                    obsLayout.set(LoadingStatus.FULL)
                    barberAdapter.updateList(res.data?.barbers)
                    workingAdapter.updateList(ArrayList(res.data?.workings))
                    experianceResponse=res
                    notifyChange()
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg.toString())
                }
            }
        }
    }
}