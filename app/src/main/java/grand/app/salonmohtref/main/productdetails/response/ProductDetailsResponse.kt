package grand.app.salonmohtref.main.productdetails.response

import com.google.gson.annotations.SerializedName
import grand.app.salonmohtref.main.bottombar.products.response.ProductItem

data class ProductDetailsResponse(

		@field:SerializedName("msg")
	val msg: String? = null,

		@field:SerializedName("code")
	val code: Int? = null,

		@field:SerializedName("data")
	val productDetailsData: ProductDetailsData? = null,

		@field:SerializedName("status")
	val status: String? = null
)

data class ProductDetailsData(

		@field:SerializedName("img")
	val img: String? = null,

		@field:SerializedName("related")
	val relatedProductsList: List<ProductItem?>? = null,

		@field:SerializedName("price")
	val price: Int? = null,

		@field:SerializedName("name")
	val name: String? = null,

		@field:SerializedName("id")
	val id: Int? = null,

		@field:SerializedName("desc")
	val desc: String? = null
)
