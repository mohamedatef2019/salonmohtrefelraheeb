package grand.app.salonmohtref.main.bookings.adapter

import  androidx.databinding.ObservableField
import grand.app.salonmohtref.R
import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.main.bookings.response.BookingServiceItem
import grand.app.salonmohtref.main.bookings.response.MyBookingItem
import grand.app.salonmohtref.utils.formatDate
import java.util.ArrayList

class ItemBookingViewModel(var item: MyBookingItem) : BaseViewModel()
{
    var adapter = BookingServiceAdapter()
    var obsDate = ObservableField<String>()
    var obsBtnText = ObservableField<String>()

    init {
        when {
            item.servicesList!!.isNotEmpty() -> {
                adapter.updateList(item.servicesList as ArrayList<BookingServiceItem>)
            }
        }

        when (item.status) {
            0, 1 -> {
                obsBtnText.set(getString(R.string.action_cancel_order))
            }
            else -> {
                obsBtnText.set(getString(R.string.action_rate))
            }
        }
        item.date?.let { date ->
            obsDate.set(formatDate(date, "dd-MM-yyyy", "dd MMM yyyy", "en"))
        }
    }
}