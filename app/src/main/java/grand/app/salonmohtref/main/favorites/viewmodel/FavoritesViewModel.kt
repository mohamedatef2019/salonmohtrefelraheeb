package grand.app.salonmohtref.main.favorites.viewmodel

import androidx.databinding.ObservableInt
import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.utils.constants.Codes

class FavoritesViewModel : BaseViewModel() {

    val obsPosition = ObservableInt(0)

    fun onTabClicked(position : Int){
        obsPosition.set(position)
        setValue(position)
    }

    fun onPageSelected(position : Int)
    {
        obsPosition.set(position)
    }
}