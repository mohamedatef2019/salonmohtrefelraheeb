package grand.app.salonmohtref.main.notifications.viewmodel

import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.base.LoadingStatus
import grand.app.salonmohtref.main.notifications.response.NotificationItem
import grand.app.salonmohtref.main.notifications.response.NotificationsResponse
import grand.app.salonmohtref.main.notifications.view.NotificationsAdapter
import grand.app.salonmohtref.network.ApiResponse
import grand.app.salonmohtref.utils.PrefMethods
import grand.app.salonmohtref.utils.constants.Codes
import grand.app.salonmohtref.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber

class NotificationsViewModel : BaseViewModel()
{
    var adapter = NotificationsAdapter()

    fun getNotifications()
    {
        obsLayout.set(LoadingStatus.SHIMMER)
        requestCall<NotificationsResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().getNotifications() }
        })
        { res ->
            when (res!!.code) {
                200 -> {
                    when (res.notificationsList!!.size) {
                        0 -> {
                            obsLayout.set(LoadingStatus.EMPTY)
                        }
                        else -> {
                            Timber.e("" + res.notificationsList[0]?.img)
                            adapter.updateList(res.notificationsList as ArrayList<NotificationItem>)
                            notifyChange()
                            obsLayout.set(LoadingStatus.FULL)
                        }
                    }
                }
                401 -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }



    fun onLoginClicked() {
        setValue(Codes.LOGIN_CLICKED)
    }

    fun getUserStatus() {
        when {
            PrefMethods.getUserData() != null -> {
                getNotifications()
            }
            else -> {
                obsLayout.set(LoadingStatus.NOTLOGIN)
            }
        }
    }

    init {
        getUserStatus()
    }
}