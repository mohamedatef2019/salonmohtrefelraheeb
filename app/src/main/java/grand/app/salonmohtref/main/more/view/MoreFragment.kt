package grand.app.salonmohtref.main.more.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import grand.app.salonmohtref.main.more.viewmodel.MoreViewModel
import grand.app.salonmohtref.R
import grand.app.salonmohtref.activity.auth.AuthActivity
import grand.app.salonmohtref.activity.home.BaseHomeFragment
import grand.app.salonmohtref.databinding.FragmentMoreBinding
import grand.app.salonmohtref.dialogs.language.DialogLanguageFragment
import grand.app.salonmohtref.utils.LocalUtil
import grand.app.salonmohtref.utils.PrefMethods
import grand.app.salonmohtref.utils.Utils
import grand.app.salonmohtref.utils.constants.Codes
import grand.app.salonmohtref.utils.constants.Params
import grand.app.salonmohtref.utils.observe
import java.util.*

class MoreFragment : BaseHomeFragment()
{
    lateinit var binding: FragmentMoreBinding
    lateinit var viewModel: MoreViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_more, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(MoreViewModel::class.java)
        binding.viewModel = viewModel

        showBottomBar(true)


        observe(viewModel.adapter.itemLiveData){
            it.let {
                when (it?.id) {
                    0 -> {
                        findNavController().navigate(R.id.more_to_notifications)
                    }
                    1 -> {
                        findNavController().navigate(R.id.more_to_terms)
                    }
                    2 -> {
                        findNavController().navigate(R.id.more_to_about)
                    }
                    3 -> {
                        findNavController().navigate(R.id.more_to_suggestions)
                    }
                    4 -> {
                        findNavController().navigate(R.id.more_to_policy)
                    }
                    5 -> {
                        findNavController().navigate(R.id.more_to_contact_us)
                    }
                    6 -> {
                        Utils.shareApp(requireActivity(), "مشاركة تطبيق صالونات مع اصدقائك", "https://play.google.com/store/apps/details?id=app.grand.borders")
                    }
                    7 -> {
                        Utils.startDialogActivity(requireActivity(), DialogLanguageFragment::class.java.name, Codes.DIALOG_SELECT_LANGUAGE, null)
                    }
                    8 -> {
                        when {
                            PrefMethods.getUserData() == null -> {
                                requireActivity().startActivity(Intent(requireActivity(), AuthActivity::class.java))
                                requireActivity().finishAffinity()
                            }
                            else -> {
                                PrefMethods.deleteUserData()
                                requireActivity().startActivity(Intent(requireActivity(), AuthActivity::class.java))
                                requireActivity().finishAffinity()
                            }
                        }
                    }
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when(requestCode){
            Codes.DIALOG_SELECT_LANGUAGE -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        var lang = data.getStringExtra(Params.LANGUAGE)
                                        LocalUtil.changeLang(requireActivity(), lang.toString())
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}