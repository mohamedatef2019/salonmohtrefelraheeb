package grand.app.salonmohtref.main.cancelapolicy.response

import com.google.gson.annotations.SerializedName

data class PrivacyPolicyResponse(

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val privacyData: PrivacyData? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class PrivacyData(

	@field:SerializedName("img")
	val img: String? = null,

	@field:SerializedName("cancellation_policy")
	val cancellationPolicyList: List<CancellationPolicyItem?>? = null
)

data class CancellationPolicyItem(

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("desc")
	val desc: String? = null
)
