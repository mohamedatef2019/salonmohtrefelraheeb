package grand.app.salonmohtref.main.rates.viewmodel

import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.base.LoadingStatus
import grand.app.salonmohtref.main.rates.response.RateItem
import grand.app.salonmohtref.main.rates.response.RatesResponse
import grand.app.salonmohtref.main.rates.view.RatesAdapter
import grand.app.salonmohtref.network.ApiResponse
import grand.app.salonmohtref.utils.PrefMethods
import grand.app.salonmohtref.utils.constants.Codes
import grand.app.salonmohtref.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.ArrayList

class RatesViewModel : BaseViewModel()
{
    var adapter = RatesAdapter()

    fun getRates(salonId : Int) {
        obsLayout.set(LoadingStatus.SHIMMER)
        requestCall<RatesResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().getRates(salonId) }
        })
        { res ->
            when (res!!.code) {
                200 -> {
                    if (res.ratesList!!.isEmpty()) {
                        obsLayout.set(LoadingStatus.EMPTY)
                    }
                    else {
                        obsLayout.set(LoadingStatus.FULL)
                        adapter.updateList(res.ratesList as ArrayList<RateItem>)
                    }
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }

    fun getAllRates() {
        obsLayout.set(LoadingStatus.SHIMMER)
        requestCall<RatesResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().getAllRates() }
        })
        { res ->
            when (res!!.code) {
                200 -> {
                    if (res.ratesList!!.isEmpty()) {
                        obsLayout.set(LoadingStatus.EMPTY)
                    }
                    else {
                        obsLayout.set(LoadingStatus.FULL)
                        adapter.updateList(res.ratesList as ArrayList<RateItem>)
                    }
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }

    fun onAddRateClicked()
    {
        when {
            PrefMethods.getUserData() == null -> {
                setValue(Codes.NOT_LOGIN)
            }
            else -> {
                setValue(Codes.RATE_APP_CLICKED)
            }
        }
    }
}