package grand.app.salonmohtref.main.terms.viewmodel

import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.main.terms.response.TermsItem

class ItemTermsViewModel(var item: TermsItem) : BaseViewModel()