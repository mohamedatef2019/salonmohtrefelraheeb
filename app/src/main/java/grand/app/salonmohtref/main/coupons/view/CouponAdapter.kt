package grand.app.salonmohtref.main.coupons.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.salonmohtref.R
import grand.app.salonmohtref.databinding.ItemCouponBinding
import grand.app.salonmohtref.main.coupons.response.CouponItem
import grand.app.salonmohtref.main.coupons.viewmodel.ItemCouponViewModel
import grand.app.salonmohtref.utils.SingleLiveEvent
import java.util.*

class CouponAdapter : RecyclerView.Adapter<CouponAdapter.CouponHolder>()
{
    var itemsList: ArrayList<CouponItem> = ArrayList()
    var itemLiveData = SingleLiveEvent<CouponItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CouponHolder
    {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemCouponBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_coupon, parent, false)
        return CouponHolder(binding)
    }

    override fun onBindViewHolder(holder: CouponHolder, position: Int) {
        val itemViewModel = ItemCouponViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.binding.rawLayout.setOnClickListener {
            itemLiveData.value = itemViewModel.item
        }
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: List<CouponItem?>?) {
        itemsList = models as ArrayList<CouponItem>
        notifyDataSetChanged()
    }

    inner class CouponHolder(val binding: ItemCouponBinding) : RecyclerView.ViewHolder(binding.root)
}
