package grand.app.salonmohtref.main.salondetails.viewmodel

import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.main.salondetails.response.SalonCategoryItem
import grand.app.salonmohtref.main.salondetails.response.SalonServiceItem
import grand.app.salonmohtref.main.salondetails.view.SalonServicesAdapter
import java.util.ArrayList

class ItemSalonCategoryViewModel(var item: SalonCategoryItem) : BaseViewModel()
{
    var adapter = SalonServicesAdapter()

    init {
        adapter.updateList(item.salonServicesList as ArrayList<SalonServiceItem>)
    }
}