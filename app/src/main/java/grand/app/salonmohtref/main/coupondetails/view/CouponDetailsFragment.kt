package grand.app.salonmohtref.main.coupondetails.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import grand.app.salonmohtref.activity.home.BaseHomeFragment
import grand.app.salonmohtref.R
import grand.app.salonmohtref.activity.home.MainActivity
import grand.app.salonmohtref.databinding.FragmentCouponDetailsBinding
import grand.app.salonmohtref.dialogs.coupon_added.DialogCouponAddedFragment
import grand.app.salonmohtref.dialogs.login.DialogLoginFragment
import grand.app.salonmohtref.dialogs.orderbooked.DialogOrderBookedFragment
import grand.app.salonmohtref.main.confirmorder.response.CreateOrderResponse
import grand.app.salonmohtref.main.coupondetails.response.AddCouponResponse
import grand.app.salonmohtref.main.coupondetails.viewmodel.CouponDetailsViewModel
import grand.app.salonmohtref.network.Status
import grand.app.salonmohtref.utils.Utils
import grand.app.salonmohtref.utils.constants.Codes
import grand.app.salonmohtref.utils.constants.Params
import grand.app.salonmohtref.utils.observe
import timber.log.Timber

class CouponDetailsFragment : BaseHomeFragment()
{
    lateinit var binding: FragmentCouponDetailsBinding
    lateinit var viewModel: CouponDetailsViewModel
    private val fragmentArgs : CouponDetailsFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_coupon_details, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(CouponDetailsViewModel::class.java)
        binding.viewModel = viewModel

        showBottomBar(false)

        viewModel.fillCouponData(fragmentArgs.couponItem)

        binding.ibBack.setOnClickListener {
            findNavController().navigateUp()
        }

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is AddCouponResponse -> {
                            val bundle = Bundle()
                            bundle.putInt(Params.COUPON_POINTS, fragmentArgs.couponItem.points!!)
                            Utils.startDialogActivity(requireActivity(), DialogCouponAddedFragment::class.java.name, Codes.DIALOG_COUPON_ADDED, bundle)
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }


        observe(viewModel.mutableLiveData){
            when (it) {
                Codes.NOT_LOGIN -> {
                    Utils.startDialogActivity(
                        requireActivity(),
                        DialogLoginFragment::class.java.name,
                        Codes.DIALOG_LOGIN_REQUEST,
                        null
                    )
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when(requestCode) {
            Codes.DIALOG_LOGIN_REQUEST -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        viewModel.onExchangeClicked()
                                    }
                                }
                            }
                        }
                    }
                }
            }

            Codes.DIALOG_COUPON_ADDED -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        requireActivity().startActivity(Intent(requireActivity(), MainActivity::class.java))
                                        requireActivity().finishAffinity()
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}