package grand.app.salonmohtref.main.ordertime.time

data class ItemSalonTime(
        var time : String ?= null,
        var isSelected : Boolean ?= null
)
