package grand.app.salonmohtref.main.more.viewmodel

import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.main.bottombar.more.view.ItemMoreProfile

class ItemMoreProfileViewModel(var item: ItemMoreProfile) : BaseViewModel()