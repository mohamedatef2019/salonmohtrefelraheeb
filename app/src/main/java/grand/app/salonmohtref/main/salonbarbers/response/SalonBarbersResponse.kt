package grand.app.salonmohtref.main.salonbarbers.response

import com.google.gson.annotations.SerializedName

data class SalonBarbersResponse(

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val salonBarberData: SalonBarbersData? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class SalonBarbersData(

	@field:SerializedName("first_page_url")
	val firstPageUrl: String? = null,

	@field:SerializedName("path")
	val path: String? = null,

	@field:SerializedName("per_page")
	val perPage: Int? = null,

	@field:SerializedName("total")
	val total: Int? = null,

	@field:SerializedName("data")
	val barbersList: List<SalonBarberItem?>? = null,

	@field:SerializedName("last_page")
	val lastPage: Int? = null,

	@field:SerializedName("last_page_url")
	val lastPageUrl: String? = null,

	@field:SerializedName("next_page_url")
	val nextPageUrl: Any? = null,

	@field:SerializedName("from")
	val from: Int? = null,

	@field:SerializedName("to")
	val to: Int? = null,

	@field:SerializedName("prev_page_url")
	val prevPageUrl: Any? = null,

	@field:SerializedName("current_page")
	val currentPage: Int? = null
)

data class WorkingsItem(

	@field:SerializedName("img")
	val img: String? = null,

	@field:SerializedName("barber_id")
	val barberId: Int? = null,

	@field:SerializedName("id")
	val id: Int? = null
)

data class SalonBarberItem(

	@field:SerializedName("img")
	val img: String? = null,

	@field:SerializedName("salon_id")
	val salonId: Int? = null,

	@field:SerializedName("is_favorite")
	var isFavorite: Int? = null,

	@field:SerializedName("tue")
	val tue: Int? = null,

	@field:SerializedName("sat")
	val sat: Int? = null,

	@field:SerializedName("end_time")
	val endTime: String? = null,

	@field:SerializedName("salon_name")
	val salonName: String? = null,

	@field:SerializedName("sun")
	val sun: Int? = null,

	@field:SerializedName("mon")
	val mon: Int? = null,

	@field:SerializedName("branch")
	val branch: String? = null,

	@field:SerializedName("start_time")
	val startTime: String? = null,

	@field:SerializedName("rate")
	val rate: String? = null,

	@field:SerializedName("working_hours")
	val workingHours: String? = null,

	@field:SerializedName("wed")
	val wed: Int? = null,

	@field:SerializedName("rates_count")
	val ratesCount: Int? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("fri")
	val fri: Int? = null,

	@field:SerializedName("thr")
	val thr: Int? = null,

	@field:SerializedName("status")
	val status: Int? = null,

	@field:SerializedName("workings")
	val workings: List<WorkingsItem?>? = null
)
