package grand.app.salonmohtref.main.bottombar.more.view

import android.graphics.drawable.Drawable

data class ItemMoreProfile(
        var id : Int ?= null,
        var title : String ?= null,
        var image : Drawable?= null
)
