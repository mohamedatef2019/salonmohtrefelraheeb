package grand.app.salonmohtref.main.rates.viewmodel

import androidx.databinding.ObservableField
import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.main.rates.response.RateItem
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class ItemRateViewModel(var item: RateItem) : BaseViewModel()
{
    var obsRate = ObservableField<Float>()
    var obsTime = ObservableField<String>()
    var date = Date()

    init {
        item.let {

            obsRate.set(item.rate!!.toFloat())

            when {
                item.createdAt != null -> {
                    val format = SimpleDateFormat("d MMM yyyy  hh:mm aaa")
                    try {
                        date = format.parse(it.createdAt)
                    } catch (e: ParseException) {
                        e.printStackTrace()
                    }

                    val dayFormat = SimpleDateFormat("dd-MM-yyyy", Locale("en"))
                    obsTime.set(dayFormat.format(date).toString());
                }
            }
        }
    }
}