package grand.app.salonmohtref.main.favorites.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import grand.app.salonmohtref.R
import grand.app.salonmohtref.activity.home.BaseHomeFragment
import grand.app.salonmohtref.base.BaseApp
import grand.app.salonmohtref.databinding.FragmentFavoritesBinding
import grand.app.salonmohtref.main.favorites.barbers.view.FavoriteBarbersFragment
import grand.app.salonmohtref.main.favorites.salons.view.FavoriteSalonsFragment
import grand.app.salonmohtref.main.favorites.viewmodel.FavoritesViewModel
import grand.app.salonmohtref.utils.constants.Codes
import grand.app.salonmohtref.utils.observe
import grand.app.salonmohtref.utils.viewpager.DepthPageTransformer

class FavoritesFragment : BaseHomeFragment() {

    private lateinit var binding : FragmentFavoritesBinding
    private lateinit var viewModel: FavoritesViewModel
    lateinit var viewPager : ViewPager2

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_favorites, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(this).get(FavoritesViewModel::class.java)
        binding.viewModel = viewModel

        showBottomBar(false)

        observe(viewModel.mutableLiveData) {
            when (it) {
                Codes.BARBER_ITEM -> {
                     viewPager.currentItem = 1
                }
                Codes.SALONS_ITEM -> {
                     viewPager.currentItem = 0
                }
            }
        }

        initViewPager()
    }

    private fun initViewPager() {

        // Instantiate a ViewPager2 and a PagerAdapter.
        viewPager = binding.vpFavorites

        // The pager adapter, which provides the pages to the view pager widget.
        val pagerAdapter = FavoritePagerAdapter(requireActivity())
        viewPager.adapter = pagerAdapter

        viewPager.setPageTransformer(DepthPageTransformer())

        viewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                viewModel.onPageSelected(position)
            }
        })
    }

    /**
     * A simple pager adapter that represents 5 ScreenSlidePageFragment objects, in
     * sequence.
     */
    private inner class FavoritePagerAdapter(fa: FragmentActivity) : FragmentStateAdapter(fa) {
        override fun getItemCount(): Int = 2

        override fun createFragment(position: Int): Fragment {
            return when (position) {
                0 -> FavoriteSalonsFragment()
                else -> FavoriteBarbersFragment()
            }
        }
    }
}