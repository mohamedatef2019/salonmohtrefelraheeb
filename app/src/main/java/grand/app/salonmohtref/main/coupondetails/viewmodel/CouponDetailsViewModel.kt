package grand.app.salonmohtref.main.coupondetails.viewmodel

import androidx.databinding.ObservableField
import grand.app.salonmohtref.R
import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.main.coupondetails.response.AddCouponResponse
import grand.app.salonmohtref.main.coupondetails.view.CouponDetailsServicesAdapter
import grand.app.salonmohtref.main.coupons.response.CouponItem
import grand.app.salonmohtref.network.ApiResponse
import grand.app.salonmohtref.utils.PrefMethods
import grand.app.salonmohtref.utils.constants.Codes
import grand.app.salonmohtref.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.*

class CouponDetailsViewModel : BaseViewModel()
{
    var adapter = CouponDetailsServicesAdapter()
    private var couponData = CouponItem()
    var obsCouponName = ObservableField<String>()

    fun fillCouponData(couponItem : CouponItem) {
        couponData = couponItem
        obsCouponName.set("""${getString(R.string.label_coupon)} ${getString(R.string.label_right_bracket)}${couponItem.points}${getString(R.string.label_left_bracket)}""")

        couponData.services?.let {
            adapter.updateList(couponData.services)
        }
    }

    fun onExchangeClicked() {

        when {
            PrefMethods.getUserData() == null -> {
                setValue(Codes.NOT_LOGIN)
            }
            else -> {
                obsIsProgress.set(true)
                requestCall<AddCouponResponse?>({ withContext(Dispatchers.IO) { return@withContext getApiRepo().addCoupon(couponData.id!!) } })
                { res ->
                    obsIsProgress.set(false)
                    when (res?.code) {
                        200 -> {
                            apiResponseLiveData.value = ApiResponse.success(res)
                        }
                        else -> {
                            apiResponseLiveData.value = ApiResponse.errorMessage(res?.msg.toString())
                        }
                    }
                }
            }
        }
    }
}
