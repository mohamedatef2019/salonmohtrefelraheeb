package grand.app.salonmohtref.main.branches.salon.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import grand.app.salonmohtref.R
import grand.app.salonmohtref.activity.auth.AuthActivity
import grand.app.salonmohtref.activity.home.BaseHomeFragment
import grand.app.salonmohtref.databinding.FragmentSalonBranchesBinding
import grand.app.salonmohtref.dialogs.login.DialogLoginFragment
import grand.app.salonmohtref.location.selectlocation.view.SelectLocationActivity
import grand.app.salonmohtref.location.util.AddressItem
import grand.app.salonmohtref.main.allsalonss.response.SalonItem
import grand.app.salonmohtref.main.branches.salon.viewmodel.SalonBranchesViewModel
import grand.app.salonmohtref.main.branches.view.AllBranchesFragmentDirections
import grand.app.salonmohtref.main.favorites.response.AddToFavResponse
import grand.app.salonmohtref.network.Status
import grand.app.salonmohtref.utils.PrefMethods
import grand.app.salonmohtref.utils.Utils
import grand.app.salonmohtref.utils.constants.Codes
import grand.app.salonmohtref.utils.constants.Const
import grand.app.salonmohtref.utils.constants.Params
import grand.app.salonmohtref.utils.observe
import timber.log.Timber

class SalonBranchesFragment : BaseHomeFragment()
{
    private lateinit var binding : FragmentSalonBranchesBinding
    private lateinit var viewModel: SalonBranchesViewModel
    var branchItem = SalonItem()

    private val navController by lazy {
        activity.let {
            Navigation.findNavController(it!!, R.id.nav_home_host_fragment)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_salon_branches, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(this).get(SalonBranchesViewModel::class.java)
        binding.viewModel = viewModel


        Params.mutableActionLiveData.observe(requireActivity(), Observer {
            when (it) {
                1 -> {
                    Const.isAskedToLogin = 1
                    Params.mutableActionLiveData.value = 0
                    requireActivity().startActivity(Intent(requireActivity(), AuthActivity::class.java).putExtra(Const.ACCESS_LOGIN , true))

                }
            }
        })

        when {
            PrefMethods.getUserLocation() == null -> {
                val intent = Intent(requireActivity(), SelectLocationActivity::class.java)
                startActivityForResult(intent, Codes.LOCATION_REQUEST)
            }
        }

        binding.inputHomeSearch.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                viewModel.onSearchClicked()
                return@OnEditorActionListener true
            }
            false
        })

        observe(viewModel.adapter.favLiveData){
            branchItem = it!!
            when {
                PrefMethods.getUserData() == null -> {
                    Utils.startDialogActivity(requireActivity(), DialogLoginFragment::class.java.name, Codes.DIALOG_LOGIN_REQUEST, null)
                }
                else -> {
                    viewModel.addToFav(it.id!!)
                }
            }
        }

        observe(viewModel.adapter.itemLiveData){
            navController.navigate(AllBranchesFragmentDirections.branchesToDetails(it?.id!!, 1))
        }

        observe(viewModel.adapter.rateLiveData){
            navController.navigate(AllBranchesFragmentDirections.branchesToRates(it?.id!!, 1))
        }

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is AddToFavResponse -> {
                            showToast(it.data.msg.toString(), 2)
                            branchItem.isFavorite = it.data.addFavData?.isFavorite
                            viewModel.adapter.notifyItemSelected(branchItem)
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }

        binding.inputHomeSearch.text?.clear()
        viewModel.obsSearchNam.set(null)
        viewModel.notifyChange()

        observe(viewModel.mutableLiveData) {
            when (it) {
                Codes.SEARCH_CLICKED -> {
                    navController.navigate(AllBranchesFragmentDirections.allBranchesToSearch(viewModel.obsSearchNam.get().toString(), 1))
                }
                Codes.EMPTY_SALON_NAME -> {
                    showToast(getString(R.string.msg_empty_salon_name) , 1)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when(requestCode){
            Codes.DIALOG_LOGIN_REQUEST -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        Const.isAskedToLogin = 1
                                        requireActivity().startActivity(Intent(requireActivity(), AuthActivity::class.java).putExtra(Const.ACCESS_LOGIN , true))
                                    }
                                }
                            }
                        }
                    }
                }
            }
            Codes.LOCATION_REQUEST -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        val userAddress = data.getParcelableExtra<AddressItem>(Params.ADDRESS_ITEM)
                                        viewModel.request.lat = userAddress!!.lat
                                        viewModel.request.lng = userAddress.lng
                                        viewModel.getAllSalons()
                                        viewModel.obsAddress.set(userAddress.address)
                                        viewModel.obsAddress.notifyChange()
                                    }
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 0 -> {
                                        findNavController().navigateUp()
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        when (Const.isAskedToLogin) {
            1 -> {
                viewModel.getAllSalons()
                Const.isAskedToLogin = 0
            }
        }
    }
}