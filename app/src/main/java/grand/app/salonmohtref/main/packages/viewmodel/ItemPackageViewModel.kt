package grand.app.salonmohtref.main.packages.viewmodel

import androidx.databinding.ObservableField
import grand.app.salonmohtref.R
import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.main.packages.response.PackageItem

class ItemPackageViewModel(var item: PackageItem) : BaseViewModel()
{
    var obsPeriod = ObservableField<String>()
    var obsPrice = ObservableField<String>()

    init {
        obsPeriod.set("""اشتراك لمدة ${item.period} ${getString(R.string.label_months)}""")
        obsPrice.set("""اشتراك  ${item.price} ${getString(R.string.label_currency)}""")
    }
}