package grand.app.salonmohtref.main.profile.viewmodel

import androidx.core.content.ContextCompat
import grand.app.salonmohtref.R
import grand.app.salonmohtref.base.BaseApp
import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.main.bottombar.more.view.ItemMoreProfile
import grand.app.salonmohtref.main.more.view.MoreProfileAdapter

class ProfileViewModel : BaseViewModel()
{
    var adapter = MoreProfileAdapter()

    init {
        val moreList = arrayListOf(
                ItemMoreProfile(0, getString(R.string.title_my_info), ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.ic_account)),
                ItemMoreProfile(1, getString(R.string.title_change_password), ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.ic_password)),
                ItemMoreProfile(2, getString(R.string.title_my_bookings), ContextCompat.getDrawable(BaseApp.getInstance, R.drawable. ic_booking)),
                ItemMoreProfile(3, getString(R.string.title_my_credit), ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.ic_my_credit)),
                ItemMoreProfile(4, getString(R.string.title_my_wallet), ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.ic_suggestions)),
                ItemMoreProfile(5, getString(R.string.title_packages), ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.ic_quota)),
                ItemMoreProfile(6, getString(R.string.title_favorite), ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.ic_favorite)),
                ItemMoreProfile(7, getString(R.string.title_coupons), ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.ic_coupon))
        )

        adapter.updateList(moreList)
    }
}