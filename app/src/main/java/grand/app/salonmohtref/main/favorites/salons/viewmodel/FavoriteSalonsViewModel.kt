package grand.app.salonmohtref.main.favorites.salons.viewmodel

import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.base.LoadingStatus
import grand.app.salonmohtref.main.favorites.response.AddToFavResponse
import grand.app.salonmohtref.main.favorites.response.FavoriteSalonItem
import grand.app.salonmohtref.main.favorites.response.FavoriteSalonResponse
import grand.app.salonmohtref.main.favorites.salons.view.FavoriteSalonsAdapter
import grand.app.salonmohtref.network.ApiResponse
import grand.app.salonmohtref.utils.PrefMethods
import grand.app.salonmohtref.utils.constants.Codes
import grand.app.salonmohtref.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.ArrayList

class FavoriteSalonsViewModel : BaseViewModel() {

    var adapter = FavoriteSalonsAdapter()
    var salonsList = ArrayList<FavoriteSalonItem>()

    private fun getFavBarbers() {

        obsLayout.set(LoadingStatus.SHIMMER)
        requestCall<FavoriteSalonResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().getFavoriteSalons() } })
        { res ->
            when (res!!.code) {
                200 -> {
                    apiResponseLiveData.value = ApiResponse.success(res)
                    when {
                        res.favoriteData?.favoritesList.isNullOrEmpty() || res.favoriteData?.favoritesList!!.isEmpty() -> {
                            obsLayout.set(LoadingStatus.EMPTY)
                        }
                        else -> {
                            obsLayout.set(LoadingStatus.FULL)
                            salonsList = res.favoriteData.favoritesList as ArrayList<FavoriteSalonItem>
                            adapter.updateList(salonsList)
                        }
                    }
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }

    fun addToFav(salonItem : FavoriteSalonItem) {
        obsIsProgress.set(true)
        requestCall<AddToFavResponse?>({ withContext(Dispatchers.IO) { return@withContext getApiRepo().addToFav(salonItem.id!!, 0) } })
        { res ->
            obsIsProgress.set(false)
            when (res!!.code) {
                200 -> {
                    apiResponseLiveData.value = ApiResponse.success(res)
                    adapter.removeItem(salonItem)
                    salonsList.remove(salonItem)
                    apiResponseLiveData.value = ApiResponse.success(res)

                    when (salonsList.size) {
                        0 -> {
                            obsLayout.set(LoadingStatus.EMPTY)
                        }
                        else -> {
                            obsLayout.set(LoadingStatus.FULL)

                        }
                    }
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }

    fun onLoginClicked() {
        setValue(Codes.LOGIN_CLICKED)
    }

    fun getUserStatus() {
        when {
            PrefMethods.getUserData() != null -> {
                getFavBarbers()
            }
            else -> {
                obsLayout.set(LoadingStatus.NOTLOGIN)
            }
        }
    }

    init {
        getUserStatus()
    }
}