package grand.app.salonmohtref.main.suggestion.view

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import grand.app.salonmohtref.main.suggestion.viewmodel.SuggestViewModel
import grand.app.salonmohtref.R
import grand.app.salonmohtref.activity.home.BaseHomeFragment
import grand.app.salonmohtref.databinding.FragmentSuggestBinding
import grand.app.salonmohtref.main.suggestion.response.SuggestResponse
import grand.app.salonmohtref.network.Status
import grand.app.salonmohtref.utils.PrefMethods
import grand.app.salonmohtref.utils.Utils
import grand.app.salonmohtref.utils.constants.Codes
import grand.app.salonmohtref.utils.observe
import timber.log.Timber

class SuggestFragment : BaseHomeFragment()
{
    lateinit var binding: FragmentSuggestBinding
    lateinit var viewModel: SuggestViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_suggest, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(SuggestViewModel::class.java)
        binding.viewModel = viewModel

        showBottomBar(false)

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is SuggestResponse -> {
                            showToast(it.data.msg.toString(), 2)
                            findNavController().navigateUp()
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }

        observe(viewModel.mutableLiveData)
        {
            when (it) {
                Codes.EMPTY_NAME -> {
                    showToast(getString(R.string.msg_empty_name), 1)
                }
                Codes.EMAIL_EMPTY -> {
                    showToast(getString(R.string.msg_empty_email), 1)
                }
                Codes.INVALID_EMAIL -> {
                    showToast(getString(R.string.msg_invalid_email), 1)
                }
                Codes.EMPTY_PHONE -> {
                    showToast(getString(R.string.msg_empty_phone), 1)
                }
                Codes.EMPTY_MESSAGE -> {
                    showToast(getString(R.string.msg_empty_message), 1)
                }
                Codes.FACE_CLICKED -> {
                    Utils.openFacebook(requireActivity(), PrefMethods.getSettingData()?.facebook)
                }
                Codes.TWITTER_CLICKED -> {
                    Utils.openTwitter(requireActivity(), PrefMethods.getSettingData()?.twitter)
                }
                Codes.WHATSAPP_CLICKED -> {
                    val url = "https://api.whatsapp.com/send?phone=" + PrefMethods.getSettingData()?.whatsapp
                    val i = Intent(Intent.ACTION_VIEW)
                    i.data = Uri.parse(url)
                    startActivity(i)
                }
            }
        }
    }
}