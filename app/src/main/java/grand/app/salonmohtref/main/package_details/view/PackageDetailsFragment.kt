package grand.app.salonmohtref.main.package_details.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import grand.app.salonmohtref.activity.home.BaseHomeFragment
import grand.app.salonmohtref.R
import grand.app.salonmohtref.activity.auth.AuthActivity
import grand.app.salonmohtref.activity.home.MainActivity
import grand.app.salonmohtref.databinding.FragmentPackageDetailsBinding
import grand.app.salonmohtref.dialogs.orderbooked.DialogOrderBookedFragment
import grand.app.salonmohtref.main.package_details.response.AddPackageResponse
import grand.app.salonmohtref.main.package_details.viewmodel.PackageDetailsViewModel
import grand.app.salonmohtref.network.Status
import grand.app.salonmohtref.utils.PrefMethods
import grand.app.salonmohtref.utils.Utils
import grand.app.salonmohtref.utils.constants.Codes
import grand.app.salonmohtref.utils.constants.Const
import grand.app.salonmohtref.utils.constants.Params
import grand.app.salonmohtref.utils.observe
import timber.log.Timber

class PackageDetailsFragment : BaseHomeFragment()
{
    lateinit var binding: FragmentPackageDetailsBinding
    lateinit var viewModel: PackageDetailsViewModel
    val fragmentArgs : PackageDetailsFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_package_details, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(PackageDetailsViewModel::class.java)
        binding.viewModel = viewModel

        showBottomBar(false)

        viewModel.fillPackageData(fragmentArgs.packageItem)

        binding.ibBack.setOnClickListener {
            findNavController().navigateUp()
        }

        observe(viewModel.mutableLiveData){
            when (it) {
                Codes.LOGIN_CLICKED -> {
                    Const.isAskedToLogin = 1
                    requireActivity().startActivity(
                        Intent(requireActivity(), AuthActivity::class.java).putExtra(
                            Const.ACCESS_LOGIN , true))
                }
            }
        }

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is AddPackageResponse -> {
                            val bundle = Bundle()
                            bundle.putString(Params.ORDER_MSG, getString(R.string.label_package_subscribed))
                            Utils.startDialogActivity(requireActivity(), DialogOrderBookedFragment::class.java.name, Codes.DIALOG_ORDER_SUCCESS, bundle)
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        when (Const.isAskedToLogin) {
            1 -> {
                viewModel.getUserStatus()
                Const.isAskedToLogin = 0
            }
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when(requestCode){
            Codes.DIALOG_ORDER_SUCCESS -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        findNavController().navigate(R.id.package_details_to_my_package)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
