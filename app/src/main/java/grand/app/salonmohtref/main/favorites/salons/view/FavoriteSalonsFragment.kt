package grand.app.salonmohtref.main.favorites.salons.view

import android.content.Intent
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import grand.app.salonmohtref.R
import grand.app.salonmohtref.activity.auth.AuthActivity
import grand.app.salonmohtref.activity.home.BaseHomeFragment
import grand.app.salonmohtref.databinding.FragmentFavoriteSalonsBinding
import grand.app.salonmohtref.dialogs.login.DialogLoginFragment
import grand.app.salonmohtref.main.favorites.response.AddToFavResponse
import grand.app.salonmohtref.main.favorites.response.FavoriteSalonItem
import grand.app.salonmohtref.main.favorites.salons.viewmodel.FavoriteSalonsViewModel
import grand.app.salonmohtref.main.favorites.view.FavoritesFragmentDirections
import grand.app.salonmohtref.network.Status
import grand.app.salonmohtref.utils.PrefMethods
import grand.app.salonmohtref.utils.Utils
import grand.app.salonmohtref.utils.constants.Codes
import grand.app.salonmohtref.utils.constants.Const
import grand.app.salonmohtref.utils.constants.Params
import grand.app.salonmohtref.utils.observe
import timber.log.Timber

class FavoriteSalonsFragment : BaseHomeFragment() {

    private lateinit var binding : FragmentFavoriteSalonsBinding
    private lateinit var viewModel: FavoriteSalonsViewModel
    var barberItem = FavoriteSalonItem()

    private val navController by lazy {
        activity.let {
            Navigation.findNavController(it!!, R.id.nav_home_host_fragment)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_favorite_salons, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(FavoriteSalonsViewModel::class.java)
        binding.viewModel = viewModel

        observe(viewModel.adapter.favLiveData){
            barberItem = it!!
            when {
                PrefMethods.getUserData() == null -> {
                    Utils.startDialogActivity(requireActivity(), DialogLoginFragment::class.java.name, Codes.DIALOG_LOGIN_REQUEST, null)
                }
                else -> {
                    viewModel.addToFav(it)
                }
            }
        }

        observe(viewModel.adapter.itemLiveData){
            navController.navigate(FavoritesFragmentDirections.favoritesToSalonDetails(it!!.id!!, -1))
        }

        observe(viewModel.mutableLiveData){
            when (it) {
                Codes.LOGIN_CLICKED -> {
                    Const.isAskedToLogin = 1
                    requireActivity().startActivity(Intent(requireActivity(), AuthActivity::class.java).putExtra(Const.ACCESS_LOGIN , true))
                }
            }
        }

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is AddToFavResponse -> {
                            showToast(it.data.msg.toString(), 2)
                            barberItem.isFavorite = it.data.addFavData?.isFavorite
                            viewModel.adapter.notifyItemSelected(barberItem)
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when(requestCode){
            Codes.DIALOG_LOGIN_REQUEST -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        Const.isAskedToLogin = 1
                                        requireActivity().startActivity(Intent(requireActivity(), AuthActivity::class.java).putExtra(Const.ACCESS_LOGIN , true))
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        when (Const.isAskedToLogin) {
            1 -> {
                viewModel.getUserStatus()
                Const.isAskedToLogin = 0
            }
        }
    }
}