package grand.app.salonmohtref.main.notifications.response

import com.google.gson.annotations.SerializedName

data class NotificationsResponse(

		@field:SerializedName("msg")
	val msg: String? = null,

		@field:SerializedName("code")
	val code: Int? = null,

		@field:SerializedName("data")
	val notificationsList: List<NotificationItem?>? = null,

		@field:SerializedName("status")
	val status: String? = null
)

data class NotificationItem(

	@field:SerializedName("img")
	val img: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("target_id")
	val targetId: Int? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("type")
	val type: Int? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("desc")
	val desc: String? = null
)
