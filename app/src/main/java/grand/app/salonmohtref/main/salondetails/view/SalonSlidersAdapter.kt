package grand.app.salonmohtref.main.salondetails.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.smarteist.autoimageslider.SliderViewAdapter
import grand.app.salonmohtref.R
import grand.app.salonmohtref.databinding.ItemSalonSliderBinding
import grand.app.salonmohtref.main.salondetails.response.MediaItem
import grand.app.salonmohtref.main.salondetails.viewmodel.ItemSalonSliderViewModel

class SalonSlidersAdapter : SliderViewAdapter<SalonSlidersAdapter.SalonSliderHolder>()
{
    var itemsList: List<MediaItem> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup?): SalonSliderHolder {
        val context = parent!!.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemSalonSliderBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_salon_slider, parent, false)
        return SalonSliderHolder(binding)
    }

    override fun onBindViewHolder(holder: SalonSliderHolder, position: Int) {
        val itemViewModel = ItemSalonSliderViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel
    }

    fun updateList(models: List<MediaItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class SalonSliderHolder(val binding: ItemSalonSliderBinding) : SliderViewAdapter.ViewHolder(binding.root)

    override fun getCount(): Int {
        return itemsList.size
    }
}
