package grand.app.salonmohtref.main.salondetails.response

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class SalonDetailsResponse(

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val salonData: SalonDetailsData? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class Logo(

	@field:SerializedName("img")
	val img: String? = null,

	@field:SerializedName("salon_id")
	val salonId: Int? = null,

	@field:SerializedName("default_type")
	val defaultType: Int? = null,

	@field:SerializedName("id")
	val id: Int? = null
)

data class SalonCategoryItem(

	@field:SerializedName("img")
	val img: String? = null,

	@field:SerializedName("subs")
	val salonServicesList: List<SalonServiceItem?>? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	var isSelected : Boolean = false
)

data class MediaItem(

	@field:SerializedName("img")
	val img: String? = null,

	@field:SerializedName("salon_id")
	val salonId: Int? = null,

	@field:SerializedName("default_type")
	val defaultType: Int? = null,

	@field:SerializedName("id")
	val id: Int? = null
)

data class CategoriesItem(

	@field:SerializedName("duration")
	val duration: String? = null,

	@field:SerializedName("salon_id")
	val salonId: Int? = null,

	@field:SerializedName("category_id")
	val categoryId: Int? = null,

	@field:SerializedName("sub_category_id")
	val subCategoryId: Int? = null,

	@field:SerializedName("price")
	val price: Int? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("switch")
	val jsonMemberSwitch: Int? = null
)

data class TakenTimesItem(

	@field:SerializedName("date")
	val date: String? = null,

	@field:SerializedName("salon_id")
	val salonId: Int? = null,

	@field:SerializedName("user_name")
	val userName: String? = null,

	@field:SerializedName("time")
	val time: String? = null,

	@field:SerializedName("user_img")
	val userImg: String? = null
)

data class City(

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null
)

data class TimesItem(

	@field:SerializedName("start_time")
	val startTime: String? = null,

	@field:SerializedName("salon_id")
	val salonId: Int? = null,

	@field:SerializedName("end_time")
	val endTime: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("day")
	val day: String? = null,

	@field:SerializedName("switch")
	val jsonMemberSwitch: Int? = null
)

data class SalonDetailsData(

	@field:SerializedName("is_favorite")
	val isFavorite: Int? = null,

	@field:SerializedName("lng")
	val lng: String? = null,

	@field:SerializedName("categories_names")
	val salonCategoriesList: List<SalonCategoryItem?>? = null,

	@field:SerializedName("city")
	val city: City? = null,

	@field:SerializedName("certificate")
	val certificate: String? = null,

	@field:SerializedName("start_time")
	val startTime: String? = null,

	@field:SerializedName("end_time")
	val endTime: String? = null,

	@field:SerializedName("media")
	val media: List<MediaItem?>? = null,

	@field:SerializedName("taken_times")
	val takenTimes: List<TakenTimesItem?>? = null,

	@field:SerializedName("license")
	val license: String? = null,

	@field:SerializedName("times")
	val times: List<TimesItem?>? = null,

	@field:SerializedName("min_price")
	val minPrice: Int? = null,

	@field:SerializedName("rate")
	val rate: String? = null,

	@field:SerializedName("rates_count")
	val ratesCount: String? = null,

	@field:SerializedName("phone")
	val phone: String? = null,

	@field:SerializedName("working_hours")
	val workingHours: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("salon_type")
	val salonType: Int? = null,

	@field:SerializedName("logo")
	val logo: Logo? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("categories")
	val categories: List<CategoriesItem?>? = null,

	@field:SerializedName("lat")
	val lat: String? = null,

	@field:SerializedName("desc")
	val desc: String? = null
) : Parcelable {
	constructor(parcel: Parcel) : this(
			parcel.readValue(Int::class.java.classLoader) as? Int,
			parcel.readString(),
			TODO("salonCategoriesList"),
			TODO("city"),
			parcel.readString(),
			parcel.readString(),
			parcel.readString(),
			TODO("media"),
			TODO("takenTimes"),
			parcel.readString(),
			TODO("times"),
			parcel.readValue(Int::class.java.classLoader) as? Int,
			parcel.readString(),
			parcel.readString(),
			parcel.readString(),
			parcel.readString(),
			parcel.readString(),
			parcel.readValue(Int::class.java.classLoader) as? Int,
			TODO("logo"),
			parcel.readValue(Int::class.java.classLoader) as? Int,
			TODO("categories"),
			parcel.readString(),
			parcel.readString()) {
	}

	override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeValue(isFavorite)
		parcel.writeString(lng)
		parcel.writeString(certificate)
		parcel.writeString(startTime)
		parcel.writeString(endTime)
		parcel.writeString(license)
		parcel.writeValue(minPrice)
		parcel.writeString(rate)
		parcel.writeString(ratesCount)
		parcel.writeString(phone)
		parcel.writeString(workingHours)
		parcel.writeString(name)
		parcel.writeValue(salonType)
		parcel.writeValue(id)
		parcel.writeString(lat)
		parcel.writeString(desc)
	}

	override fun describeContents(): Int {
		return 0
	}

	companion object CREATOR : Parcelable.Creator<SalonDetailsData> {
		override fun createFromParcel(parcel: Parcel): SalonDetailsData {
			return SalonDetailsData(parcel)
		}

		override fun newArray(size: Int): Array<SalonDetailsData?> {
			return arrayOfNulls(size)
		}
	}
}

data class SalonServiceItem(

	@field:SerializedName("duration")
	val duration: String? = null,

	@field:SerializedName("price")
	val price: Int? = null,

	@field:SerializedName("icon")
	val icon: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("switch")
	val jsonMemberSwitch: Int? = null,

	var isSelected : Boolean = false

)
