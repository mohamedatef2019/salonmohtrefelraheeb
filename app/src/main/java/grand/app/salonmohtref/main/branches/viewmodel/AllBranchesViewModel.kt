package grand.app.salonmohtref.main.branches.viewmodel

import androidx.databinding.ObservableInt
import grand.app.salonmohtref.base.BaseViewModel

class AllBranchesViewModel : BaseViewModel() {

    val obsPosition = ObservableInt(0)

    fun onTabClicked(position : Int){
        obsPosition.set(position)
        setValue(position)
    }

    fun onPageSelected(position : Int)
    {
        obsPosition.set(position)
    }
}