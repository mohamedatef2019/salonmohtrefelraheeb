package grand.app.salonmohtref.main.salondetails.viewmodel

import androidx.databinding.ObservableField
import grand.app.salonmohtref.R
import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.base.LoadingStatus
import grand.app.salonmohtref.main.confirmorder.request.OrderRequest
import grand.app.salonmohtref.main.favorites.response.AddToFavResponse
import grand.app.salonmohtref.main.salondetails.response.SalonCategoryItem
import grand.app.salonmohtref.main.salondetails.response.SalonDetailsData
import grand.app.salonmohtref.main.salondetails.response.SalonDetailsResponse
import grand.app.salonmohtref.main.salondetails.view.SalonCategoriesAdapter
import grand.app.salonmohtref.main.salondetails.view.SalonSlidersAdapter
import grand.app.salonmohtref.network.ApiResponse
import grand.app.salonmohtref.utils.PrefMethods
import grand.app.salonmohtref.utils.constants.Codes
import grand.app.salonmohtref.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.*

class SalonDetailsViewModel : BaseViewModel()
{
    var salonDetails = SalonDetailsData()
    var obsRate : Float = 0f
    var sliderAdapter = SalonSlidersAdapter()
    var categoriesAdapter = SalonCategoriesAdapter()
    var obsPrices = ObservableField<String>()
    var obsTimes = ObservableField<String>()
    var orderRequest = OrderRequest()
    var isFirstTime : Boolean = true
    var serviceType = 0   // 0 For Home --- 1 for Salon

    fun getDetails(id : Int)
    {
        when {
            PrefMethods.getUserData() == null -> {
                getVisitorSalonDetails(id)
            }
            else -> {
                getUserSalonDetails(id)
            }
        }
    }

    private fun getUserSalonDetails(salonId : Int) {
        when {
            isFirstTime -> {
                obsLayout.set(LoadingStatus.SHIMMER)
                requestCall<SalonDetailsResponse?>({ withContext(Dispatchers.IO) { return@withContext getApiRepo().getUserSalonDetails(salonId) } })
                { res ->
                    when (res!!.code) {
                        200 -> {
                            obsLayout.set(LoadingStatus.FULL)
                            salonDetails = res.salonData!!
                            obsRate = res.salonData.rate!!.toFloat()

                            obsPrices.set(getSalonPrices(res.salonData.minPrice.toString()))
                            obsTimes.set(getSalonTimes(res.salonData.startTime.toString(), res.salonData.endTime.toString()))

                            apiResponseLiveData.value = ApiResponse.success(res)
                            categoriesAdapter.updateList(res.salonData.salonCategoriesList as ArrayList<SalonCategoryItem>)
                            notifyChange()
                        }
                        else -> {
                            apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                        }
                    }
                }
            }
        }
    }

    private fun getVisitorSalonDetails(salonId : Int) {
        when {
            isFirstTime -> {
                obsLayout.set(LoadingStatus.SHIMMER)
                requestCall<SalonDetailsResponse?>({ withContext(Dispatchers.IO) { return@withContext getApiRepo().getVisitorSalonDetails(salonId) } })
                { res ->
                    when (res!!.code) {
                        200 -> {
                            obsLayout.set(LoadingStatus.FULL)
                            salonDetails = res.salonData!!
                            obsRate = res.salonData.rate!!.toFloat()

                            obsPrices.set(getSalonPrices(res.salonData.minPrice.toString()))

                            apiResponseLiveData.value = ApiResponse.success(res)
                            categoriesAdapter.updateList(res.salonData.salonCategoriesList as ArrayList<SalonCategoryItem>)
                            notifyChange()
                        }
                        else -> {
                            apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                        }
                    }
                }
            }
        }
    }

    fun addToFav() {
        when {
            PrefMethods.getUserData() == null -> {
                setValue(Codes.NOT_LOGIN)
            }
            else -> {
                obsIsProgress.set(true)
                requestCall<AddToFavResponse?>({
                    withContext(Dispatchers.IO) { return@withContext getApiRepo().addToFav(salonDetails.id!!, 0) }
                })
                { res ->
                    obsIsProgress.set(false)
                    when (res!!.code) {
                        200 -> {
                            apiResponseLiveData.value = ApiResponse.success(res)
                        }
                        else -> {
                            apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                        }
                    }
                }
            }
        }
    }

    private fun getSalonPrices(price : String) : String {

        return getString(R.string.label_prices_start_from) + " " + price + " " + getString(R.string.label_currency)
    }

    private fun getSalonTimes(startTime : String, endTime : String) : String {

        return getString(R.string.label_working_houser_start_from) + " " + startTime + " " + getString(R.string.label_to) + " " + endTime
    }

    fun onBookOrderClicked() {
        setValue(Codes.BOOK_ORDER_CLICKED)
    }

    fun onRateClicked() {
        setValue(Codes.RATE_ORDER_CLICKED)
    }

    fun onHomeClicked() {
        serviceType = 0
        setValue(Codes.HOME_CLICKED)
    }

    fun onSalonClicked() {
        serviceType = 1
        setValue(Codes.SALON_CLICKED)
    }
}