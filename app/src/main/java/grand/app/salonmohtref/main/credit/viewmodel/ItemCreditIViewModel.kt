package grand.app.salonmohtref.main.credit.viewmodel

import androidx.databinding.ObservableField
import grand.app.salonmohtref.R
import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.main.credit.response.CreditItem

class ItemCreditIViewModel(orderPoints: Int,couponPoints: Int, var item: CreditItem) : BaseViewModel()
{
    var obsDesc= ObservableField<String>()

    init {
        when (item.type) {
            0 -> {
                obsDesc.set(" ${getString(R.string.label_added)} $orderPoints " +
                        "${getString(R.string.label_point)} ${getString(R.string.to_your_credit)} ${item.updated_at}")
            }
            1 -> {
                obsDesc.set(" ${getString(R.string.label_subtracted)} ${item.couponPoints} " +
                        "${getString(R.string.label_point)} ${getString(R.string.label_coupon)} $couponPoints ${getString(R.string.label_point)}")
            }
        }
    }
}