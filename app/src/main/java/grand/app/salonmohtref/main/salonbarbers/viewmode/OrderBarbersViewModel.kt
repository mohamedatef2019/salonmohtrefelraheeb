package grand.app.salonmohtref.main.salonbarbers.viewmode

import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.base.LoadingStatus
import grand.app.salonmohtref.main.salonbarbers.request.AllBarbersRequest
import grand.app.salonmohtref.main.salonbarbers.view.BarberAdapter
import grand.app.salonmohtref.main.confirmorder.request.OrderRequest
import grand.app.salonmohtref.main.favorites.response.AddToFavResponse
import grand.app.salonmohtref.main.salonbarbers.response.SalonBarberItem
import grand.app.salonmohtref.main.salonbarbers.response.SalonBarbersResponse
import grand.app.salonmohtref.network.ApiResponse
import grand.app.salonmohtref.utils.PrefMethods
import grand.app.salonmohtref.utils.constants.Codes
import grand.app.salonmohtref.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class OrderBarbersViewModel : BaseViewModel()
{
    var adapter = BarberAdapter()
    var orderRequest = OrderRequest()
    var request = AllBarbersRequest()

    private fun getAllBarbers()
    {
        obsLayout.set(LoadingStatus.SHIMMER)
        requestCall<SalonBarbersResponse?>({ withContext(Dispatchers.IO) { return@withContext getApiRepo().getAllBarbers(request) } })
        { res ->
            when (res?.code) {
                200 -> {
                    when (res.salonBarberData?.barbersList?.size) {
                        0 -> {
                            obsLayout.set(LoadingStatus.EMPTY)
                        }
                        else -> {
                            obsLayout.set(LoadingStatus.FULL)
                            adapter.updateList(res.salonBarberData?.barbersList)
                        }
                    }
                }
                401 -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }

    fun addToFav(barberItem : SalonBarberItem) {
        obsIsProgress.set(true)
        requestCall<AddToFavResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().addToFav(barberItem.id!!, 1) }
        })
        { res ->
            obsIsProgress.set(false)
            when (res!!.code) {
                200 -> {
                    apiResponseLiveData.value = ApiResponse.success(res)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }

    fun onPayClicked(){
        setValue(Codes.PAY_CLICKED)
    }

    fun onLoginClicked() {
        setValue(Codes.LOGIN_CLICKED)
    }

    fun getUserStatus() {
        when {
            PrefMethods.getUserData() != null -> {
                getAllBarbers()
            }
            else -> {
                obsLayout.set(LoadingStatus.NOTLOGIN)
            }
        }
    }

    init {
        getUserStatus()
    }
}