package grand.app.salonmohtref.main.mypackage.viewmodel

import androidx.databinding.ObservableField
import grand.app.salonmohtref.R
import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.main.packages.response.PackageServiceItem

class ItemPackageServiceViewModel(visitCount : Int , var item: PackageServiceItem) : BaseViewModel()
{
    var obsPeriod = ObservableField<String>()

    init {
        obsPeriod.set("$visitCount ${getString(R.string.label_package_service_period)}")
    }
}