package grand.app.salonmohtref.main.barberprofile.viewmodel

import androidx.databinding.ObservableField
import grand.app.salonmohtref.R
import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.main.barberprofile.view.BarberWorkingAdapter
import grand.app.salonmohtref.main.favorites.response.BarberWorkingItem
import grand.app.salonmohtref.main.favorites.response.FavoriteBarberItem
import grand.app.salonmohtref.utils.constants.Codes
import java.util.ArrayList

class BarberProfileViewModel : BaseViewModel()
{
    var adapter = BarberWorkingAdapter()
    var barberDetails = FavoriteBarberItem()


    var obsWorkingHours = ObservableField<String>()

    private fun getSalonTimes(startTime : String, endTime : String) : String {
        return getString(R.string.label_from) + " " + startTime + " " + getString(R.string.label_to) + " " + endTime
    }

    fun setBarberData(barber : FavoriteBarberItem){

        barberDetails = barber

        try{
            obsWorkingHours.set(getSalonTimes(barber.startTime!!, barber.endTime!!))



        when {
            barber.workings!!.isEmpty() -> {
                obsIsVisible.set(false)
            }
            else ->
            {
                obsIsVisible.set(true)
                adapter.updateList(barber.workings as ArrayList<BarberWorkingItem>)
                notifyChange()
            }
        }

        }catch (e:Exception){
            e.stackTrace
        }
    }

    fun onSalonClicked()
    {
        setValue(Codes.SALON_CLICKED)
    }
}