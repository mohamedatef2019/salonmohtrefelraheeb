package grand.app.salonmohtref.main.confirmorder.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import grand.app.salonmohtref.R
import grand.app.salonmohtref.activity.auth.AuthActivity
import grand.app.salonmohtref.activity.home.BaseHomeFragment
import grand.app.salonmohtref.activity.home.MainActivity
import grand.app.salonmohtref.databinding.FragmentConfirmOrderBinding
import grand.app.salonmohtref.dialogs.orderbooked.DialogOrderBookedFragment
import grand.app.salonmohtref.main.activitycoupons.view.CouponsActivity
import grand.app.salonmohtref.main.confirmorder.request.OrderRequest
import grand.app.salonmohtref.main.confirmorder.response.CreateOrderResponse
import grand.app.salonmohtref.main.confirmorder.viewmodel.ConfirmOrderViewModel
import grand.app.salonmohtref.main.salondetails.response.SalonDetailsData
import grand.app.salonmohtref.network.Status
import grand.app.salonmohtref.utils.PrefMethods
import grand.app.salonmohtref.utils.Utils
import grand.app.salonmohtref.utils.constants.Codes
import grand.app.salonmohtref.utils.constants.Const
import grand.app.salonmohtref.utils.constants.Params
import grand.app.salonmohtref.utils.observe
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import timber.log.Timber

class ConfirmOrderFragment : BaseHomeFragment()
{
    lateinit var binding: FragmentConfirmOrderBinding
    lateinit var viewModel: ConfirmOrderViewModel
    val fragmentArgs : ConfirmOrderFragmentArgs by navArgs()
    var orderRequest = OrderRequest()
    var salonData = SalonDetailsData()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_confirm_order, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(ConfirmOrderViewModel::class.java)
        binding.viewModel = viewModel

        orderRequest = fragmentArgs.orderRequest
        salonData = fragmentArgs.salonDetails

        showBottomBar(false)

        viewModel.orderRequest = orderRequest
        viewModel.salonData = salonData
        viewModel.setData(orderRequest)

        observe(viewModel.mutableLiveData) {
            when (it) {
                Codes.APPLY_COUPON_CLICKED -> {
                    val intent = Intent(requireActivity(), CouponsActivity::class.java)
                    startActivityForResult(intent, Codes.ALL_COUPONS_REQUEST)
                }
            }
        }

        observe(viewModel.adapter.removeLiveData) {
            viewModel.orderRequest.servicesList.remove(it)
            viewModel.orderRequest.sub_category_id.remove(it?.id as Int)
            when (viewModel.orderRequest.servicesList.size) {
                0 -> {
                    showToast(getString(R.string.please_confirm_your_order_again), 1)
                    lifecycleScope.launch {
                        var flag : Int = -1
                        flag = when (viewModel.orderRequest.city_id) {
                            null -> {
                                0
                            }
                            else -> {
                                1
                            }
                        }
                        delay(2000)
                        findNavController().navigate(ConfirmOrderFragmentDirections.confirmOrderToSalonDetails(fragmentArgs.salonDetails.id!!, flag))
                    }
                }
                else -> {
                    viewModel.total = viewModel.total - it?.price!!
                    viewModel.orderRequest.total_price = viewModel.total
                    viewModel.obsTotalPrice.set("${viewModel.total} ${getString(R.string.label_currency)}")
                    viewModel.orderRequest.servicesList.remove(it)
                    viewModel.adapter.updateList(viewModel.orderRequest.servicesList)
                }
            }
        }


        observe(viewModel.paymentAdapter.itemLiveData){
            when (viewModel.paymentAdapter.selectedPosition) {
                -1 -> {
                    viewModel.orderRequest.payment_method = null
                    binding.btnPay.visibility = View.GONE
                }
                else -> {
                    when (it?.id) {
                        2 -> {
                            when{
                                viewModel.packageId != -1 -> {
                                    viewModel.orderRequest.payment_method = it.id
                                    binding.btnPay.visibility = View.VISIBLE
                                }
                                else ->{
                                    viewModel.orderRequest.payment_method = null
                                    binding.btnPay.visibility = View.GONE
                                }
                            }
                        }
                        else -> {
                            viewModel.orderRequest.payment_method = it?.id
                            binding.btnPay.visibility = View.VISIBLE
                        }
                    }
                }
            }
        }

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is CreateOrderResponse -> {
                            val bundle = Bundle()
                            bundle.putString(Params.ORDER_MSG, getString(R.string.msg_order_booked))
                            Utils.startDialogActivity(requireActivity(), DialogOrderBookedFragment::class.java.name, Codes.DIALOG_ORDER_SUCCESS, bundle)
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when(requestCode){
            Codes.DIALOG_LOGIN_REQUEST -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        Const.isAskedToLogin = 1
                                        requireActivity().startActivity(Intent(requireActivity(), AuthActivity::class.java)
                                                .putExtra(Const.ACCESS_LOGIN , true))
                                    }
                                }
                            }
                        }
                    }
                }
            }

            Codes.DIALOG_ORDER_SUCCESS -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        requireActivity().startActivity(Intent(requireActivity(), MainActivity::class.java))
                                        requireActivity().finishAffinity()
                                    }
                                }
                            }
                        }
                    }
                }
            }
            Codes.ALL_COUPONS_REQUEST -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        Timber.e("coupon_back")
                                        binding.couponLayout.setBackgroundResource(R.drawable.unselected_coupon_bg)
                                        viewModel.orderRequest.free_sub_category_id
                                        viewModel.orderRequest.free_sub_category_id
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}