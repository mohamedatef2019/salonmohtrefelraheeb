package grand.app.salonmohtref.main.package_details.response

import com.google.gson.annotations.SerializedName

data class UpgradePackageResponse(

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("status")
	val status: String? = null
)
