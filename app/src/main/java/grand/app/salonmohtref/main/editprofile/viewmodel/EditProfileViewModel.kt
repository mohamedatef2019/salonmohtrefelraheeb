package grand.app.salonmohtref.main.editprofile.viewmodel

import androidx.databinding.ObservableField
import grand.app.salonmohtref.R
import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.base.LoadingStatus
import grand.app.salonmohtref.location.util.AddressItem
import grand.app.salonmohtref.main.editprofile.request.EditProfileRequest
import grand.app.salonmohtref.main.editprofile.response.UpdateProfileResponse
import grand.app.salonmohtref.network.ApiResponse
import grand.app.salonmohtref.utils.PrefMethods
import grand.app.salonmohtref.utils.constants.Codes
import grand.app.salonmohtref.utils.requestCall
import grand.app.salonmohtref.utils.stringPathToFile
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class EditProfileViewModel : BaseViewModel()
{
    var request = EditProfileRequest()
    var obsUserImg = ObservableField<Any>()
    var obsShowEditBtn = ObservableField<Boolean>()

    fun onUpdateClicked() {
        when {
            request.name == null || request.phone == null|| request.email == null || request.address == null -> {
                apiResponseLiveData.value = ApiResponse.errorMessage(getString(R.string.msg_invalide_data))
            }
            request.name.isNullOrEmpty()  || request.phone.isNullOrEmpty() || request.email.isNullOrEmpty() || request.address.isNullOrEmpty() -> {
                apiResponseLiveData.value = ApiResponse.errorMessage(getString(R.string.msg_invalide_data))
            }
            else -> {
                setClickable()
                updateProfile()
            }
        }
    }

    fun onChooseImgClicked() {
        setClickable()
        setValue(Codes.CHOOSE_IMAGE_CLICKED)
    }

    private fun updateProfile() {

        obsIsProgress.set(true)
        requestCall<UpdateProfileResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().updateProfile(request) } })
        { res ->
            obsIsProgress.set(false)
            when (res!!.code) {
                200 -> {
                    PrefMethods.saveUserData(res.userData)
                    PrefMethods.saveUserLocation(AddressItem(request.lat!!, request.lng!!,request.address))
                    apiResponseLiveData.value = ApiResponse.successMessage(res.msg)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }

    fun onLocationClicked() {
        setClickable()
        setValue(Codes.LOCATION_CLICKED)
    }

    fun gotImage(requestCode: Int, path: String) {
        when (requestCode) {
            Codes.USER_PROFILE_IMAGE_REQUEST -> {
                request.userImg = path.stringPathToFile()
                notifyChange()
            }
        }
    }

    fun onLoginClicked() {
        setValue(Codes.LOGIN_CLICKED)
    }

    fun getUserStatus() {
        when {
            PrefMethods.getUserData() != null -> {
                obsLayout.set(LoadingStatus.FULL)
                request.name = PrefMethods.getUserData()!!.name.toString()
                request.email = PrefMethods.getUserData()!!.email.toString()
                request.phone = PrefMethods.getUserData()!!.phone.toString()
                request.address = PrefMethods.getUserLocation()!!.address.toString()
                request.lat = PrefMethods.getUserLocation()!!.lat
                request.lng = PrefMethods.getUserLocation()!!.lng
                obsUserImg.set(PrefMethods.getUserData()?.img.toString())
                obsShowEditBtn.set(true)
            }
            else -> {
                obsLayout.set(LoadingStatus.NOTLOGIN)
                obsShowEditBtn.set(false)
            }
        }
    }

    init {
        getUserStatus()
    }
}