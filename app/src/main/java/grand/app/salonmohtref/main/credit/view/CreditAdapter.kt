package grand.app.salonmohtref.main.credit.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.salonmohtref.R
import grand.app.salonmohtref.databinding.ItemCreditBinding
import grand.app.salonmohtref.main.credit.response.CreditItem
import grand.app.salonmohtref.main.credit.viewmodel.ItemCreditIViewModel
import grand.app.salonmohtref.utils.SingleLiveEvent
import java.util.*

class CreditAdapter : RecyclerView.Adapter<CreditAdapter.CreditHolder>()
{
    var itemsList: ArrayList<CreditItem> = ArrayList()
    var itemLiveData = SingleLiveEvent<CreditItem>()
    var orderPoints : Int = 0
    var couponPoints : Int = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CreditHolder
    {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemCreditBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_credit, parent, false)
        return CreditHolder(binding)
    }

    override fun onBindViewHolder(holder: CreditHolder, position: Int) {
        val itemViewModel = ItemCreditIViewModel(orderPoints, couponPoints, itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.binding.rawLayout.setOnClickListener {
            itemLiveData.value = itemViewModel.item
        }
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<CreditItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class CreditHolder(val binding: ItemCreditBinding) : RecyclerView.ViewHolder(binding.root)
}
