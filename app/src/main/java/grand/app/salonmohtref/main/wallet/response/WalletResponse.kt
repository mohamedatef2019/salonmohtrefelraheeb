package grand.app.salonmohtref.main.wallet.response

import com.google.gson.annotations.SerializedName

data class WalletResponse(

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val walletData: WalletData? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class WalletHistoryItem(

	@field:SerializedName("user_id")
	val userId: Int? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("credit")
	val credit: Int? = null,

	@field:SerializedName("type")
	val type: Int? = null
)

data class WalletData(

	@field:SerializedName("wallet")
	val wallet: Int? = null,

	@field:SerializedName("history")
	val historyList: List<WalletHistoryItem?>? = null
)
