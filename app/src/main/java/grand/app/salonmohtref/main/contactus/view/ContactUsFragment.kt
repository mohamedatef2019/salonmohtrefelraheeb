package grand.app.salonmohtref.main.contactus.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import grand.app.salonmohtref.R
import grand.app.salonmohtref.activity.home.BaseHomeFragment
import grand.app.salonmohtref.activity.home.MainActivity
import grand.app.salonmohtref.databinding.FragmentContactUsBinding
import grand.app.salonmohtref.main.contactus.response.ContactUsResponse
import grand.app.salonmohtref.main.contactus.viewmodel.ContactUsViewModel
import grand.app.salonmohtref.network.Status
import grand.app.salonmohtref.utils.constants.Codes
import grand.app.salonmohtref.utils.observe
import timber.log.Timber

class ContactUsFragment : BaseHomeFragment(), androidx.lifecycle.Observer<Any?>
{
    lateinit var binding: FragmentContactUsBinding
    lateinit var vieWModel: ContactUsViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_contact_us, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        vieWModel = ViewModelProvider(this).get(ContactUsViewModel::class.java)
        binding.viewModel = vieWModel

        vieWModel.mutableLiveData.observe(viewLifecycleOwner, this)

        showBottomBar(false)

        observe(vieWModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is ContactUsResponse -> {
                            showToast(it.data.msg.toString(), 2)
                            findNavController().navigateUp()
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }
    }

    override fun onChanged(it: Any?) {

        if(it == null) return
        when (it) {
            Codes.EMPTY_NAME -> {
                showToast(getString(R.string.msg_empty_name), 1)
            }
            Codes.EMAIL_EMPTY -> {
                showToast(getString(R.string.msg_empty_email), 1)
            }
            Codes.INVALID_EMAIL -> {
                showToast(getString(R.string.msg_invalid_email), 1)
            }
            Codes.EMPTY_PHONE -> {
                showToast(getString(R.string.msg_empty_phone), 1)
            }
            Codes.EMPTY_MESSAGE -> {
                showToast(getString(R.string.msg_empty_message), 1)
            }
        }
    }
}