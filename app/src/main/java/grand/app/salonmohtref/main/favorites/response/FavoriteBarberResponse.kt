package grand.app.salonmohtref.main.favorites.response

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class FavoriteBarberResponse(

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val favoriteData: BarberFavoriteData? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class FavoriteBarberItem(

		@field:SerializedName("img")
		var img: String? = null,

		@field:SerializedName("salon_id")
		var salonId: Int? = null,

		@field:SerializedName("tue")
		var tue: Int? = null,

		@field:SerializedName("sat")
		var sat: Int? = null,

		@field:SerializedName("rates_count")
		var ratesCount: Int? = null,

		@field:SerializedName("end_time")
		var endTime: String? = null,

		@field:SerializedName("salon_name")
		var salonName: String? = null,

		@field:SerializedName("sun")
		val sun: Int? = null,

		@field:SerializedName("mon")
		val mon: Int? = null,

		@field:SerializedName("branch")
		val branch: String? = null,

		@field:SerializedName("start_time")
        var startTime: String? = null,

		@field:SerializedName("rate")
		val rate: String? = null,

		@field:SerializedName("working_hours")
		val workingHours: String? = null,

		@field:SerializedName("wed")
		val wed: Int? = null,

		@field:SerializedName("name")
		var name: String? = null,

		@field:SerializedName("id")
		var id: Int? = null,

		@field:SerializedName("fri")
		val fri: Int? = null,

		@field:SerializedName("thr")
		val thr: Int? = null,

		@field:SerializedName("workings")
		val workings: List<BarberWorkingItem?>? = null
) : Parcelable {
	constructor(parcel: Parcel) : this(
			parcel.readString(),
			parcel.readValue(Int::class.java.classLoader) as? Int,
			parcel.readValue(Int::class.java.classLoader) as? Int,
			parcel.readValue(Int::class.java.classLoader) as? Int,
			parcel.readValue(Int::class.java.classLoader) as? Int,
			parcel.readString(),
			parcel.readString(),
			parcel.readValue(Int::class.java.classLoader) as? Int,
			parcel.readValue(Int::class.java.classLoader) as? Int,
			parcel.readString(),
			parcel.readString(),
			parcel.readString(),
			parcel.readString(),
			parcel.readValue(Int::class.java.classLoader) as? Int,
			parcel.readString(),
			parcel.readValue(Int::class.java.classLoader) as? Int,
			parcel.readValue(Int::class.java.classLoader) as? Int,
			parcel.readValue(Int::class.java.classLoader) as? Int,
			TODO("workings")) {
	}

	override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeString(img)
		parcel.writeValue(salonId)
		parcel.writeValue(tue)
		parcel.writeValue(sat)
		parcel.writeValue(ratesCount)
		parcel.writeString(endTime)
		parcel.writeString(salonName)
		parcel.writeValue(sun)
		parcel.writeValue(mon)
		parcel.writeString(branch)
		parcel.writeString(startTime)
		parcel.writeString(rate)
		parcel.writeString(workingHours)
		parcel.writeValue(wed)
		parcel.writeString(name)
		parcel.writeValue(id)
		parcel.writeValue(fri)
		parcel.writeValue(thr)
	}

	override fun describeContents(): Int {
		return 0
	}

	companion object CREATOR : Parcelable.Creator<FavoriteBarberItem> {
		override fun createFromParcel(parcel: Parcel): FavoriteBarberItem {
			return FavoriteBarberItem(parcel)
		}

		override fun newArray(size: Int): Array<FavoriteBarberItem?> {
			return arrayOfNulls(size)
		}
	}
}

data class BarberWorkingItem(

	@field:SerializedName("img")
	val img: String? = null,

	@field:SerializedName("barber_id")
	val barberId: Int? = null,

	@field:SerializedName("id")
	val id: Int? = null
)

data class BarberFavoriteData(

	@field:SerializedName("first_page_url")
	val firstPageUrl: String? = null,

	@field:SerializedName("path")
	val path: String? = null,

	@field:SerializedName("per_page")
	val perPage: Int? = null,

	@field:SerializedName("total")
	val total: Int? = null,

	@field:SerializedName("data")
	val barbersList: List<FavoriteBarberItem?>? = null,

	@field:SerializedName("last_page")
	val lastPage: Int? = null,

	@field:SerializedName("last_page_url")
	val lastPageUrl: String? = null,

	@field:SerializedName("next_page_url")
	val nextPageUrl: Any? = null,

	@field:SerializedName("from")
	val from: Int? = null,

	@field:SerializedName("to")
	val to: Int? = null,

	@field:SerializedName("prev_page_url")
	val prevPageUrl: Any? = null,

	@field:SerializedName("current_page")
	val currentPage: Int? = null
)
