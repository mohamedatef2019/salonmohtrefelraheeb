package grand.app.salonmohtref.main.products.viewmodel

import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.base.LoadingStatus
import grand.app.salonmohtref.main.bottombar.products.response.ProductItem
import grand.app.salonmohtref.main.products.view.ProductsAdapter
import grand.app.salonmohtref.network.ApiResponse
import grand.app.salonmohtref.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.ArrayList

class ProductsViewModel : BaseViewModel() {

    var adapter = ProductsAdapter()
    var productsList = ArrayList<ProductItem>()

    fun getProducts() {
        obsLayout.set(LoadingStatus.SHIMMER)
        requestCall({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().getProducts() } })
        { res ->
            when (res!!.code) {
                200 -> {
                    when (res.productsList!!.size) {
                        0 -> {
                            obsLayout.set(LoadingStatus.EMPTY)
                        }
                        else -> {
                            obsLayout.set(LoadingStatus.FULL)
                            apiResponseLiveData.value = ApiResponse.success(res)
                            productsList = res.productsList as ArrayList<ProductItem>
                            adapter.updateList(productsList)
                        }
                    }
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }

    init {
        getProducts()
    }
}