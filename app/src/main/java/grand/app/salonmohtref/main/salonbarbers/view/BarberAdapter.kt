package grand.app.salonmohtref.main.salonbarbers.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import grand.app.salonmohtref.R
import grand.app.salonmohtref.base.BaseApp
import grand.app.salonmohtref.databinding.ItemSalonBarberBinding
import grand.app.salonmohtref.main.salonbarbers.response.SalonBarberItem
import grand.app.salonmohtref.main.salonbarbers.viewmode.ItemBarberViewModel
import grand.app.salonmohtref.utils.SingleLiveEvent
import java.util.*

class BarberAdapter : RecyclerView.Adapter<BarberAdapter.BarberHolder>()
{
    var itemsList: List<SalonBarberItem?> ?= mutableListOf()
    var itemLiveData  = SingleLiveEvent<SalonBarberItem>()
    var favLiveData  = SingleLiveEvent<SalonBarberItem>()
    var selectedPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BarberHolder
    {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemSalonBarberBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_salon_barber, parent, false)
        return BarberHolder(binding)
    }

    override fun onBindViewHolder(holder: BarberHolder, position: Int) {
        val itemViewModel = ItemBarberViewModel(itemsList?.get(position))
        holder.binding.viewModel = itemViewModel

        holder.setSelected()
        holder.setFav()

        holder.binding.ibFav.setOnClickListener {
            favLiveData.value = itemViewModel.item!!
        }

        holder.binding.rawLayout.setOnClickListener {
            when {
                itemViewModel.item?.status != 0 -> {
                    when {
                        position != selectedPosition -> {
                            notifyItemChanged(position)
                            notifyItemChanged(selectedPosition)
                            selectedPosition = position
                        }
                        selectedPosition == position -> {
                            selectedPosition = -1
                            notifyItemChanged(position)
                        }
                    }
                    itemLiveData.value = itemViewModel.item!!
                }
            }
        }
    }

    fun getItem(pos:Int): SalonBarberItem? {
        return itemsList?.get(pos)
    }

    override fun getItemCount(): Int {
        return itemsList?.size?:0
    }

    fun notifyItemSelected(item : SalonBarberItem?)
    {
        item?.let {
            itemsList?.let { list ->
                getItem(list.indexOf(item))?.isFavorite = item.isFavorite
                notifyItemChanged(list.indexOf(item))
            }
        }
    }

    fun updateList(models: List<SalonBarberItem?>?) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class BarberHolder(val binding: ItemSalonBarberBinding) : RecyclerView.ViewHolder(binding.root)
    {
        fun setSelected()
        {
            when (selectedPosition) {
                adapterPosition -> {
                    binding.rawLayout.background = ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.barber_selected_bg)
                    binding.tvBarberName.setTextColor(ContextCompat.getColor(BaseApp.getInstance, R.color.white))
                    binding.tvBarberAddress.setTextColor(ContextCompat.getColor(BaseApp.getInstance, R.color.white))
                    binding.tvBarberWorkingHours.setTextColor(ContextCompat.getColor(BaseApp.getInstance, R.color.white))
                    binding.tvStatus.background = ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.barber_not_selected_bg)
                    binding.tvStatus.setTextColor(ContextCompat.getColor(BaseApp.getInstance, R.color.color_primary))
                }
                else -> {
                    binding.rawLayout.background = ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.barber_not_selected_bg)
                    binding.tvBarberName.setTextColor(ContextCompat.getColor(BaseApp.getInstance, R.color.color_primary))
                    binding.tvBarberAddress.setTextColor(ContextCompat.getColor(BaseApp.getInstance, R.color.color_primary))
                    binding.tvBarberWorkingHours.setTextColor(ContextCompat.getColor(BaseApp.getInstance, R.color.color_primary))

                    when (getItem(adapterPosition)?.status) {
                        0 -> {
                            binding.tvStatus.background = ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.btn_not_available)
                            binding.tvStatus.text =  BaseApp.getInstance.getString(R.string.label_not_available)
                        }
                        1 -> {
                            binding.tvStatus.background = ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.barber_selected_bg)
                        }
                    }
                    binding.tvStatus.setTextColor(ContextCompat.getColor(BaseApp.getInstance, R.color.white))
                }
            }
        }

        fun setFav(){
            when (getItem(adapterPosition)?.isFavorite) {
                1 -> {
                    Glide.with(BaseApp.getInstance).load(R.drawable.ic_fav).into(binding.ibFav)
                }
                else -> {
                    Glide.with(BaseApp.getInstance).load(R.drawable.ic_un_fav).into(binding.ibFav)
                }
            }
        }
    }
}
