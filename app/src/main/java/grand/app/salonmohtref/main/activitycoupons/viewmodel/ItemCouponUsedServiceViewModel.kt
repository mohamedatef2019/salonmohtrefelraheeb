package grand.app.salonmohtref.main.activitycoupons.viewmodel

import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.main.coupons.response.UserServicesItem

class ItemCouponUsedServiceViewModel(var item: UserServicesItem) : BaseViewModel()
