package grand.app.salonmohtref.main.mypackage.response

import com.google.gson.annotations.SerializedName
import grand.app.salonmohtref.main.packages.response.PackageServiceItem

data class MyPackageResponse(

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val packageDetails: PackageDetailsData? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class PackageDetailsData(

	@field:SerializedName("period")
	val period: Int? = null,

	@field:SerializedName("next_package_price")
	val nextPackagePrice: Int? = null,

	@field:SerializedName("services")
	val servicesList: List<PackageServiceItem?>? = null,

	@field:SerializedName("free_months")
	val freeMonths: Int? = null,

	@field:SerializedName("user_visits")
	val userVisits: Int? = null,

	@field:SerializedName("user_months")
	val userMonths: Int? = null,

	@field:SerializedName("next_package_name")
	val nextPackageName: String? = null,

	@field:SerializedName("cover")
	val cover: String? = null,

	@field:SerializedName("next_package_id")
	val nextPackageId: Int? = null,

	@field:SerializedName("badge")
	val badge: String? = null,

	@field:SerializedName("visits")
	val visits: Int? = null,

	@field:SerializedName("price")
	val price: Int? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null
)