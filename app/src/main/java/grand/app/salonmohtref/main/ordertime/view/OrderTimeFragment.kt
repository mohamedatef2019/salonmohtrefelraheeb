package grand.app.salonmohtref.main.ordertime.view

import CalendarUtil
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.skyhope.eventcalenderlibrary.listener.CalenderDayClickListener
import es.dmoral.toasty.Toasty
import grand.app.salonmohtref.R
import grand.app.salonmohtref.activity.home.BaseHomeFragment
import grand.app.salonmohtref.databinding.FragmentOrderTimeBinding
import grand.app.salonmohtref.main.ordertime.time.ItemSalonTime
import grand.app.salonmohtref.main.ordertime.viewmodel.OrderTimeViewModel
import grand.app.salonmohtref.main.salondetails.response.TakenTimesItem
import grand.app.salonmohtref.utils.constants.Codes
import grand.app.salonmohtref.utils.formatDate
import grand.app.salonmohtref.utils.observe
import java.text.ParseException
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

class OrderTimeFragment : BaseHomeFragment() {
    lateinit var binding: FragmentOrderTimeBinding
    lateinit var viewModel: OrderTimeViewModel
    val fragmentArgs : OrderTimeFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentOrderTimeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(this).get(OrderTimeViewModel::class.java)
        binding.viewModel = viewModel

        viewModel.salonDetails = fragmentArgs.salonDetails
        viewModel.orderRequest = fragmentArgs.orderRequest

        showBottomBar(false)

        binding.calenderEvent.initCalderItemClickCallback(CalenderDayClickListener { dayContainerModel ->
            val itemDate = formatDate(dayContainerModel.date, "dd MMM yyyy", "dd-MM-yyyy", "en")
            viewModel.orderRequest.date = itemDate
            setTimesList(itemDate)

            val curFormater = SimpleDateFormat("dd-MM-yyyy")
            val dateObj = curFormater.parse(itemDate)
            when {
                dateObj.before(Calendar.getInstance().time) -> {
                    showToast(getString(R.string.label_invalide_date), 1)
                }
            }
        })

        val sdf = SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH)
        val currentDate = sdf.format(Date())
        setTimesList(currentDate)
        viewModel.obsDay.set(currentDate)
        viewModel.orderRequest.date = currentDate

        observe(viewModel.timesAdapter.itemLiveData) {
            viewModel.orderRequest.time = it!!.time
            viewModel.obstime.set(it.time)
        }

        observe(viewModel.mutableLiveData) {
            when (it) {
                Codes.EMPTY_DATE -> {
                    Toasty.warning(requireActivity(), getString(R.string.msg_select_date)).show()
                }
                Codes.EMPTY_TIME -> {
                    Toasty.warning(requireActivity(), getString(R.string.msg_select_time)).show()
                }
                Codes.TIME_SELECTED -> {
                    findNavController().navigate(OrderTimeFragmentDirections.orderTimeToSalonBarbers(fragmentArgs.salonDetails, viewModel.orderRequest))
                }
            }
        }
    }

    private fun setTimesList(itemDate: String) {

        val startTime = fragmentArgs.salonDetails.startTime
        val endTime = fragmentArgs.salonDetails.endTime
        val cTimesList = CalendarUtil.getTimes(30, startTime, endTime)

        // booked times
        val takenList = fragmentArgs.salonDetails.takenTimes
        val newList = arrayListOf<ItemSalonTime>()
        val salonTimesList = arrayListOf<String>()

        cTimesList.forEach { itemtime ->
            var date = Date()
            val format = SimpleDateFormat("HH:mm")
            try {
                date = format.parse(itemtime)
            } catch (e: ParseException) {
                e.printStackTrace()
            }

            val timeFormat = SimpleDateFormat("HH:mm", Locale("en"))
            salonTimesList.add(timeFormat.format(date).toString())
        }

        salonTimesList.forEach { itemtime ->
            newList.add(ItemSalonTime(itemtime, found(takenList, itemtime, itemDate)))
        }

        viewModel.timesAdapter.updateList(newList)
    }

    fun found(takenList: List<TakenTimesItem?>?, itemTime: String, currentDate: String) : Boolean{
        takenList.let {
            it?.forEach {

                val timeFormat = SimpleDateFormat("HH:mm", Locale("en"))
                var date = Date()
                val format = SimpleDateFormat("HH:mm")
                try {
                    date = format.parse(it?.time)
                } catch (e: ParseException) {
                    e.printStackTrace()
                }

                val temp = timeFormat.format(date).toString()

                when {
                    temp == itemTime && currentDate == it?.date -> {
                        return false
                    }
                }
            }
        }
        return true
    }

    @RequiresApi(Build.VERSION_CODES.O)
    @Throws(ParseException::class)
    fun checkProductValidationDate(selectedDate: String, todayDate: String): Boolean {

        val dtf: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss")
        val now: LocalDateTime = LocalDateTime.now()
        var today  = dtf.format(now)

        var result = false
        val sdformat = SimpleDateFormat("yyyy-MM-dd")
        val d1 = sdformat.parse(selectedDate)
        val d2 = sdformat.parse(todayDate)
        if (d1.compareTo(d2) > 0) {
            showToast("Date 1 occurs after Date 2", 1)
            result = false
        } else if (d1.compareTo(d2) < 0) {
            println("Date 1 occurs before Date 2")
            showToast("Date 1 occurs before Date 2", 1)
            result = true
        } else if (d1.compareTo(d2) == 0) {
            println("Date 1 occurs before Date 2")
            result = false
        }
        return result
    }
}