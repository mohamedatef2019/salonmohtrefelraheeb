package grand.app.salonmohtref.main.cancelapolicy.viewmodel

import androidx.databinding.ObservableField
import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.base.LoadingStatus
import grand.app.salonmohtref.main.cancelapolicy.response.CancellationPolicyItem
import grand.app.salonmohtref.main.cancelapolicy.response.PrivacyPolicyResponse
import grand.app.salonmohtref.main.cancelapolicy.view.PolicyAdapter
import grand.app.salonmohtref.network.ApiResponse
import grand.app.salonmohtref.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.ArrayList

class PolicyViewModel : BaseViewModel()
{
    var adapter = PolicyAdapter()
    var policyImg = ObservableField<String>()

    init {
        getAboutData()
    }

    fun getAboutData() {
        obsLayout.set(LoadingStatus.SHIMMER)
        requestCall<PrivacyPolicyResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().getPrivacyPolicy() }
        })
        { res ->
            when (res!!.code) {
                200 -> {
                    obsLayout.set(LoadingStatus.FULL)
                    adapter.updateList(res.privacyData?.cancellationPolicyList as ArrayList<CancellationPolicyItem>)
                    policyImg.set(res.privacyData.img)
                    notifyChange()
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg.toString())
                }
            }
        }
    }
}