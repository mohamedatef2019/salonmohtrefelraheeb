package grand.app.salonmohtref.main.salonbarbers.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import grand.app.salonmohtref.R
import grand.app.salonmohtref.activity.auth.AuthActivity
import grand.app.salonmohtref.activity.home.BaseHomeFragment
import grand.app.salonmohtref.databinding.FragmentBarbersBinding
import grand.app.salonmohtref.main.salonbarbers.viewmode.OrderBarbersViewModel
import grand.app.salonmohtref.main.favorites.response.AddToFavResponse
import grand.app.salonmohtref.main.salonbarbers.request.AllBarbersRequest
import grand.app.salonmohtref.main.salonbarbers.response.SalonBarberItem
import grand.app.salonmohtref.network.Status
import grand.app.salonmohtref.utils.PrefMethods
import grand.app.salonmohtref.utils.constants.Codes
import grand.app.salonmohtref.utils.constants.Const
import grand.app.salonmohtref.utils.observe
import timber.log.Timber

class BarbersFragment : BaseHomeFragment()
{
    lateinit var binding: FragmentBarbersBinding
    lateinit var viewModel: OrderBarbersViewModel
    val fragmentArgs : BarbersFragmentArgs by navArgs()
    var favItem : SalonBarberItem = SalonBarberItem()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_barbers, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(OrderBarbersViewModel::class.java)
        binding.viewModel = viewModel

        viewModel.orderRequest = fragmentArgs.orderRequest

        viewModel.adapter.selectedPosition = -1
        binding.btnPay.visibility = View.GONE

        viewModel.request = AllBarbersRequest(salon_id = fragmentArgs.salonDetails.id!!,
                date = fragmentArgs.orderRequest.date,
                time = fragmentArgs.orderRequest.time)
        viewModel.getUserStatus()

        showBottomBar(false)

        observe(viewModel.mutableLiveData){
            when (it) {
                Codes.PAY_CLICKED -> {
                    findNavController().navigate(BarbersFragmentDirections.salonBarbersToConfirmOrder(fragmentArgs.salonDetails, viewModel.orderRequest))
                }
                Codes.LOGIN_CLICKED -> {
                    Const.isAskedToLogin = 1
                    requireActivity().startActivity(Intent(requireActivity(), AuthActivity::class.java).putExtra(Const.ACCESS_LOGIN , true))
                }
            }
        }

        observe(viewModel.adapter.itemLiveData){
            when (viewModel.adapter.selectedPosition) {
                -1 -> {
                    binding.btnPay.visibility = View.GONE
                }
                else -> {
                    viewModel.orderRequest.barber_id = it?.id
                    binding.btnPay.visibility = View.VISIBLE
                }
            }
        }

        observe(viewModel.adapter.favLiveData){
            favItem = it!!
            viewModel.addToFav(it)
        }

        showBottomBar(false)

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is AddToFavResponse -> {
                            showToast(it.data.msg.toString(), 2)
                            favItem.isFavorite = it.data.addFavData?.isFavorite
                            viewModel.adapter.notifyItemSelected(favItem)
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        when (Const.isAskedToLogin) {
            1 -> {
                viewModel.getUserStatus()
                Const.isAskedToLogin = 0
            }
        }
    }
}