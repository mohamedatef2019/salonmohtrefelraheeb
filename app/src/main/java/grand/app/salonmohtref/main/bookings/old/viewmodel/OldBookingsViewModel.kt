package grand.app.salonmohtref.main.bookings.old.viewmodel

import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.base.LoadingStatus
import grand.app.salonmohtref.main.bookings.adapter.BookingsAdapter
import grand.app.salonmohtref.main.bookings.response.CancelOrderResponse
import grand.app.salonmohtref.main.bookings.response.MyBookingItem
import grand.app.salonmohtref.main.bookings.response.MyBookingsResponse
import grand.app.salonmohtref.network.ApiResponse
import grand.app.salonmohtref.utils.PrefMethods
import grand.app.salonmohtref.utils.constants.Codes
import grand.app.salonmohtref.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.ArrayList

class OldBookingsViewModel : BaseViewModel()
{
    var adapter = BookingsAdapter()
    var pageNumber = 0

    fun getOldBookings()
    {
        obsLayout.set(LoadingStatus.SHIMMER)
        requestCall<MyBookingsResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().getMyBookings(2, pageNumber) }
        })
        { res ->
            when (res?.code) {
                200 -> {
                    when {
                        res.myBookingsData?.myBookingsList.isNullOrEmpty() || res.myBookingsData?.myBookingsList!!.isEmpty() -> {
                            obsLayout.set(LoadingStatus.EMPTY)
                        }
                        else -> {
                            obsLayout.set(LoadingStatus.FULL)
                            adapter.updateList(res.myBookingsData.myBookingsList as ArrayList<MyBookingItem>)
                        }
                    }
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res?.msg)
                }
            }
        }
    }

    fun cancelOrder(item : MyBookingItem)
    {
        obsIsProgress.set(true)
        requestCall<CancelOrderResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().cancelOrder(item.id!!.toInt()) }
        })
        { res ->
            obsIsProgress.set(false)
            when (res!!.code) {
                200 -> {
                    getOldBookings()
                    apiResponseLiveData.value = ApiResponse.success(res)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }

    fun onLoginClicked() {
        setValue(Codes.LOGIN_CLICKED)
    }

    fun getUserStatus() {
        when {
            PrefMethods.getUserData() != null -> {
                getOldBookings()
            }
            else -> {
                obsLayout.set(LoadingStatus.NOTLOGIN)
            }
        }
    }

    init {
        getUserStatus()
    }
}