package grand.app.salonmohtref.main.package_details.viewmodel

import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.main.confirmorder.request.OrderPaymentItem
import grand.app.salonmohtref.main.package_details.view.PackagePaymentItem

class ItemPackagePaymentViewModel(var item: PackagePaymentItem) : BaseViewModel()
