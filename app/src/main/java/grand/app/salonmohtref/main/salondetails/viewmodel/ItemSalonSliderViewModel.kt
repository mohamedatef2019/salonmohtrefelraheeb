package grand.app.salonmohtref.main.salondetails.viewmodel

import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.main.salondetails.response.MediaItem

class ItemSalonSliderViewModel(var item: MediaItem) : BaseViewModel()