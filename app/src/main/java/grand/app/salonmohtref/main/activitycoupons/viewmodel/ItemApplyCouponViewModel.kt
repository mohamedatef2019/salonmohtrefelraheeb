package grand.app.salonmohtref.main.activitycoupons.viewmodel

import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.main.coupons.response.CouponServiceItem

class ItemApplyCouponViewModel(var item: CouponServiceItem) : BaseViewModel()