package grand.app.salonmohtref.main.packages.viewmodel

import androidx.databinding.ObservableField
import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.base.LoadingStatus
import grand.app.salonmohtref.main.packages.response.PackageItem
import grand.app.salonmohtref.main.packages.response.PackagesResponse
import grand.app.salonmohtref.main.packages.view.PackagesAdapter
import grand.app.salonmohtref.network.ApiResponse
import grand.app.salonmohtref.utils.PrefMethods
import grand.app.salonmohtref.utils.constants.Codes
import grand.app.salonmohtref.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.ArrayList

class PackagesViewModel : BaseViewModel()
{
    var obsCredit = ObservableField<String>()
    var adapter = PackagesAdapter()

    fun getPackages()
    {
        obsLayout.set(LoadingStatus.SHIMMER)
        requestCall<PackagesResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().getAllPackages() }
        })
        { res ->
            apiResponseLiveData.value = ApiResponse.shimmerLoading(false)
            when (res!!.code) {
                200 -> {
                    when (res.packagesList?.size) {
                        0 -> {
                            obsIsVisible.set(true)
                        }
                        else -> {
                            obsLayout.set(LoadingStatus.FULL)

                            res.packagesList?.let { list ->
                                adapter.updateList(list as ArrayList<PackageItem>)
                            }
                        }
                    }
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }

    fun onLoginClicked() {
        setValue(Codes.LOGIN_CLICKED)
    }


    fun getUserStatus() {
        when {
            PrefMethods.getUserData() != null -> {
                getPackages()
            }
            else -> {
                obsIsVisible.set(false)
                obsLayout.set(LoadingStatus.NOTLOGIN)
            }
        }
    }

    init {
        getUserStatus()
    }
}
