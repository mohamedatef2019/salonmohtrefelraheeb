package grand.app.salonmohtref.main.coupons.viewmodel

import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.base.LoadingStatus
import grand.app.salonmohtref.main.coupons.response.CouponResponse
import grand.app.salonmohtref.main.coupons.view.CouponAdapter
import grand.app.salonmohtref.network.ApiResponse
import grand.app.salonmohtref.utils.PrefMethods
import grand.app.salonmohtref.utils.constants.Codes
import grand.app.salonmohtref.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class CouponsViewModel : BaseViewModel()
{
    var adapter = CouponAdapter()
    var isFirstTime : Boolean = true

    fun getCoupons()
    {
        if (isFirstTime)
        {
            obsLayout.set(LoadingStatus.SHIMMER)
            requestCall<CouponResponse?>({
                withContext(Dispatchers.IO) { return@withContext getApiRepo().getAllCoupons() }
            })
            { res ->
                apiResponseLiveData.value = ApiResponse.shimmerLoading(false)
                when (res!!.code) {
                    200 -> {
                        when (res.couponsList?.size) {
                            0 -> {
                                obsLayout.set(LoadingStatus.EMPTY)
                            }
                            else ->{
                                obsLayout.set(LoadingStatus.FULL)
                                adapter.updateList(res.couponsList)
                            }
                        }
                    }
                    else -> {
                        apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                    }
                }
            }
            isFirstTime = false
        }
    }

    fun onBackPressed()
    {
        setValue(Codes.BACK_PRESSED)
    }

    fun onLoginClicked() {
        setValue(Codes.LOGIN_CLICKED)
    }

    fun getUserStatus() {
        when {
            PrefMethods.getUserData() != null -> {
                getCoupons()
            }
            else -> {
                obsLayout.set(LoadingStatus.NOTLOGIN)
            }
        }
    }

    init {
        getUserStatus()
    }
}
