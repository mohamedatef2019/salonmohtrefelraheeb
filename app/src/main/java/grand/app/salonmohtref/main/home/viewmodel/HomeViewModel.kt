package grand.app.salonmohtref.main.home.viewmodel

import androidx.databinding.ObservableField
import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.base.LoadingStatus
import grand.app.salonmohtref.main.bottombar.home.request.UpdateTokenRequest
import grand.app.salonmohtref.main.bottombar.home.response.CategoriesItem
import grand.app.salonmohtref.main.bottombar.home.response.HomeResponse
import grand.app.salonmohtref.main.bottombar.home.response.UpdateTokenResponse
import grand.app.salonmohtref.main.bottombar.home.view.BannersAdapter
import grand.app.salonmohtref.main.home.view.CategoriesAdapter
import grand.app.salonmohtref.network.ApiResponse
import grand.app.salonmohtref.utils.constants.Codes
import grand.app.salonmohtref.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.ArrayList

class HomeViewModel : BaseViewModel()
{
    var bannersAdapter = BannersAdapter()
    var categoriesAdapter = CategoriesAdapter()
    var obsSearchNam = ObservableField<String>()

    fun onServicesClick() {
        setValue(Codes.SERVICES_CLICKED)
    }

    fun onExperianceClick() {
        setValue(Codes.EXPERIANCE_CLICKED)
    }

    fun onRatingClick() {
        setValue(Codes.RATING_CLICKED)
    }

    fun onFavouritesClick() {
        setValue(Codes.FAVOURITES_CLICKED)
    }

    fun onSearchClicked() {
        when {
            obsSearchNam.get() == null -> {
                setValue(Codes.EMPTY_SALON_NAME)
            }
            else -> {
                setValue(Codes.SEARCH_CLICKED)
            }
        }
    }



    fun getHomePage() {
        obsLayout.set(LoadingStatus.SHIMMER)
        requestCall<HomeResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().getHome() }
        })
        { res ->
            when (res!!.code) {
                200 -> {
                    obsLayout.set(LoadingStatus.FULL)
                    apiResponseLiveData.value = ApiResponse.success(res)
                    categoriesAdapter.updateList(res.homeData?.homeCategoriesList as ArrayList<CategoriesItem>)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }

    fun updateToken(token: String) {
        requestCall<UpdateTokenResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().updateToken(UpdateTokenRequest(token)) }
        })
        { res ->
            when (res!!.code) {
                200 -> {

                }
                401 -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }
}