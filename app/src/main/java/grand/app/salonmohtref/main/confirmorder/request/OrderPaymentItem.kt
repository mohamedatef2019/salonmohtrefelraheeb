package grand.app.salonmohtref.main.confirmorder.request

data class OrderPaymentItem(
    var id: Int? = null,
    var name: String? = null,
    var desc: String? = null
)
