package grand.app.salonmohtref.main.bookings.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.salonmohtref.R
import grand.app.salonmohtref.databinding.ItemBookingServiceBinding
import grand.app.salonmohtref.main.bookings.response.BookingServiceItem
import java.util.*

class BookingServiceAdapter : RecyclerView.Adapter<BookingServiceAdapter.SalonServicesHolder>()
{
    var itemsList: ArrayList<BookingServiceItem> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SalonServicesHolder
    {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemBookingServiceBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_booking_service, parent, false)
        return SalonServicesHolder(binding)
    }

    override fun onBindViewHolder(holder: SalonServicesHolder, position: Int) {
        val itemViewModel = ItemBookingServiceViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<BookingServiceItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class SalonServicesHolder(val binding: ItemBookingServiceBinding) : RecyclerView.ViewHolder(binding.root)
}
