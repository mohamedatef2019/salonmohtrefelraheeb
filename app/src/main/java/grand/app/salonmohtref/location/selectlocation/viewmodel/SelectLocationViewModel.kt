package grand.app.salonmohtref.location.selectlocation.viewmodel

import androidx.databinding.ObservableField
import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.base.LoadingStatus
import grand.app.salonmohtref.location.selectlocation.view.AddressesAdapter
import grand.app.salonmohtref.location.util.AddressItem
import grand.app.salonmohtref.utils.PrefMethods
import grand.app.salonmohtref.utils.constants.Codes
import grand.app.salonmohtref.utils.constants.Const

class SelectLocationViewModel : BaseViewModel()
{
    var adapter = AddressesAdapter()
    val obsAddress = ObservableField<String>()
    var latitude =  Const.latitude
    var longitude = Const.longitude
    var addressesList = ArrayList<AddressItem>()

    init {
        getSavedAddress()
    }

    private fun getSavedAddress() {
        when (PrefMethods.getSavedAddress()!!.size) {
            0 -> {
                obsLayout.set(LoadingStatus.EMPTY)
            }
            else -> {
                addressesList = PrefMethods.getSavedAddress() as ArrayList<AddressItem>
                 adapter.updateList(PrefMethods.getSavedAddress() as ArrayList<AddressItem>)
                obsLayout.set(LoadingStatus.FULL)
            }
        }
    }

    fun onBackClicked() {
        setValue(Codes.BACK_PRESSED)
    }

    fun onCurrentLocationClicked() {
        setValue(Codes.GETTING_CURRENT_LOCATION)
    }

    fun onSearchClicked() {
        setValue(Codes.GET_LOCATION_FROM_MAP)
    }
}