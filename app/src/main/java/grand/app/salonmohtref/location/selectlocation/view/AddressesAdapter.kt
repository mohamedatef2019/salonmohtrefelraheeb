package grand.app.salonmohtref.location.selectlocation.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.salonmohtref.R
import grand.app.salonmohtref.databinding.ItemAddressBinding
import grand.app.salonmohtref.location.selectlocation.viewmodel.ItemAddressViewModel
import grand.app.salonmohtref.location.util.AddressItem
import grand.app.salonmohtref.utils.SingleLiveEvent
import java.util.*

class AddressesAdapter : RecyclerView.Adapter<AddressesAdapter.AddressHolder>()
{
    var itemsList: ArrayList<AddressItem> = ArrayList()
    var itemLiveData = SingleLiveEvent<AddressItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddressHolder
    {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: ItemAddressBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_address, parent, false)
        return AddressHolder(binding)
    }

    override fun onBindViewHolder(holder: AddressHolder, position: Int) {
        val itemViewModel = ItemAddressViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.binding.rawLayout.setOnClickListener {
            itemLiveData.value = itemViewModel.item
        }
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<AddressItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class AddressHolder(val binding: ItemAddressBinding) : RecyclerView.ViewHolder(binding.root)
}
