package grand.app.salonmohtref.location.selectlocation.viewmodel

import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.location.util.AddressItem

class ItemAddressViewModel(var item: AddressItem) : BaseViewModel()