/*
package com.tarashly.user.utils.fcm

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class FirebaseNotficationResponse(

		@field:SerializedName("notification_type")
		val notificationType: Int? = null,

		@field:SerializedName("image")
		val image: String? = null,

		@field:SerializedName("type_data")
		val typeData: Int? = null,

		@field:SerializedName("sender_type")
		val senderType: Int? = null,

		@field:SerializedName("sound")
		val sound: String? = null,

		@field:SerializedName("icon")
		val icon: String? = null,

		@field:SerializedName("audio")
		val audio: String? = null,

		@field:SerializedName("title")
		val title: String? = null,

		@field:SerializedName("body")
		val body: String? = null,

		@field:SerializedName("order_id")
		val orderId: Int? = null,

		@field:SerializedName("wallet")
		val wallet: String? = null


) : Parcelable {
	override fun toString(): String {
		return "FirebaseNotficationResponse(notificationType=$notificationType, image=$image, typeData=$typeData, senderType=$senderType, sound=$sound, icon=$icon, audio=$audio, title=$title, body=$body, orderId=$orderId, wallet=$wallet)"
	}
}


*/
