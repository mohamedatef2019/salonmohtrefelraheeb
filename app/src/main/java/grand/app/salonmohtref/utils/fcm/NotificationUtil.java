/*
package grand.app.salonssuser.utils.fcm;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.os.Build;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.RemoteMessage;
import com.tarashly.user.R;
import com.tarashly.user.application.MyApplication;
import com.tarashly.user.data.Codes;
import com.tarashly.user.ui.activity.HomeActivity;
import com.tarashly.user.utils.PrefMethods;
import com.tarashly.user.utils.fcm.FirebaseNotficationResponse;

import java.util.Map;

import static com.tarashly.user.utils.AppUtil.getString;

*/
/**
 * Created by MahmoudAyman on 3/29/2020.
 **//*

public abstract class NotificationUtil {
    static void sendNotification(FirebaseNotficationResponse response) {
        if (new PrefMethods().getUserData() != null)
            sendNow(response.getBody(), response);
    }

    public static void sendNotification(RemoteMessage.Notification notification) {
        sendNow(notification.getTitle(), notification.getBody());
    }

    public static void sendNotification(Map<String, String> data) {
        sendNow(data.get("title"), data.get("body"));
    }

    public static void sendNow(String title, String body) {
        NotificationCompat.Builder notificationBuilder = getNotificationBuilder()
                .setContentTitle(title)
                .setContentText(body);
        notify(notificationBuilder);
    }

    private static void sendNow(String body, FirebaseNotficationResponse response)
    {
        NotificationCompat.Builder notificationBuilder = getNotificationBuilder(response).setContentText(body);
        notify(notificationBuilder);
    }

    private static NotificationCompat.Builder getNotificationBuilder(FirebaseNotficationResponse response) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(MyApplication.getAppContext(), "0");
        Intent intent = new Intent();
        intent.putExtra(Codes.NOTIFICATION_RESPONSE, response);
        intent.setClass(MyApplication.getAppContext(), HomeActivity.class);
       // intent.setClass(MyApplication.getAppContext(), HomeActivity.class);
        if (response.getNotificationType() != null)
        {
            if (response.getNotificationType() == Codes.NOTIFICATION_CHAT)
            {

                //chat
            }
            else if (response.getNotificationType() == Codes.NOTIFICATION_WALLET)
            {
                //update wallet
                new PrefMethods().saveWallet(response.getWallet());
            }
            else if (response.getNotificationType() == Codes.NOTIFICATION_ADMIN)
            {
                //from admin
            }
            else if (response.getNotificationType() == Codes.NOTIFICATION_ORDER)
            {
                // order
            }
            else if (response.getNotificationType() == Codes.NOTIFICATION_RATE)
            {

                // rate
            }
        }
        setPendingIntent(intent, builder);
        return builder;
    }

    private static void setPendingIntent(Intent intent, NotificationCompat.Builder builder) {
        PendingIntent pendingIntent = PendingIntent.getActivity(MyApplication.getAppContext().getApplicationContext(),
                (int) System.currentTimeMillis() */
/*Request code*//*
,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pendingIntent);
        builder.setSmallIcon(R.mipmap.ic_launcher_round)
                .setContentTitle(getString(R.string.app_name))
                .setAutoCancel(true);
        builder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
        builder.setDefaults(Notification.DEFAULT_VIBRATE);
    }


    private static NotificationCompat.Builder getNotificationBuilder() {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(MyApplication.getAppContext(), "0");
        //Intent intent = new Intent(MyApplication.getAppContext(), HomeActivity.class);
        Intent intent = new Intent(MyApplication.getAppContext(), HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        setPendingIntent(intent, builder);
        return builder;
    }

    private static void notify(NotificationCompat.Builder notificationBuilder) {
        NotificationManager notificationManager = (NotificationManager) MyApplication.getAppContext().getSystemService(Context.NOTIFICATION_SERVICE);
        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("0",
                    "notification",
                    NotificationManager.IMPORTANCE_HIGH);
            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .build();
            channel.enableLights(true);
            channel.enableVibration(true);
            channel.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION), audioAttributes);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(channel);
            }
        }
        if (notificationManager != null)
            notificationManager.notify(0, notificationBuilder.build());
    }
}
*/
