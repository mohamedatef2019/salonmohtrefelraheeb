package grand.app.salonmohtref.network

import grand.app.salonmohtref.auth.forgotpass.request.ForgotRequest
import grand.app.salonmohtref.auth.forgotpass.response.ForgotResponse
import grand.app.salonmohtref.auth.login.request.LoginRequest
import grand.app.salonmohtref.auth.login.response.LoginResponse
import grand.app.salonmohtref.auth.register.response.RegisterResponse
import grand.app.salonmohtref.auth.resetpass.request.ResetRequest
import grand.app.salonmohtref.auth.resetpass.response.ResetPassResponse
import grand.app.salonmohtref.auth.splash.response.SettingResponse
import grand.app.salonmohtref.auth.splash.response.SplashResponse
import grand.app.salonmohtref.auth.verifycode.request.VerifyRequest
import grand.app.salonmohtref.auth.verifycode.response.VerifyResponse
import grand.app.salonmohtref.main.aboutapp.response.AboutAppResponse
import grand.app.salonmohtref.dialogs.addrate.reuqest.AddRateRequest
import grand.app.salonmohtref.dialogs.addrate.response.AddRateResponse
import grand.app.salonmohtref.main.aboutapp.response.ExperianceResponse
import grand.app.salonmohtref.main.allsalonss.request.HomeShopsRequest
import grand.app.salonmohtref.main.allsalonss.response.AllSalonsResponse
import grand.app.salonmohtref.main.salonbarbers.request.AllBarbersRequest
import grand.app.salonmohtref.main.bookings.response.MyBookingsResponse
import grand.app.salonmohtref.main.bookings.response.CancelOrderResponse
import grand.app.salonmohtref.main.bottombar.home.request.UpdateTokenRequest
import grand.app.salonmohtref.main.bottombar.home.response.HomeResponse
import grand.app.salonmohtref.main.bottombar.home.response.UpdateTokenResponse
import grand.app.salonmohtref.main.productdetails.response.ProductDetailsResponse
import grand.app.salonmohtref.main.bottombar.products.response.ProductsResponse
import grand.app.salonmohtref.main.branches.response.BranchesResponse
import grand.app.salonmohtref.main.suggestion.response.SuggestResponse
import grand.app.salonmohtref.main.editprofile.response.UpdateProfileResponse
import grand.app.salonmohtref.main.favorites.response.AddToFavResponse
import grand.app.salonmohtref.main.cancelapolicy.response.PrivacyPolicyResponse
import grand.app.salonmohtref.main.changepassword.request.ChangePassRequest
import grand.app.salonmohtref.main.confirmorder.request.OrderRequest
import grand.app.salonmohtref.main.confirmorder.response.CreateOrderResponse
import grand.app.salonmohtref.main.contactus.request.ContactRequest
import grand.app.salonmohtref.main.contactus.response.ContactUsResponse
import grand.app.salonmohtref.main.coupondetails.response.AddCouponResponse
import grand.app.salonmohtref.main.coupons.response.CouponResponse
import grand.app.salonmohtref.main.credit.response.CreditResponse
import grand.app.salonmohtref.main.favorites.response.FavoriteBarberResponse
import grand.app.salonmohtref.main.favorites.response.FavoriteSalonResponse
import grand.app.salonmohtref.main.package_details.response.AddPackageResponse
import grand.app.salonmohtref.main.mypackage.response.MyPackageResponse
import grand.app.salonmohtref.main.notifications.response.NotificationsResponse
import grand.app.salonmohtref.main.package_details.response.UpgradePackageResponse
import grand.app.salonmohtref.main.packages.response.PackagesResponse
import grand.app.salonmohtref.main.rates.response.RatesResponse
import grand.app.salonmohtref.main.salonbarbers.response.SalonBarbersResponse
import grand.app.salonmohtref.main.salondetails.response.SalonDetailsResponse
import grand.app.salonmohtref.main.search.request.SearchRequest
import grand.app.salonmohtref.main.search.response.SearchResponse
import grand.app.salonmohtref.main.selectaddress.response.CitiesResponse
import grand.app.salonmohtref.main.suggestion.request.SuggestRequest
import grand.app.salonmohtref.main.terms.response.TermsResponse
import grand.app.salonmohtref.main.wallet.response.WalletResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*

interface ApiInterface
{
    /******* Auth APIs  ********/

    // Splash
    @GET("api/settings/sliders")
    suspend fun splash(): SplashResponse

    // login
    @POST("api/user/login")
     suspend fun login(@Body loginRequest: LoginRequest): LoginResponse

    @Multipart
    @POST("api/user/register")
    suspend fun register(@Part("name") name: RequestBody?,
                         @Part("email") email: RequestBody?,
                         @Part("phone") phone: RequestBody?,
                         @Part("password") password: RequestBody?,
                         @Part("firebase_token") firebaseToken: RequestBody?,
                         @Part("type") type: RequestBody?,
                         @Part image: MultipartBody.Part?): RegisterResponse

    @Multipart
    @POST("api/user/update-profile")
    suspend fun updateProfile(@Header("jwt") jwt : String?,
                         @Part("name") name: RequestBody?,
                         @Part("email") email: RequestBody?,
                         @Part("phone") phone: RequestBody?,
                         @Part("address") address: RequestBody?,
                         @Part("lat") lat: RequestBody?,
                         @Part("lng") lng: RequestBody?,
                         @Part image: MultipartBody.Part?): UpdateProfileResponse

    @GET("api/settings/cities")
    suspend fun getCities(): CitiesResponse

    // forgotPass
    @POST("api/user/send-code")
    suspend fun forgotPass(@Body forgotPassRequest: ForgotRequest?): ForgotResponse

    // verify
    @POST("api/user/verify-code")
    suspend fun verify(@Body verifyCodeRequest: VerifyRequest?): VerifyResponse

    // resetPass
    @POST("api/user/change-password")
    suspend fun resetPass(@Body resetPassRequest: ResetRequest?): ResetPassResponse

    // resetPass
    @POST("api/user/change-password")
    suspend fun changePassword(@Header("jwt") jwt : String, @Body resetPassRequest: ChangePassRequest?): ResetPassResponse

    /******* Home APIs  ********/

//    @POST("api/salons/{catId}")
//    suspend fun getVisitorSalonsShops(@Path("catId") catId : Int): AllSalonsResponse
//
//    @POST("api/salons/{catId}")
//    suspend fun getUserSalonsShops(@Header("jwt") jwt : String,@Path("catId") catId : Int): AllSalonsResponse

    @POST("api/salons/by-lat-lng")
    suspend fun getVisitorAllSalons(@Body request : HomeShopsRequest): AllSalonsResponse

    @POST("api/salons/by-lat-lng")
    suspend fun getUserAllSalons(@Header("jwt") jwt : String,@Body request : HomeShopsRequest): AllSalonsResponse

    @POST("api/user/update-token")
    suspend fun updateToken(@Header("jwt") jwt : String, @Body request: UpdateTokenRequest): UpdateTokenResponse

    @GET("api/settings/home")
    suspend fun getHome(): HomeResponse

    @GET("api/salons/rates/{id}")
    suspend fun getRates(@Path("id") id : Int): RatesResponse

    @GET("api/salons/settings/all-rates")
    suspend fun getAllRates(): RatesResponse

    @POST("api/salons/add-rate")
    suspend fun addRate(@Header("jwt") jwt : String, @Body addRateRequest: AddRateRequest?): AddRateResponse

    @GET("api/salons/salon/{id}")
    suspend fun getVisitorSalonDetails(@Path("id") id : Int): SalonDetailsResponse

    @GET("api/salons/salon/{id}")
    suspend fun getUserSalonDetails(@Header("jwt") jwt : String, @Path("id") id : Int): SalonDetailsResponse

    @POST("api/settings/search")
    suspend fun searchSkipSalons(@Body request: SearchRequest): SearchResponse

    @POST("api/settings/search")
    suspend fun searchLoginSalons(@Header("jwt") jwt : String, @Body request: SearchRequest): SearchResponse

    @POST("api/orders/create-order")
    suspend fun createOrder(@Header("jwt") jwt : String, @Body request: OrderRequest): CreateOrderResponse

      /*--- PRODUCTS ---*/
    @GET("api/settings/products")
    suspend fun getProducts(): ProductsResponse

    @GET("api/settings/product/{productId}")
    suspend fun getProductDetails(@Path("productId")id : Int): ProductDetailsResponse

    /******* PROFILE APIs  ********/

    @GET("api/orders/{bookingsStatus}")
    suspend fun getMyBookings(@Header("jwt") jwt : String, @Path("bookingsStatus") bookingsStatus: Int,
                              @Query("pageNumber") pageNumber : Int): MyBookingsResponse

    @GET("api/orders/cancel-order/{bookingId}")
    suspend fun cancelBooking(@Header("jwt") jwt : String, @Path("bookingId") bookingId : Int): CancelOrderResponse

    @POST("api/salons/barbers")
    suspend fun getAllBarbers(@Header("jwt") jwt : String, @Body request : AllBarbersRequest): SalonBarbersResponse

    @GET("api/salons/favorite/{id}/{type}")
    suspend fun addToFav(@Header("jwt") jwt : String, @Path("id") id : Int, @Path("type") type : Int): AddToFavResponse

    @GET("api/salons/favorites/1")
    suspend fun getFavoriteBarbers(@Header("jwt") jwt : String): FavoriteBarberResponse

    @GET("api/salons/favorites/0")
    suspend fun getFavoriteSalons(@Header("jwt") jwt : String): FavoriteSalonResponse


    @GET("api/salons/my-points")
    suspend fun getMyCredit(@Header("jwt") jwt : String): CreditResponse

    @GET("api/salons/my-wallet")
    suspend fun getWallet(@Header("jwt") jwt : String): WalletResponse

    @GET("api/settings/coupons")
    suspend fun getAllCoupons(): CouponResponse

    @GET("api/settings/coupons")
    suspend fun getMyCoupons(@Header("jwt") jwt : String): CouponResponse

    @FormUrlEncoded
    @POST("api/orders/add-coupon")
    suspend fun addCoupon(@Header("jwt") jwt : String, @Field("coupon_id") id : Int): AddCouponResponse

    @GET("api/orders/get-package")
    suspend fun getMyPackage(@Header("jwt") jwt : String): MyPackageResponse

    @GET("api/settings/packages")
    suspend fun getPackages(): PackagesResponse

    @FormUrlEncoded
    @POST("api/orders/add-package")
    suspend fun addPackage(@Header("jwt") jwt : String, @Field("package_id") packageId : Int): AddPackageResponse

    @GET("api/orders/upgrade-package/{package_id}")
    suspend fun upgradePackage(@Header("jwt") jwt : String, @Path("package_id") packageId : Int): UpgradePackageResponse

    /******* MORE APIs  ********/

    /*--- MORE PAGE ---*/
    @GET("api/settings/notifications")
    suspend fun getNotifications(@Header("jwt") jwt : String): NotificationsResponse

    // Settings - Social links
    @GET("api/settings/settings")
    suspend fun getSettings(): SettingResponse

    // Terms
    @GET("api/settings/terms")
    suspend fun getTerms(): TermsResponse

    // ABOUT
    @GET("api/settings/about")
    suspend fun getAboutApp(): AboutAppResponse

    // Experiences
    @GET("api/settings/experiences")
    suspend fun getExperience(): ExperianceResponse

    // Terms
    @GET("api/settings/cancellation-policy")
    suspend fun getCancellationPolicy(): PrivacyPolicyResponse

    @POST("api/settings/contact-us")
    suspend fun sendHelpMsg(@Body request: ContactRequest): ContactUsResponse

    @POST("api/settings/suggestions")
    suspend fun sendSuggest(@Body request: SuggestRequest): SuggestResponse

    @GET("api/salons/branches")
    suspend fun getVisitorSalonBranches(): BranchesResponse

    @GET("api/salons/branches")
    suspend fun getUserSalonBranches(@Header("jwt") jwt : String): BranchesResponse

    @POST("api/salons/branches/by-lat-lng")
    suspend fun getVisitorHomeBranches(@Body request: HomeShopsRequest): BranchesResponse

    @POST("api/salons/branches/by-lat-lng")
    suspend fun getUserHomeBranches(@Header("jwt") jwt : String ?, @Body request: HomeShopsRequest): BranchesResponse
}