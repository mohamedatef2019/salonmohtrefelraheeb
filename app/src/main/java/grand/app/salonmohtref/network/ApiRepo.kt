package grand.app.salonmohtref.network

import grand.app.salonmohtref.auth.forgotpass.request.ForgotRequest
import grand.app.salonmohtref.auth.login.request.LoginRequest
import grand.app.salonmohtref.auth.register.request.RegisterRequest
import grand.app.salonmohtref.auth.resetpass.request.ResetRequest
import grand.app.salonmohtref.auth.verifycode.request.VerifyRequest
import grand.app.salonmohtref.dialogs.addrate.reuqest.AddRateRequest
import grand.app.salonmohtref.main.allsalonss.request.HomeShopsRequest
import grand.app.salonmohtref.main.salonbarbers.request.AllBarbersRequest
import grand.app.salonmohtref.main.bottombar.home.request.UpdateTokenRequest
import grand.app.salonmohtref.main.changepassword.request.ChangePassRequest
import grand.app.salonmohtref.main.confirmorder.request.OrderRequest
import grand.app.salonmohtref.main.contactus.request.ContactRequest
import grand.app.salonmohtref.main.editprofile.request.EditProfileRequest
import grand.app.salonmohtref.main.search.request.SearchRequest
import grand.app.salonmohtref.main.suggestion.request.SuggestRequest
import grand.app.salonmohtref.utils.PrefMethods
import grand.app.salonmohtref.utils.toMultiPart
import grand.app.salonmohtref.utils.toRequestBodyParam

/**
 * Created by MouazSalah on 28/12/2020.
 **/

class ApiRepo(private val apiInterface: ApiInterface) {

    suspend fun login(request : LoginRequest) = apiInterface.login(request)

    suspend fun register(request : RegisterRequest) = apiInterface.register(
        request.name?.toRequestBodyParam(),
        request.email?.toRequestBodyParam(),
        request.phone?.toRequestBodyParam(),
        request.password?.toRequestBodyParam(),
        request.firebase_token?.toRequestBodyParam(),
        request.type.toRequestBodyParam(),
        request.userImg?.toMultiPart("img")
    )

    suspend fun forgotPassword(request : ForgotRequest) = apiInterface.forgotPass(request)
    suspend fun verifyCode(request : VerifyRequest) = apiInterface.verify(request)
    suspend fun resetPassword(request : ResetRequest) = apiInterface.resetPass(request)
    suspend fun changePassword(request : ChangePassRequest) = apiInterface.changePassword(PrefMethods.getUserData()?.jwt.toString(), request)

    suspend fun splash() = apiInterface.splash()

    suspend fun getTerms() = apiInterface.getTerms()
    suspend fun getAboutApp() = apiInterface.getAboutApp()
    suspend fun getPrivacyPolicy() = apiInterface.getCancellationPolicy()
    suspend fun getExperience() = apiInterface.getExperience()
    suspend fun getHome() = apiInterface.getHome()


    suspend fun getVisitorHomeShops(request : HomeShopsRequest) = apiInterface.getVisitorAllSalons(request)
    suspend fun getUserHomeShops(request : HomeShopsRequest) = apiInterface.getUserAllSalons(PrefMethods.getUserData()?.jwt.toString(), request)

    suspend fun getVisitorSalonBranches() = apiInterface.getVisitorSalonBranches()
    suspend fun getUserSalonBranches() = apiInterface.getUserSalonBranches(PrefMethods.getUserData()?.jwt.toString())
    suspend fun getVisitorHomeBranches(request : HomeShopsRequest) = apiInterface.getVisitorHomeBranches(request)
    suspend fun getUserHomeBranches(request : HomeShopsRequest) = apiInterface.getUserHomeBranches(PrefMethods.getUserData()?.jwt.toString(), request)

    suspend fun getProducts() = apiInterface.getProducts()
    suspend fun getProductDetails(productId : Int) = apiInterface.getProductDetails(productId)

    suspend fun addToFav(itemId : Int, type : Int) = apiInterface.addToFav(PrefMethods.getUserData()?.jwt.toString(), itemId, type)

    suspend fun getAllRates() = apiInterface.getAllRates()
    suspend fun getRates(salonId : Int) = apiInterface.getRates(salonId)
    suspend fun addRate(addRateRequest : AddRateRequest) = apiInterface.addRate(PrefMethods.getUserData()?.jwt.toString(), addRateRequest)

    suspend fun getVisitorSalonDetails(salonId : Int) = apiInterface.getVisitorSalonDetails(salonId)
    suspend fun getUserSalonDetails(salonId : Int) = apiInterface.getUserSalonDetails(PrefMethods.getUserData()?.jwt.toString(), salonId)

    suspend fun sendHelpMessage(request : ContactRequest) = apiInterface.sendHelpMsg(request)
    suspend fun sendSuggest(request : SuggestRequest) = apiInterface.sendSuggest(request)

    suspend fun getSocialLinks() = apiInterface.getSettings()

    /******* Profile APIs  ********/

    suspend fun updateProfile(request : EditProfileRequest) = apiInterface.updateProfile(
            PrefMethods.getUserData()?.jwt.toString(),
            request.name?.toRequestBodyParam(),
            request.email?.toRequestBodyParam(),
            request.phone?.toRequestBodyParam(),
            request.address?.toRequestBodyParam(),
            request.lat?.toRequestBodyParam(),
            request.lng?.toRequestBodyParam(),
            request.userImg?.toMultiPart("img")
    )

    suspend fun getMyBookings(bookingState : Int, pageNumber : Int) = apiInterface.getMyBookings(PrefMethods.getUserData()?.jwt.toString(), bookingState, pageNumber)

    suspend fun getMyCredit() = apiInterface.getMyCredit(PrefMethods.getUserData()?.jwt.toString())

    suspend fun createOrder(request : OrderRequest) = apiInterface.createOrder(PrefMethods.getUserData()?.jwt.toString(), request)

    suspend fun getWallet() = apiInterface.getWallet(PrefMethods.getUserData()?.jwt.toString())

    suspend fun getMyCoupons() = apiInterface.getMyCoupons(PrefMethods.getUserData()?.jwt.toString())
    suspend fun getAllCoupons() = apiInterface.getAllCoupons()

    suspend fun getMyPackage() = apiInterface.getMyPackage(PrefMethods.getUserData()?.jwt.toString())

    suspend fun addCoupon(id : Int) = apiInterface.addCoupon(PrefMethods.getUserData()?.jwt.toString(), id)

    suspend fun addPackage(packageId : Int) = apiInterface.addPackage(PrefMethods.getUserData()?.jwt.toString(), packageId)

    suspend fun upgradePackage(packageId : Int) = apiInterface.upgradePackage(PrefMethods.getUserData()?.jwt.toString(), packageId)

    suspend fun getAllPackages() = apiInterface.getPackages()

    suspend fun updateToken(request : UpdateTokenRequest) = apiInterface.updateToken(PrefMethods.getUserData()?.jwt.toString(), request)

    suspend fun getNotifications() = apiInterface.getNotifications(PrefMethods.getUserData()?.jwt.toString())

    suspend fun getCities() = apiInterface.getCities()

    suspend fun getAllBarbers(request : AllBarbersRequest) = apiInterface.getAllBarbers(PrefMethods.getUserData()?.jwt.toString(), request)

    suspend fun getFavoriteSalons() = apiInterface.getFavoriteSalons(PrefMethods.getUserData()?.jwt.toString())
    suspend fun getFavoriteBarbers() = apiInterface.getFavoriteBarbers(PrefMethods.getUserData()?.jwt.toString())

    suspend fun searchSkipSalons(request : SearchRequest) = apiInterface.searchSkipSalons(request)
    suspend fun searchLoginSalons(request : SearchRequest) = apiInterface.searchLoginSalons(PrefMethods.getUserData()?.jwt.toString(), request)
    suspend fun cancelOrder(id : Int) = apiInterface.cancelBooking(PrefMethods.getUserData()?.jwt.toString(), id)
}