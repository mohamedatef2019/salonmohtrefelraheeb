package grand.app.salonmohtref.activity.auth

import android.os.Bundle
import android.view.View
import androidx.activity.addCallback
import androidx.core.content.ContextCompat
import androidx.navigation.fragment.findNavController
import grand.app.salonmohtref.R
import grand.app.salonmohtref.base.BaseFragment

open class BaseAuthFragment : BaseFragment()
{
    override fun onDestroyView() {
        super.onDestroyView()
        if (!isNavigated)
            requireActivity().onBackPressedDispatcher.addCallback(this) {
                val navController = findNavController()
                if (navController.currentBackStackEntry?.destination?.id != null)
                {
                    findNavController().navigateUp()
                }
                else
                    navController.popBackStack()
            }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

//    fun setBarName(title: String, show : Boolean = false , color: Int = ContextCompat.getColor(requireActivity(), R.color.white)) {
//        (requireActivity() as AuthActivity).viewModel?.obsShowToolbar!!.set(show)
//        (requireActivity() as AuthActivity).customToolBar(title, show , color)
//    }

}

