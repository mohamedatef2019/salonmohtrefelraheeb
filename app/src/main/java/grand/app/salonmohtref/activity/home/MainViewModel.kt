package grand.app.salonmohtref.activity.home

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import grand.app.salonmohtref.auth.splash.response.SettingResponse
import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.utils.PrefMethods
import grand.app.salonmohtref.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class MainViewModel : BaseViewModel()
{
    val obsShowToolbar = ObservableBoolean()
    val obsTitle = ObservableField<String>()
    val obsProgressBar = ObservableField(false)

    init {
        getSocialLinks()
    }

    fun getSocialLinks()
    {
        requestCall<SettingResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().getSocialLinks() }
        })
        { res ->
            when (res!!.code) {
                200 -> {
                    PrefMethods.saveSettingData(res.settingData!!)
                    notifyChange()
                }
            }
        }
    }
}