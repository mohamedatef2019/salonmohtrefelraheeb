package grand.app.salonmohtref.activity.home

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.WindowManager
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import grand.app.salonmohtref.R
import grand.app.salonmohtref.activity.auth.AuthActivity
import grand.app.salonmohtref.base.BaseActivity
import grand.app.salonmohtref.databinding.ActivityMainBinding
import grand.app.salonmohtref.dialogs.language.DialogLanguageFragment
import grand.app.salonmohtref.utils.LocalUtil
import grand.app.salonmohtref.utils.Utils
import grand.app.salonmohtref.utils.constants.Codes
import grand.app.salonmohtref.utils.constants.Const
import grand.app.salonmohtref.utils.constants.Params

class MainActivity : BaseActivity()
{
    private lateinit var binding: ActivityMainBinding
    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var navView: BottomNavigationView
    var viewModel: MainViewModel? = null
    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        //init view model
        initViewModel()
        binding.viewModel = viewModel

        navView = findViewById(R.id.bottomBar)
        navController = findNavController(R.id.nav_home_host_fragment)
        appBarConfiguration = AppBarConfiguration(setOf(R.id.navigation_home, R.id.navigation_favorites,
                R.id.navigation_branches, R.id.navigation_profile, R.id.navigation_more))
        navView.setupWithNavController(navController)


        binding.ivMainChangeLanguage.setOnClickListener {
            Utils.startDialogActivity(this, DialogLanguageFragment::class.java.name, Codes.DIALOG_SELECT_LANGUAGE, null)
        }

        binding.ivMainNotifications.setOnClickListener {
            customBarColor(ContextCompat.getColor(this, R.color.color_primary))
            viewModel?.obsShowToolbar!!.set(true)
            viewModel?.obsTitle!!.set(getString(R.string.title_notifications))
            binding.ibBack.visibility = View.VISIBLE
            navController.navigate(R.id.navigation_notifications)
        }

        binding.ibBack.setOnClickListener {
            when (navController.currentDestination?.id) {
                R.id.navigation_home -> {
                    finishAffinity()
                }
                else -> {
                    navController.navigateUp()
                }
            }
        }
        setUpToolbarAndStatusBar()
    }

    private fun setUpToolbarAndStatusBar()
    {
        navController.addOnDestinationChangedListener { controller, destination, arguments ->
            when (destination.id) {
                R.id.navigation_home -> {
                    customBarColor(ContextCompat.getColor(this, R.color.color_primary))
                    viewModel?.obsShowToolbar!!.set(true)
                    viewModel?.obsTitle!!.set(getString(R.string.title_home))
                    binding.bottomBar.visibility = View.VISIBLE
                    binding.ibBack.visibility = View.GONE
                }
                R.id.navigation_products -> {
                    customBarColor(ContextCompat.getColor(this, R.color.color_primary))
                    viewModel?.obsShowToolbar!!.set(true)
                    viewModel?.obsTitle!!.set(getString(R.string.title_products))
                    binding.ibBack.visibility = View.GONE
                }
                R.id.navigation_product_details -> {
                    customBarColor(ContextCompat.getColor(this, R.color.color_primary))
                    viewModel?.obsShowToolbar!!.set(true)
                    viewModel?.obsTitle!!.set(getString(R.string.title_products))
                    binding.ibBack.visibility = View.VISIBLE
                }
                R.id.navigation_barber_profile -> {
                    customBarColor(ContextCompat.getColor(this, R.color.color_primary))
                    viewModel?.obsShowToolbar!!.set(true)
                    viewModel?.obsTitle!!.set("")
                    binding.ibBack.visibility = View.VISIBLE
                }
                R.id.navigation_search -> {
                    customBarColor(ContextCompat.getColor(this, R.color.color_primary))
                    viewModel?.obsShowToolbar!!.set(true)
                    viewModel?.obsTitle!!.set(getString(R.string.title_search))
                    binding.ibBack.visibility = View.VISIBLE
                }
                R.id.navigation_confirm_order -> {
                    customBarColor(ContextCompat.getColor(this, R.color.color_primary))
                    viewModel?.obsShowToolbar!!.set(true)
                    viewModel?.obsTitle!!.set(getString(R.string.title_confirm_order))
                    binding.ibBack.visibility = View.VISIBLE
                }
                R.id.navigation_order_time -> {
                    customBarColor(ContextCompat.getColor(this, R.color.color_primary))
                    viewModel?.obsShowToolbar!!.set(true)
                    viewModel?.obsTitle!!.set(getString(R.string.title_order_time))
                    binding.ibBack.visibility = View.VISIBLE
                }
                R.id.navigation_salon_barbers -> {
                    customBarColor(ContextCompat.getColor(this, R.color.color_primary))
                    viewModel?.obsShowToolbar!!.set(true)
                    viewModel?.obsTitle!!.set(getString(R.string.title_choose_barber))
                    binding.ibBack.visibility = View.VISIBLE
                }
                R.id.navigation_branches -> {
                    customBarColor(ContextCompat.getColor(this, R.color.color_primary))
                    viewModel?.obsShowToolbar!!.set(true)
                    viewModel?.obsTitle!!.set(getString(R.string.title_branches))
                    binding.bottomBar.visibility = View.VISIBLE
                    binding.ibBack.visibility = View.GONE
                }
                 R.id.navigation_rates -> {
                    customBarColor(ContextCompat.getColor(this, R.color.color_primary))
                    viewModel?.obsShowToolbar!!.set(true)
                    viewModel?.obsTitle!!.set(getString(R.string.title_rates))
                    binding.bottomBar.visibility = View.GONE
                     binding.ibBack.visibility = View.VISIBLE
                 }
                R.id.navigation_favorites -> {
                    customBarColor(ContextCompat.getColor(this, R.color.color_primary))
                    viewModel?.obsShowToolbar!!.set(true)
                    viewModel?.obsTitle!!.set(getString(R.string.title_favorite))
                    binding.bottomBar.visibility = View.VISIBLE
                    binding.ibBack.visibility = View.VISIBLE
                }
                R.id.navigation_select_address -> {
                    customBarColor(ContextCompat.getColor(this, R.color.color_primary))
                    viewModel?.obsShowToolbar!!.set(true)
                    viewModel?.obsTitle!!.set(getString(R.string.title_select_address))
                    binding.bottomBar.visibility = View.VISIBLE
                    binding.ibBack.visibility = View.VISIBLE
                }
                R.id.navigation_change_pass -> {
                    customBarColor(ContextCompat.getColor(this, R.color.color_primary))
                    viewModel?.obsShowToolbar!!.set(true)
                    viewModel?.obsTitle!!.set(getString(R.string.title_change_password))
                    binding.bottomBar.visibility = View.VISIBLE
                    binding.ibBack.visibility = View.VISIBLE
                }
                R.id.navigation_profile -> {
                    customBarColor(ContextCompat.getColor(this, R.color.color_primary))
                    viewModel?.obsShowToolbar!!.set(true)
                    viewModel?.obsTitle!!.set(getString(R.string.title_profile))
                    binding.bottomBar.visibility = View.VISIBLE
                    binding.ibBack.visibility = View.GONE
                }
                R.id.navigation_salon_details -> {
                    customBarColor(ContextCompat.getColor(this, R.color.color_primary))
                    viewModel?.obsShowToolbar!!.set(false)
                    viewModel?.obsTitle!!.set(getString(R.string.title_salon_details))
                    binding.ibBack.visibility = View.VISIBLE
                }
                R.id.navigation_all_salons -> {
                    customBarColor(ContextCompat.getColor(this, R.color.color_primary))
                    viewModel?.obsShowToolbar!!.set(true)
                    viewModel?.obsTitle!!.set(getString(R.string.title_all_salons))
                    binding.ibBack.visibility = View.VISIBLE
                }
                R.id.navigation_edit_profile -> {
                    customBarColor(ContextCompat.getColor(this, R.color.color_primary))
                    viewModel?.obsShowToolbar!!.set(true)
                    viewModel?.obsTitle!!.set(getString(R.string.title_edit_profile))
                    binding.ibBack.visibility = View.VISIBLE
                }
                 R.id.navigation_wallet -> {
                    customBarColor(ContextCompat.getColor(this, R.color.color_primary))
                    viewModel?.obsShowToolbar!!.set(true)
                    viewModel?.obsTitle!!.set(getString(R.string.title_wallet))
                     binding.ibBack.visibility = View.VISIBLE
                 }
                R.id.navigation_credit -> {
                    customBarColor(ContextCompat.getColor(this, R.color.color_primary))
                    viewModel?.obsShowToolbar!!.set(true)
                    viewModel?.obsTitle!!.set(getString(R.string.title_my_credit))
                    binding.ibBack.visibility = View.VISIBLE
                }
                R.id.navigation_coupons -> {
                    customBarColor(ContextCompat.getColor(this, R.color.color_primary))
                    viewModel?.obsShowToolbar!!.set(true)
                    viewModel?.obsTitle!!.set(getString(R.string.title_coupons))
                    binding.ibBack.visibility = View.VISIBLE
                }
                R.id.navigation_coupon_details -> {
                    customBarColor(ContextCompat.getColor(this, R.color.color_primary))
                    viewModel?.obsShowToolbar!!.set(false)
                }
                R.id.navigation_my_package, R.id.navigation_all_packages -> {
                    customBarColor(ContextCompat.getColor(this, R.color.color_primary))
                    viewModel?.obsShowToolbar!!.set(true)
                    viewModel?.obsTitle!!.set(getString(R.string.title_my_package))
                    binding.ibBack.visibility = View.VISIBLE
                }
                R.id.navigation_package_details -> {
                    customBarColor(ContextCompat.getColor(this, R.color.color_primary))
                    viewModel?.obsShowToolbar!!.set(false)
                }
                R.id.navigation_bookings -> {
                    customBarColor(ContextCompat.getColor(this, R.color.color_primary))
                    viewModel?.obsShowToolbar!!.set(true)
                    viewModel?.obsTitle!!.set(getString(R.string.title_my_bookings))
                    binding.ibBack.visibility = View.VISIBLE
                }
                R.id.navigation_more -> {
                    customBarColor(ContextCompat.getColor(this, R.color.color_primary))
                    viewModel?.obsShowToolbar!!.set(true)
                    viewModel?.obsTitle!!.set(getString(R.string.title_more))
                    binding.bottomBar.visibility = View.VISIBLE
                    binding.ibBack.visibility = View.GONE
                }
                R.id.navigation_notifications -> {
                    customBarColor(ContextCompat.getColor(this, R.color.color_primary))
                    viewModel?.obsShowToolbar!!.set(true)
                    viewModel?.obsTitle!!.set(getString(R.string.title_notifications))
                    binding.ibBack.visibility = View.VISIBLE
                }
                R.id.navigation_terms -> {
                    customBarColor(ContextCompat.getColor(this, R.color.color_primary))
                    viewModel?.obsShowToolbar!!.set(true)
                    viewModel?.obsTitle!!.set(getString(R.string.title_terms))
                    binding.ibBack.visibility = View.VISIBLE
                }
                R.id.navigation_about_app -> {
                    customBarColor(ContextCompat.getColor(this, R.color.color_primary))
                    viewModel?.obsShowToolbar!!.set(true)
                    viewModel?.obsTitle!!.set(getString(R.string.title_about_app))
                    binding.ibBack.visibility = View.VISIBLE
                }
                 R.id.navigation_policy -> {
                    customBarColor(ContextCompat.getColor(this, R.color.color_primary))
                    viewModel?.obsShowToolbar!!.set(true)
                    viewModel?.obsTitle!!.set(getString(R.string.title_cancel_policy))
                     binding.ibBack.visibility = View.VISIBLE
                 }
                R.id.navigation_suggestions -> {
                    customBarColor(ContextCompat.getColor(this, R.color.color_primary))
                    viewModel?.obsShowToolbar!!.set(true)
                    viewModel?.obsTitle!!.set(getString(R.string.title_suggestions))
                    binding.ibBack.visibility = View.VISIBLE
                }
                R.id.navigation_contact_us -> {
                    customBarColor(ContextCompat.getColor(this, R.color.color_primary))
                    viewModel?.obsShowToolbar!!.set(true)
                    viewModel?.obsTitle!!.set(getString(R.string.title_contact_us))
                    binding.ibBack.visibility = View.VISIBLE
                }  R.id.navigation_experience -> {
                    customBarColor(ContextCompat.getColor(this, R.color.color_primary))
                    viewModel?.obsShowToolbar!!.set(true)
                    viewModel?.obsTitle!!.set(getString(R.string.label_experiance))
                    binding.ibBack.visibility = View.VISIBLE
                }
                else -> {
                    viewModel?.obsShowToolbar!!.set(true)
                    customBarColor(ContextCompat.getColor(this, R.color.white))
                    binding.ibBack.visibility = View.VISIBLE
                }
            }
        }
    }
    private fun initViewModel() {
        if (viewModel == null) {
            viewModel = ViewModelProvider(this@MainActivity).get(MainViewModel::class.java)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_home_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    private fun customBarColor(color: Int) {

        binding.toolbar.setBackgroundColor(color)

        when {
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP -> {
                val window: Window = window
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                window.statusBarColor = color
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()

//        when (navController.currentDestination!!.id) {
//            R.id.nav_order_status -> {
//                navController.navigate(R.id.nav_my_orders)
//            }
//        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when(requestCode){
            Codes.DIALOG_SELECT_LANGUAGE -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        var lang = data.getStringExtra(Params.LANGUAGE)
                                        LocalUtil.changeLang(this, lang.toString())
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}