package grand.app.salonmohtref.activity.auth

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import grand.app.salonmohtref.base.BaseViewModel

class AuthViewModel : BaseViewModel()
{
    val obsShowToolbar = ObservableBoolean()
    val obsShowSkipBtn = ObservableBoolean()
    val obsBackBtn = ObservableBoolean()
    val obsTitle = ObservableField<String>()
    val obsProgressBar = ObservableField<Boolean>()
}