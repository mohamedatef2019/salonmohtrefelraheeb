package grand.app.salonmohtref.activity.home

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.addCallback
import androidx.lifecycle.MutableLiveData
import androidx.navigation.fragment.findNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import grand.app.salonmohtref.R
import grand.app.salonmohtref.base.BaseFragment
import grand.app.salonmohtref.utils.constants.Codes
import grand.app.salonmohtref.utils.constants.Params
import timber.log.Timber

open class BaseHomeFragment : BaseFragment()
{

    override fun onDestroyView() {
        super.onDestroyView()
        when {
            !isNavigated -> requireActivity().onBackPressedDispatcher.addCallback(this) {
                val navController = findNavController()
                when {
                    navController.currentBackStackEntry?.destination?.id != null -> {
                        findNavController().navigateUp()
                    }
                    else -> navController.popBackStack()
                }
            }
        }
    }

    fun setBarName(title: String)
    {
        (requireActivity() as MainActivity).run {
            viewModel!!.obsTitle.set(title)
        }
    }
    
    fun showBottomBar(show: Boolean)
    {
        try
        {
            val navView: BottomNavigationView = requireActivity().findViewById(R.id.bottomBar)
            when {
                show -> {
                    navView.visibility = View.VISIBLE
                }
                else -> navView.visibility = View.GONE
            }
        }
        catch (e: Exception)
        {
            Timber.e(e)
        }
    }
}

