package grand.app.salonmohtref.activity.auth

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.observe
import androidx.navigation.NavController
import androidx.navigation.findNavController
import grand.app.salonmohtref.base.BaseActivity
import grand.app.salonmohtref.R
import grand.app.salonmohtref.activity.home.MainActivity
import grand.app.salonmohtref.databinding.ActivityAuthBinding
import grand.app.salonmohtref.utils.LocalUtil
import grand.app.salonmohtref.utils.constants.Const
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import timber.log.Timber

class AuthActivity : BaseActivity()
{
    lateinit var binding : ActivityAuthBinding
    var viewModel: AuthViewModel? = null
    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?)
    {
        LocalUtil.changeLanguage(this)
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_auth)

        initViewModel()
        binding.viewModel = viewModel
        navController = findNavController(R.id.nav_auth_host_fragment)

        binding.ibBack.setOnClickListener {
            when (navController.currentDestination?.id) {
                R.id.navigation_login -> {
                    finishAffinity()
                }
                else -> {
                    navController.navigateUp()
                }
            }
        }

        binding.btnSkip.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
            finishAffinity()
        }

        viewModel!!.clickableLiveData.observe(this) {
            viewModel!!.obsIsClickable.set(false)
            lifecycleScope.launch {
                delay(2000)
                viewModel!!.clickableLiveData.value = false
                viewModel!!.obsIsClickable.set(true)
            }
        }
        when {
            intent.hasExtra(Const.ACCESS_LOGIN) -> {
                when {
                    intent.getBooleanExtra(Const.ACCESS_LOGIN, false) -> {
                        navController.navigate(R.id.navigation_login)
                    }
                }
            }
        }


         setUpToolbarAndStatusBar()
    }

    private fun initViewModel() {
        when (viewModel) {
            null -> {
                viewModel = ViewModelProvider(this@AuthActivity).get(AuthViewModel::class.java)
            }
        }
    }

    private fun setUpToolbarAndStatusBar() {
        navController.addOnDestinationChangedListener { controller, destination, arguments ->
            when (destination.id) {
                R.id.navigation_splash -> {
                    viewModel?.obsShowToolbar!!.set(false)
                    customToolBar(color = ContextCompat.getColor(this, R.color.color_primary))
                    viewModel?.obsBackBtn!!.set(false)
                }
                R.id.navigation_intro_one, R.id.navigation_intro_two, R.id.navigation_intro_three -> {
                    viewModel?.obsShowToolbar!!.set(false)
                    customToolBar("",
                        show = false,
                        showSkip = false,
                        color = ContextCompat.getColor(this, R.color.color_primary)
                    )
                    viewModel?.obsBackBtn!!.set(false)
                }
                R.id.navigation_login -> {
                    customToolBar(getString(R.string.title_login), show = true, showSkip = true,
                        color = ContextCompat.getColor(this, R.color.color_primary)
                    )
                    viewModel?.obsBackBtn!!.set(false)
                }
                R.id.navigation_register -> {
                    customToolBar(getString(R.string.title_register), show = true, showSkip = true,
                        color = ContextCompat.getColor(this, R.color.color_primary)
                    )
                    viewModel?.obsBackBtn!!.set(true)
                }
                R.id.navigation_forgot_password -> {
                    customToolBar("", show = true, showSkip = false,
                        color = ContextCompat.getColor(this, R.color.color_primary)
                    )
                    viewModel?.obsBackBtn!!.set(true)
                }
                R.id.navigation_verify_code -> {
                    customToolBar("", show = true, showSkip = false,
                        color = ContextCompat.getColor(this, R.color.color_primary)
                    )
                    viewModel?.obsBackBtn!!.set(true)
                }
                R.id.navigation_reset_password -> {
                    customToolBar("", show = true, showSkip = false,
                        color = ContextCompat.getColor(this, R.color.color_primary)
                    )
                    viewModel?.obsBackBtn!!.set(true)
                }
                else -> {
                    viewModel?.obsShowToolbar!!.set(true)
                    viewModel?.obsBackBtn!!.set(true)
                }
            }
        }
    }

    private fun customToolBar(title: String = "", show : Boolean = false, showSkip : Boolean = false, color: Int = ContextCompat.getColor(this, R.color.color_primary)) {

        viewModel?.obsTitle!!.set(title)
        viewModel?.obsShowToolbar!!.set(show)
        viewModel?.obsShowSkipBtn!!.set(showSkip)
        binding.toolbar.setBackgroundColor(color)

        when {
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP -> {
                val window: Window = window
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                window.statusBarColor = color
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()

        Timber.e("mou3az_back" + navController.currentDestination!!.id)

        when (navController.currentDestination!!.id) {
            R.id.navigation_login -> {
                Timber.e("mou3az_back login " + navController.currentDestination!!.id)
                finish()
            }
            R.id.navigation_splash -> {
                Timber.e("mou3az_back splash" + navController.currentDestination!!.id)
                finish()
            }
        }
    }
}