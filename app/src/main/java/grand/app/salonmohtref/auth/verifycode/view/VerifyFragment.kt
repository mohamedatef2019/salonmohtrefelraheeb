package grand.app.salonmohtref.auth.verifycode.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import grand.app.salonmohtref.R
import grand.app.salonmohtref.activity.auth.BaseAuthFragment
import grand.app.salonmohtref.activity.home.MainActivity
import grand.app.salonmohtref.auth.verifycode.response.VerifyResponse
import grand.app.salonmohtref.auth.verifycode.viewmodel.VerifyViewModel
import grand.app.salonmohtref.databinding.FragmentVerifyCodeBinding
import grand.app.salonmohtref.network.Status
import grand.app.salonmohtref.utils.PrefMethods
import grand.app.salonmohtref.utils.constants.Codes
import grand.app.salonmohtref.utils.constants.Const
import grand.app.salonmohtref.utils.observe
import timber.log.Timber

class VerifyFragment : BaseAuthFragment(), Observer<Any?>
{
    lateinit var binding: FragmentVerifyCodeBinding
    lateinit var viewModel: VerifyViewModel
    private val fragmentArgs : VerifyFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_verify_code, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(VerifyViewModel::class.java)
        binding.viewModel = viewModel

        viewModel.mutableLiveData.observe(viewLifecycleOwner, this)

        viewModel.request.phone = fragmentArgs.phone

        when (fragmentArgs.flag) {
            Codes.REGISTER_INTENT -> {
                binding.tvVerifyTitle.text = getString(R.string.label_verify_register)
                binding.imgVerify.setImageResource(R.drawable.img_verify_register)
            }
            Codes.FORGOT_INTENT -> {
                binding.tvVerifyTitle.text = getString(R.string.label_verify_title)
                binding.imgVerify.setImageResource(R.drawable.img_verify_forgot)
            }
        }

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is VerifyResponse -> {
                            when (fragmentArgs.flag) {
                                Codes.REGISTER_INTENT -> {
                                    PrefMethods.saveUserData(it.data.userData)
                                    when (Const.isAskedToLogin) {
                                        1 -> {
                                            requireActivity().finish()
                                        }
                                        else -> {
                                            requireActivity().finishAffinity()
                                            startActivity(Intent(requireActivity(), MainActivity::class.java))
                                        }
                                    }
                                }
                                else -> {
                                    val action = VerifyFragmentDirections.verifyToReset(viewModel.request.phone!!)
                                    findNavController().navigate(action)
                                }
                            }
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }
    }

    override fun onChanged(it: Any?) {
        if (it == null) return
        when (it) {
            Codes.EMPTY_CODE -> {
                showToast(getString(R.string.msg_empty_code) , 1)
            }
            Codes.SHORT_CODE -> {
                showToast(getString(R.string.msg_short_code) , 1)
            }
        }
    }
}