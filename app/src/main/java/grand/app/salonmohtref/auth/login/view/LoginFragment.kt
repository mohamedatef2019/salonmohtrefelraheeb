package grand.app.salonmohtref.auth.login.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.google.firebase.messaging.FirebaseMessaging
import grand.app.salonmohtref.R
import grand.app.salonmohtref.activity.auth.BaseAuthFragment
import grand.app.salonmohtref.activity.home.MainActivity
import grand.app.salonmohtref.auth.login.response.LoginResponse
import grand.app.salonmohtref.auth.login.viewmodel.LoginViewModel
import grand.app.salonmohtref.databinding.FragmentLoginBinding
import grand.app.salonmohtref.network.Status
import grand.app.salonmohtref.utils.PrefMethods
import grand.app.salonmohtref.utils.constants.Codes
import grand.app.salonmohtref.utils.constants.Const
import grand.app.salonmohtref.utils.observe
import timber.log.Timber

class LoginFragment : BaseAuthFragment(), Observer<Any?>
{
    lateinit var binding: FragmentLoginBinding
    lateinit var viewModel: LoginViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(LoginViewModel::class.java)

        binding.viewModel = viewModel
        viewModel.mutableLiveData.observe(viewLifecycleOwner, this)

        FirebaseMessaging.getInstance().token.addOnCompleteListener { task->
            when {
                !task.isSuccessful -> {
                    Timber.tag("fcm_token").e(task.exception)
                    return@addOnCompleteListener
                }
                task.result != null -> {
                    viewModel.loginRequest.firebase_token = task.result!!
                    Timber.tag("fcm_token").e(task.result!!)
                }
            }
        }

        Timber.e("mou3az_access_value : " + Const.isAskedToLogin)


        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is LoginResponse -> {
                            when (it.data.code) {
                                405 -> {
                                    val action = LoginFragmentDirections.loginToVerify(viewModel.loginRequest.loginkey!!, Codes.REGISTER_INTENT)
                                    findNavController().navigate(action)
                                }
                                else -> {
                                    PrefMethods.saveUserData(it.data.userData)
                                    Timber.e("mou3az_access_value : " + Const.isAskedToLogin)
                                    when (Const.isAskedToLogin) {
                                        1 -> {
                                            requireActivity().finish()
                                        }
                                        else -> {
                                            Const.isAskedToLogin = 0
                                            startActivity(Intent(requireActivity(), MainActivity::class.java))
                                            requireActivity().finishAffinity()
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }
    }

    override fun onChanged(it: Any?) {
        if (it == null) return
        when (it) {
            Codes.EMPTY_PHONE -> {
                showToast(getString(R.string.msg_empty_login_key) , 1)
            }
            Codes.PASSWORD_EMPTY -> {
                showToast(getString(R.string.msg_empty_password) , 1)
            }
            Codes.PASSWORD_SHORT -> {
                showToast(getString(R.string.msg_invalid_password) , 1)
            }
            Codes.REGISTER_INTENT -> {
                findNavController().navigate(R.id.login_to_register)
            }
            Codes.FORGOT_INTENT -> {
                findNavController().navigate(R.id.login_to_forgot_password)
            }
        }
    }
}