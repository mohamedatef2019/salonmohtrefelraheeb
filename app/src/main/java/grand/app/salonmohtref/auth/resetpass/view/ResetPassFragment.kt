package grand.app.salonmohtref.auth.resetpass.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import grand.app.salonmohtref.R
import grand.app.salonmohtref.activity.auth.BaseAuthFragment
import grand.app.salonmohtref.auth.resetpass.response.ResetPassResponse
import grand.app.salonmohtref.auth.resetpass.viewmodel.ResetPassViewModel
import grand.app.salonmohtref.databinding.FragmentResetPasswordBinding
import grand.app.salonmohtref.network.Status
import grand.app.salonmohtref.utils.constants.Codes
import grand.app.salonmohtref.utils.observe
import timber.log.Timber

class ResetPassFragment : BaseAuthFragment(), Observer<Any?>
{
    lateinit var binding: FragmentResetPasswordBinding
    lateinit var viewModel: ResetPassViewModel
    private val fragmentArgs : ResetPassFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_reset_password, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(ResetPassViewModel::class.java)
        binding.viewModel = viewModel

        viewModel.mutableLiveData.observe(viewLifecycleOwner, this)

        viewModel.request.phone = fragmentArgs.phone

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is ResetPassResponse -> {
                            findNavController().navigate(R.id.reset_to_login)
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }
    }

    override fun onChanged(it: Any?) {
        if (it == null) return
        when (it) {
            Codes.PASSWORD_EMPTY -> {
                showToast(getString(R.string.msg_empty_password) , 1)
            }
            Codes.PASSWORD_SHORT -> {
                showToast(getString(R.string.msg_invalid_password) , 1)
            }
            Codes.CONFIRM_PASS_EMPTY -> {
                showToast(getString(R.string.msg_empty_confirm_password) , 1)
            }
            Codes.PASSWORD_NOT_MATCH -> {
                showToast(getString(R.string.msg_not_match) , 1)
            }
        }
    }
}