package grand.app.salonmohtref.auth.forgotpass.viewmodel

import grand.app.salonmohtref.auth.forgotpass.request.ForgotRequest
import grand.app.salonmohtref.auth.forgotpass.response.ForgotResponse
import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.network.ApiResponse
import grand.app.salonmohtref.utils.constants.Codes
import grand.app.salonmohtref.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ForgotPassViewModel : BaseViewModel()
{
    var request = ForgotRequest()

    fun onSendClicked() {
        setClickable()

        when {
            request.phone.isNullOrEmpty() || request.phone.isNullOrBlank() -> {
                setValue(Codes.EMPTY_PHONE)
            }
            request.phone!!.length < 11 -> {
                setValue(Codes.INVALID_PHONE)
            }
            else -> {
                sendCodeToVerify()
            }
        }
    }

    private fun sendCodeToVerify() {
        obsIsProgress.set(true)
        requestCall<ForgotResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().forgotPassword(request) }
        })
        { res ->
            obsIsProgress.set(false)
            when (res!!.code) {
                200 -> {
                    apiResponseLiveData.value = ApiResponse.success(res)
                }
                else ->
                {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }
}