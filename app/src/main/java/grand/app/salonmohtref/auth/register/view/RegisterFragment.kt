package grand.app.salonmohtref.auth.register.view

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.net.toUri
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.fxn.pix.Pix
import grand.app.salonmohtref.R
import grand.app.salonmohtref.activity.auth.BaseAuthFragment
import grand.app.salonmohtref.auth.register.response.RegisterResponse
import grand.app.salonmohtref.auth.register.viewmodel.RegisterViewModel
import grand.app.salonmohtref.databinding.FragmentRegisterBinding
import grand.app.salonmohtref.network.Status
import grand.app.salonmohtref.utils.constants.Codes
import grand.app.salonmohtref.utils.observe
import timber.log.Timber

class RegisterFragment : BaseAuthFragment(), Observer<Any?>
{
    lateinit var binding : FragmentRegisterBinding
    lateinit var viewModel : RegisterViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_register, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(RegisterViewModel::class.java)
        binding.viewModel = viewModel
        viewModel.mutableLiveData.observe(viewLifecycleOwner, this)

        Glide.with(this).load(viewModel.obsImg.get()).error(R.drawable.ic_user).into(binding.ivUserImg)

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is RegisterResponse -> {
                            val action = RegisterFragmentDirections.registerToVerify(
                                viewModel.request.phone!!,
                                Codes.REGISTER_INTENT
                            )
                            findNavController().navigate(action)
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }
    }

    override fun onChanged(it: Any?)
    {
        if(it == null) return
        when (it) {
            Codes.EMPTY_USER_IMAGE -> {
                showToast(getString(R.string.msg_empty_user_image), 1)
            }
            Codes.CHOOSE_IMAGE_CLICKED -> {
                pickImage(Codes.USER_PROFILE_IMAGE_REQUEST)
            }
            Codes.LOGIN_INTENT -> {
                findNavController().navigate(R.id.register_to_login)
            }
            Codes.EMPTY_NAME -> {
                showToast(getString(R.string.msg_empty_name), 1)
            }
            Codes.EMAIL_EMPTY -> {
                showToast(getString(R.string.msg_empty_email), 1)
            }
            Codes.EMPTY_PHONE -> {
                showToast(getString(R.string.msg_empty_phone), 1)
            }
            Codes.INVALID_PHONE -> {
                showToast(getString(R.string.msg_invalid_phone), 1)
            }
            Codes.PASSWORD_EMPTY -> {
                showToast(getString(R.string.msg_empty_password), 1)
            }
            Codes.PASSWORD_SHORT -> {
                showToast(getString(R.string.msg_invalid_password), 1)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (resultCode) {
            Activity.RESULT_OK -> {
                when (requestCode) {
                    Codes.USER_PROFILE_IMAGE_REQUEST -> {
                        data?.let {
                            val returnValue = it.getStringArrayListExtra(Pix.IMAGE_RESULTS)
                            returnValue?.let { array ->
                                when (requestCode) {
                                    Codes.USER_PROFILE_IMAGE_REQUEST -> {
                                        val returnValue = data.getStringArrayListExtra(Pix.IMAGE_RESULTS)!!
                                        viewModel.obsImg.set(returnValue[0].toUri().toString())
                                         binding.ivUserImg.setImageURI(returnValue[0].toUri())
                                        viewModel.gotImage(requestCode, array[0])
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}