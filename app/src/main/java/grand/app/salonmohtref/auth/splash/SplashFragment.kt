package grand.app.salonmohtref.auth.splash

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import grand.app.salonmohtref.R
import grand.app.salonmohtref.activity.auth.BaseAuthFragment
import grand.app.salonmohtref.activity.home.MainActivity
import grand.app.salonmohtref.auth.splash.response.SplashResponse
import grand.app.salonmohtref.databinding.FragmentSplashBinding
import grand.app.salonmohtref.network.Status
import grand.app.salonmohtref.utils.PrefMethods
import grand.app.salonmohtref.utils.observe
import timber.log.Timber

class SplashFragment : BaseAuthFragment()
{
    lateinit var binding: FragmentSplashBinding
    lateinit var viewModel: SplashViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_splash, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(SplashViewModel::class.java)

        binding.viewModel = viewModel

        binding.swipeLayout.setOnRefreshListener {
            binding.swipeLayout.isRefreshing = false
            viewModel.getSliders()
        }

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is SplashResponse -> {
                            PrefMethods.saveSplashData(it.data)
                            takeAction()
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }
    }

     private fun takeAction() {
         when {
             /*** User logged in ***/
             PrefMethods.getIsFirstTime() -> {
                 findNavController().navigate(R.id.splash_to_intro)
             }
             /**
              * If user open the app for the first time
              * */
             else -> {
                 when {
                     PrefMethods.getUserData() != null -> {
                         startActivity(Intent(requireActivity(), MainActivity::class.java))
                         requireActivity().finishAffinity()
                     }
                     else -> {
                         findNavController().navigate(R.id.splash_to_login)
                     }
                 }
             }
         }
    }

    override fun onStart() {
        super.onStart()

        viewModel.getSettings()
        viewModel.getSliders()
    }
}