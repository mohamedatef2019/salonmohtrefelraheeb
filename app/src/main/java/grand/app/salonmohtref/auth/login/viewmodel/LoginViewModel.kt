package grand.app.salonmohtref.auth.login.viewmodel

import grand.app.salonmohtref.auth.login.request.LoginRequest
import grand.app.salonmohtref.auth.login.response.LoginResponse
import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.network.ApiResponse
import grand.app.salonmohtref.utils.constants.Codes
import grand.app.salonmohtref.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class LoginViewModel : BaseViewModel()
{
    var loginRequest = LoginRequest()

    fun onLoginClicked() {
        setClickable()

        when {
            loginRequest.loginkey.isNullOrEmpty() || loginRequest.loginkey.isNullOrBlank() -> {
                setValue(Codes.EMPTY_LOGIN_KEY)
            }
            loginRequest.password.isNullOrEmpty() || loginRequest.password.isNullOrBlank() -> {
                setValue(Codes.PASSWORD_EMPTY)
            }
            loginRequest.password!!.length < 6 -> {
                setValue(Codes.PASSWORD_SHORT)
            }
            else -> {
                login()
            }
        }
    }

    fun login() {

        obsIsProgress.set(true)
        requestCall<LoginResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().login(loginRequest) }
        })
        { res ->
            obsIsProgress.set(false)
            when (res!!.code) {
                200 -> {
                    apiResponseLiveData.value = ApiResponse.success(res)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
                }
            }
        }
    }

    fun onForgotClicked() {
        setClickable()
        setValue(Codes.FORGOT_INTENT)
    }

    fun onRegisterClicked() {
        setClickable()
        setValue(Codes.REGISTER_INTENT)
    }
}