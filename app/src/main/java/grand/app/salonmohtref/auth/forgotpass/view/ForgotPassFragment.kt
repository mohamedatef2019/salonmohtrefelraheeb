package grand.app.salonmohtref.auth.forgotpass.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import grand.app.salonmohtref.R
import grand.app.salonmohtref.activity.auth.BaseAuthFragment
import grand.app.salonmohtref.auth.forgotpass.response.ForgotResponse
import grand.app.salonmohtref.auth.forgotpass.viewmodel.ForgotPassViewModel
import grand.app.salonmohtref.databinding.FragmentForgotPasswordBinding
import grand.app.salonmohtref.network.Status
import grand.app.salonmohtref.utils.constants.Codes
import grand.app.salonmohtref.utils.observe
import timber.log.Timber

class ForgotPassFragment : BaseAuthFragment(), Observer<Any?>
{
    lateinit var binding: FragmentForgotPasswordBinding
    lateinit var viewModel: ForgotPassViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_forgot_password, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(ForgotPassViewModel::class.java)
        binding.viewModel = viewModel
        viewModel.mutableLiveData.observe(viewLifecycleOwner, this)

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is ForgotResponse -> {
                            val action = ForgotPassFragmentDirections.forgotToVerify(viewModel.request.phone!!, Codes.FORGOT_INTENT)
                            findNavController().navigate(action)
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }
    }

    override fun onChanged(it: Any?) {
        if (it == null) return
        when (it) {
            Codes.EMPTY_PHONE -> {
                showToast(getString(R.string.msg_empty_phone) , 1)
            }
            Codes.INVALID_PHONE -> {
                showToast(getString(R.string.msg_invalid_phone) , 1)
            }
            Codes.LOGIN_INTENT -> {
                findNavController().navigate(R.id.forgot_to_login)
            }
        }
    }
}