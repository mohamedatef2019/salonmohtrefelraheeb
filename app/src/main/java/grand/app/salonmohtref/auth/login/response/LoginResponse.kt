package grand.app.salonmohtref.auth.login.response

import com.google.gson.annotations.SerializedName

data class LoginResponse(

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val userData: UserData? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class UserData(

	@field:SerializedName("img")
	val img: String? = null,

	@field:SerializedName("wallet")
	val wallet: Int? = null,

	@field:SerializedName("lng")
	val lng: String? = null,

	@field:SerializedName("jwt")
	var jwt: String ?= null,

	@field:SerializedName("package_id")
	val packageId: Int? = null,

	@field:SerializedName("type")
	val type: Int? = null,

	@field:SerializedName("firebase_token")
	val firebaseToken: String? = null,

	@field:SerializedName("points")
	val points: Int? = null,

	@field:SerializedName("social_id")
	val socialId: String? = null,

	@field:SerializedName("phone")
	val phone: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("email")
	val email: String? = null,

	@field:SerializedName("lat")
	val lat: String? = null,

	@field:SerializedName("status")
	val status: Int? = null,

	@field:SerializedName("package")
	val packageDetails: LoginPackageItem? = null
)

data class LoginPackageItem(

		@field:SerializedName("cover")
		val cover: String? = null,

		@field:SerializedName("badge")
		val badge: String? = null,

		@field:SerializedName("visits")
		val visits: Int? = null,

		@field:SerializedName("period")
		val period: Int? = null,

		@field:SerializedName("price")
		val price: Int? = null,

		@field:SerializedName("name")
		val name: String? = null,

		@field:SerializedName("id")
		val id: Int? = null,

		@field:SerializedName("free_months")
		val freeMonths: Int? = null
)
