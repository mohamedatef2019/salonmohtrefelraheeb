package grand.app.salonmohtref.auth.splash.response

import com.google.gson.annotations.SerializedName

data class SettingResponse(

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val settingData: SettingData? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class SettingData(

	@field:SerializedName("whatsapp")
	val whatsapp: String? = null,

	@field:SerializedName("twitter")
	val twitter: String? = null,

	@field:SerializedName("reservation_points")
	val reservationPoints: Int? = null,

	@field:SerializedName("cancellation_percent")
	val cancellationPercent: Int? = null,

	@field:SerializedName("facebook")
	val facebook: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("tax")
	val tax: Double? = null
)
