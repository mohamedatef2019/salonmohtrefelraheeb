package grand.app.salonmohtref.auth.forgotpass.request

data class ForgotRequest (
    var phone: String? = null)