package grand.app.salonmohtref.auth.splash

import grand.app.salonmohtref.auth.splash.response.SettingResponse
import grand.app.salonmohtref.auth.splash.response.SplashResponse
import grand.app.salonmohtref.base.BaseViewModel
import grand.app.salonmohtref.main.suggestion.request.SuggestRequest
import grand.app.salonmohtref.network.ApiResponse
import grand.app.salonmohtref.utils.PrefMethods
import grand.app.salonmohtref.utils.requestCall
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class SplashViewModel : BaseViewModel()
{
    fun getSliders()
    {
        requestCall<SplashResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().splash() }
        })
        { res ->
            when (res!!.code) {
                200 -> {
                    apiResponseLiveData.value = ApiResponse.success(res)
                }
                401 -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg.toString())
                }
            }
        }
    }

    fun getSettings()
    {
        requestCall<SettingResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().getSocialLinks() }
        })
        { res ->
            when (res!!.code) {
                200 -> {
                    PrefMethods.saveSettingData(res.settingData!!)
                }
            }
        }
    }
}