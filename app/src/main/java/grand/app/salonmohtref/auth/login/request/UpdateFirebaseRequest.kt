package grand.app.salonmohtref.auth.login.request

data class UpdateFirebaseRequest (
    var firebase_token: String? = null
)