package grand.app.salonmohtref.databinding

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.BindingAdapter
import grand.app.salonmohtref.R
import grand.app.salonmohtref.base.LoadingStatus
import grand.app.salonmohtref.utils.isValidUrl
import grand.app.salonmohtref.utils.loadImageFromURL
import timber.log.Timber
import java.net.URI

/**
 * Created by MouazSalah on 15/04/2021.
 **/
class OtherViewsBinding {

    @BindingAdapter("setImage")
    fun setImage(imageView: ImageView, obj: Any?) {
        obj?.let {
            when (it) {
                is Int -> imageView.setImageResource(it)
                is Drawable -> imageView.setImageDrawable(it)
                is Bitmap -> imageView.setImageBitmap(it)
                is Uri -> imageView.setImageURI(it)
                is URI -> imageView.setImageURI(it as Uri)
                is String -> if (it.isValidUrl()) imageView.loadImageFromURL(it) else {
                    Timber.e("image url isn't valid")
                    imageView.loadImageFromURL("")
                }
            }
        } ?: imageView.setImageResource(R.drawable.ic_logo)
    }

        /* Showing Shimmer Layouts while requesting API calls */
        @BindingAdapter("showShimmer")
        fun showShimmer(view: View, state: LoadingStatus?) {
            when (state) {
                LoadingStatus.SHIMMER -> {
                    view.visibility = View.VISIBLE
                }
                else -> {
                    view.visibility = View.GONE
                }
            }
        }

        /* Showing Shimmer Layouts while requesting API calls */
        @BindingAdapter("showFull")
        fun showFull(view: View, state: LoadingStatus?) {
            when (state) {
                LoadingStatus.FULL -> {
                    view.visibility = View.VISIBLE
                }
                else -> {
                    view.visibility = View.GONE
                }
            }
        }

        /* Showing Shimmer Layouts while requesting API calls */
        @BindingAdapter("showEmpty")
        fun showEmpty(view: View, state: LoadingStatus?) {
            when (state) {
                LoadingStatus.EMPTY -> {
                    view.visibility = View.VISIBLE
                }
                else -> {
                    view.visibility = View.GONE
                }
            }
        }

        /* Showing Shimmer Layouts while requesting API calls */
        @BindingAdapter("showNotLogin")
        fun showNotLogin(view: View, state: LoadingStatus?) {
            when (state) {
                LoadingStatus.NOTLOGIN -> {
                    view.visibility = View.VISIBLE
                }
                else -> {
                    view.visibility = View.GONE
                }
            }
        }

        @BindingAdapter("isHome")
        fun isHome(view: ImageView, flag: Int?) {
            when (flag) {
                1, 3 -> {
                    view.visibility = View.VISIBLE
                }
                else -> {
                    view.visibility = View.GONE
                }
            }
        }

    @BindingAdapter("setTextColor" , "setBackground")
    fun customTab(view: TextView, @ColorRes color: Int?, @DrawableRes backgroundColor: Int?) {
        color?.let {
            view.setTextColor(view.resources.getColor(color, null))
        }
        backgroundColor?.let {
            view.setBackgroundResource(backgroundColor)
        }
    }


        @BindingAdapter("isSalon")
        fun isSalon(view: ImageView, flag: Int?) {
            when (flag) {
                2, 3 -> {
                    view.visibility = View.VISIBLE
                }
                else -> {
                    view.visibility = View.GONE
                }
            }
        }

        @BindingAdapter("isVisible")
        fun isVisible(view: View, value: Boolean?) {
            if (value == false) {
                view.visibility = View.GONE
            } else {
                view.visibility = View.VISIBLE
            }
        }

        /* Which shows the address on MAP activity ... Urgent */
        @BindingAdapter("showAddressLayout")
        fun showAddressLayout(view: ConstraintLayout, address: String?) {
            if (address == null) {
                view.visibility = View.GONE
            } else {
                view.visibility = View.VISIBLE
            }
        }

        @BindingAdapter("showSubCategories")
        fun showSubCategories(view: View, state: Int?) {
            if (state == -1) {
                view.visibility = View.GONE
            } else {
                view.visibility = View.VISIBLE
            }
        }

        @BindingAdapter("visibleGone")
        fun visibleGone(view: View, state: Boolean?) {
            if (state == true) {
                view.visibility = View.VISIBLE
            } else {
                view.visibility = View.GONE
            }
        }

        @BindingAdapter("showPass")
        fun showPass(view: EditText, isShown: Boolean?) {
            if (isShown == true) {
                view.transformationMethod = HideReturnsTransformationMethod.getInstance()
            } else {
                view.transformationMethod = PasswordTransformationMethod.getInstance()
            }
        }


//
//    @BindingAdapter("renderHtml")
//    fun bindRenderHtml(view: TextView, description: String?) {
//        if (description != null) {
//            view.text = HtmlCompat.fromHtml(description, HtmlCompat.FROM_HTML_MODE_COMPACT)
//            view.movementMethod = LinkMovementMethod.getInstance()
//        } else {
//            view.text = ""
//        }
//    }
//
//    @BindingAdapter("app:icon")
//    fun bindIconButton(btn: MaterialButton, resId: Int) {
//        resId.let {
//            btn.icon = ContextCompat.getDrawable(btn.context, resId)
//        }
//    }
//
//    @BindingAdapter("app:animateSplashImage")
//    fun bindSplashImage(imageView: ImageView, b: Boolean) {
//        val padding = 90
//        if (b) {
//            val params = imageView.layoutParams as ConstraintLayout.LayoutParams
//            params.verticalBias = 0.2f // here is one modification for example.
//            imageView.layoutParams = params
//            imageView.setPadding(padding, padding, padding, padding)
//        }
//    }
//
//    @BindingAdapter("app:visibleGone")
//    fun bindViewGone(view: View, b: Boolean) {
//        view.visibility = when (b) {
//            true -> View.VISIBLE
//            else -> View.GONE
//        }
//    }
//
//    @BindingAdapter("app:visibleInVisible")
//    fun bindViewInvisible(view: View, b: Boolean) {
//        view.visibility = when (b) {
//            true -> View.VISIBLE
//            else -> View.INVISIBLE
//        }
//    }
//
//    @BindingAdapter("app:lockView")
//    fun bindLockView(view: View, b: Boolean) {
//        view.isEnabled = !b
//    }

}