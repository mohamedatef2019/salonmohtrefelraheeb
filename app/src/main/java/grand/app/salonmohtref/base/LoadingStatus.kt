package grand.app.salonmohtref.base

/**
 * Created by MouazSalah 28/12/2020.
 **/
enum class LoadingStatus {
    SHIMMER,
    FULL,
    EMPTY,
    NOTLOGIN
}

