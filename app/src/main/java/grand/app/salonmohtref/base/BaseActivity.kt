package grand.app.salonmohtref.base

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import grand.app.salonmohtref.utils.LocalUtil
import grand.app.salonmohtref.utils.constants.Codes
import grand.app.salonmohtref.utils.constants.Const
import grand.app.salonmohtref.utils.constants.Params
import timber.log.Timber

open class BaseActivity : AppCompatActivity()
{
    override fun onCreate(savedInstanceState: Bundle?) {
        LocalUtil.changeLanguage(this)
        super.onCreate(savedInstanceState)
        LocalUtil.changeLanguage(this)
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(LocalUtil.onAttach(newBase))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        for (fragment in supportFragmentManager.primaryNavigationFragment!!.childFragmentManager.fragments) {
            fragment.onActivityResult(requestCode, resultCode, data)
            when (requestCode) {
                Codes.DIALOG_CANCEL_BOOKING -> {
                    data?.let { intent ->
                        when {
                            intent.getIntExtra(Params.DIALOG_CLICK_ACTION, 0) == 1 -> {
                                Params.mutableActionLiveData.value = 1
                            }
                        }
                    }
                }
                Codes.DIALOG_LOGIN_REQUEST -> {
                    data?.let { intent ->
                        when {
                            intent.getIntExtra(Params.DIALOG_CLICK_ACTION, 0) == 1 -> {
                                Params.mutableActionLiveData.value = 1
                            }
                        }
                    }
                }
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        for (fragment in supportFragmentManager.primaryNavigationFragment!!.childFragmentManager.fragments)
        {
            fragment.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }
}