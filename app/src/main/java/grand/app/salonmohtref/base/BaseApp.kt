package grand.app.salonmohtref.base

import android.app.Application
import android.content.Context
import androidx.databinding.DataBindingUtil
import androidx.multidex.MultiDex
import grand.app.salonmohtref.databinding.AppDataBindingComponent
import grand.app.salonmohtref.utils.LocalUtil
import timber.log.Timber

class BaseApp : Application() {

    override fun onCreate() {
        super.onCreate()
        getInstance = this
        initTimber()
        DataBindingUtil.setDefaultComponent(AppDataBindingComponent())
    }

    private fun initTimber() {
        Timber.plant(object : Timber.DebugTree() {
            override fun createStackElementTag(element: StackTraceElement): String? {
                return super.createStackElementTag(element) + " line: " + element.lineNumber
            }
        })
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(LocalUtil.onAttach(base))
        MultiDex.install(this)
    }
    companion object {
        lateinit var getInstance: BaseApp
    }
}